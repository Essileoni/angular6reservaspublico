import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoadingService } from './loading.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit, OnDestroy {
  mostrar = false;
  private suscripcionLoading: Subscription;
  constructor(private loadingService: LoadingService) {
    this.suscripcionLoading = this.loadingService
      .getObservadorLoading()
      .subscribe((mostrar: boolean) => {
        if (mostrar === this.mostrar) {
          return;
        }
        setTimeout(() => {
          this.mostrar = mostrar;
        }, 0);
      });
  }

  ngOnInit() {
    // Cuidado el ngOnInit no es muy rapido.
  }

  ngOnDestroy() {
    this.suscripcionLoading.unsubscribe();
  }


}
