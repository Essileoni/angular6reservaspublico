import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private subject: Subject<boolean> = new Subject<boolean>();
  private estadoActual: boolean;
  constructor() { }

  getObservadorLoading(): Observable<boolean> {
    return this.subject.asObservable();
  }

  start() {
    if (!environment.loadingOn) {
      return;
    }
    this.estadoActual = true;
    this.subject.next(true);
  }

  stop() {
    this.estadoActual = false;
    this.subject.next(false);
  }

  mostrarEstado(): Boolean {
    return this.estadoActual;
  }
}
