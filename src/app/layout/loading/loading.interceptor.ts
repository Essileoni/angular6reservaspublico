import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';

import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  // HttpUserEvent,
  HttpEvent,
} from '@angular/common/http';
import { LoadingService } from './loading.service';
import { tap, catchError, finalize } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const rutasExcluidas: string[] = environment.rutasExcluidas;

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  private arrayStart: HttpRequest<any>[] = [];

  constructor(private loadingService: LoadingService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const rutasExcluidasComplejas = rutasExcluidas.filter(
      (r) => r.indexOf('{*}') !== -1
    );
    let salir = false;
    if (rutasExcluidas.find((r) => req.url.indexOf(r) > -1)) {

      salir = true;
    }

    rutasExcluidasComplejas.forEach((element) => {
      const arregloRutaCompleja = element.split('{*}');
      if (
        req.url.indexOf(arregloRutaCompleja[0]) !== -1 &&
        req.url.indexOf(arregloRutaCompleja[1]) !== -1
      ) {
        salir = true;
      }
    });

    if (salir) {
      return next.handle(req);
    }

    this.loadingService.start();
    this.arrayStart.push(req);

    if (!next.handle(req)) {
      return throwError('');
    }

    return next.handle(req).pipe(
      // tap((event: HttpEvent<any>) => {
      //   if (event.type !== 0) {
      //     // Nota: Recordar que las event.type === 0 ocurren antes de recibir la respuesta real de la api, desconozco la razón.
      //     this.stopSiCorresponde(req);
      //   }
      // }),
      // catchError((err: any) => {
      //   this.stopSiCorresponde(req);
      //   return throwError(err);
      // }),
      finalize(() => {
        // this.totalRequests--;
        this.stopSiCorresponde(req);
      })
    );
  }

  stopSiCorresponde(req: HttpRequest<any>) {
    setTimeout(() => {
      this.arrayStart = this.arrayStart.filter((r) => r !== req);
      if (this.arrayStart.length === 0) {
        this.loadingService.stop();
      }
    }, 300);
  }
}
