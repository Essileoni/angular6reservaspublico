import { TestBed, inject } from '@angular/core/testing';

import { RouterExtenderService } from './router-extender.service';

describe('RouterExtenderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouterExtenderService]
    });
  });

  it('should be created', inject([RouterExtenderService], (service: RouterExtenderService) => {
    expect(service).toBeTruthy();
  }));
});
