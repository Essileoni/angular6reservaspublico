import { Injectable } from '@angular/core';
import { Cliente } from 'src/app/empresa/clientes/clientes.model';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Location } from '@angular/common';
import { Reserva, Sucursal } from 'src/app/sucursal/componentes/reservas/reserva-page/reserva.model';
import { Subject } from 'rxjs';
import { SucursalReservas } from 'src/app/sucursal/models/surcursal-reservas.model';

@Injectable({
  providedIn: 'root',
})
export class RouterExtenderService {
  // tslint:disable-next-line: no-inferrable-types
  private previousUrl: string = '';

  // Reserva - Cliente
  public reservaDeReservaACliente: Reserva;
  public clienteDeReservaACliente: Cliente;
  public clienteCreadoDeClienteAReserva: Cliente;
  // END - Reserva - Cliente

  // Grilla - Reserva
  public reservaDeGrillaAReservaPage: Reserva;
  public reservaDeReservaPageAReservaComenzada: Reserva;
  public sucursalIdDeReservaPageAGrilla: number;
  public sucursalIdDeReservaComenzadaAGrilla: number;
  public sucursalIdDeModalPorFinalizarPageAGrilla: number;
  public subjectSucursalIdDeModalPorFinalizarPageAGrilla: Subject<
    number
  > = new Subject<number>();
  public subjectReservaIdSucursalIdDeModalPorFinalizarPageAGrilla: Subject<
  [Sucursal, Reserva]
> = new Subject<[Sucursal, Reserva]>();
  // END - Grilla - Reserva

  constructor(private router: Router, private location: Location) {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationStart) {
        this.previousUrl = this.router.url;
      }
    });
  }

  getPreviousUrl(): string {
    return this.previousUrl.trim().toLowerCase();
  }

  volver() {
    if (this.previousUrl) {
      this.location.back();
    } else {
      this.router.navigate(['/reservas']);
    }
    // this.router.navigate([this.previousUrl || '/reservas']);
  }

  navigateForced(arregloParaMetodoRouterNavigate: any[], previousPath: string) {
    this.router.navigate([previousPath]).then(() => {
      this.router.navigate(arregloParaMetodoRouterNavigate);
    });
  }
}
