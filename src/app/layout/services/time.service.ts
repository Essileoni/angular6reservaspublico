import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as cloneDeep from 'node_modules/lodash/cloneDeep';

@Injectable({
  providedIn: 'root',
})
export class TimeService {
  constructor() {}

  extraerFechaDeUnaFechaYHora(fechaYHora: Date): Date {
    const fechaSinHora = new Date(
      fechaYHora.getFullYear(),
      fechaYHora.getMonth(),
      fechaYHora.getDate(),
      0,
      0,
      0,
      0
    );
    return fechaSinHora;
  }

  sumarRestarDias(fecha: Date, dias: number): Date {
    return new Date(fecha.getTime() + 1000 * 60 * 60 * 24 * dias);
  }

  sumarRestarHoras(fecha: Date, horas: number): Date {
    return new Date(fecha.getTime() + 1000 * 60 * 60 * horas);
  }

  sumarRestarMinutos(fecha: Date, minutos: number): Date {
    return new Date(fecha.getTime() + 1000 * 60 * minutos);
  }

  sumarRestarMilisegundos(fecha: Date, milisegundos: number): Date {
    return new Date(fecha.getTime() + milisegundos);
  }

  obtenerFechaHoySinHora(): Date {
    const ahora = new Date();
    const hoy = new Date(
      ahora.getFullYear(),
      ahora.getMonth(),
      ahora.getDate(),
      0,
      0,
      0,
      0
    );
    return hoy;
  }

  obtenerFechaInicialParaFranjaHoraria() {
    const hoy = this.obtenerFechaHoySinHora();
    const ayer = this.sumarRestarDias(hoy, -1);

    if (this.esMomentoActualAnteriorAlComienzoDelDia()) {
      return ayer;
    } else {
      return hoy;
    }
  }

  obtenerTicksEntreFechas(fecha1: Date, fecha2: Date) {
    let ticks = 0;
    if (fecha1.getTime() > fecha2.getTime()) {
      ticks = fecha1.getTime() - fecha2.getTime();
    } else {
      ticks = fecha2.getTime() - fecha1.getTime();
    }
    return ticks;
  }

  obtenerSegundosEntreFechas(fecha1: Date, fecha2: Date) {
    return this.obtenerTicksEntreFechas(fecha1, fecha2) / 1000;
  }

  obtenerMinutosEntreFechas(fecha1: Date, fecha2: Date) {
    return this.obtenerTicksEntreFechas(fecha1, fecha2) / 1000 / 60;
  }

  obtenerHorasEntreFechas(fecha1: Date, fecha2: Date) {
    return this.obtenerTicksEntreFechas(fecha1, fecha2) / 1000 / 60 / 60;
  }

  obtenerDiasEntreFechas(fecha1: Date, fecha2: Date) {
    return this.obtenerTicksEntreFechas(fecha1, fecha2) / 1000 / 60 / 60 / 24;
  }

  obtenerSemanasEntreFechas(fecha1: Date, fecha2: Date) {
    return this.obtenerTicksEntreFechas(fecha1, fecha2) / 1000 / 60 / 60 / 24 / 7;
  }

  private esMomentoActualAnteriorAlComienzoDelDia() {
    const hoy = this.obtenerFechaHoySinHora();
    const horaComienzoDia = environment.ReservaCrear.comienzoDia / 60;
    const horaComienzoDiaDateTime = cloneDeep(hoy);
    horaComienzoDiaDateTime.setHours(horaComienzoDia);
    const ahora = new Date();
    return ahora.getTime() < horaComienzoDiaDateTime.getTime();
  }
}
