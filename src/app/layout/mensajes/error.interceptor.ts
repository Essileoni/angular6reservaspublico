import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, retry, tap } from 'rxjs/operators';
import { MessageBoxService } from './messagebox.service';
import { Router } from '@angular/router';

import { MessageTipo, MessageFloat, MessageBox } from './message-box.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private messageBoxService: MessageBoxService,
    private router: Router
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap((request: any) => {
        // const responseApi: ResponseApi = request.body;
        // const hashResponse: HashResponse = <HashResponse>request.body;
        // const funcion = result => {
        //   this.router.navigate(['error']);
        // };
        // if (responseApi !== undefined && responseApi.IsOk === false) {
        //   this.aDigitalService.setError(<ErrorAMostrar>{
        //     titulo: 'Lo sentimos',
        //     motivo: responseApi.Message,
        //     accion: 'Solicitá ayuda a tu productor o agencia más cercana'
        //   });

        //   this.aDigitalService.enviarRegistroEventos(funcion);
        // }
        return request;
      }),
      retry(5),
      catchError((httpErrorResponse: HttpErrorResponse) => {
        let mensaje: MessageBox = <MessageBox>{
          Tipo: MessageTipo.Danger,
          Mensaje: ' Reintente repetir la tarea más en unos minutos',
          Strong: 'Falla de comunicación - ',
          Float: MessageFloat.StickyFloat,
          Segundos: 10
        };

        if (httpErrorResponse.status === 401 || httpErrorResponse.status === 403) {
          return throwError(httpErrorResponse);
        }

        if (httpErrorResponse.status === 400) {
          mensaje = <MessageBox>{
            Tipo: MessageTipo.Warning,
            Mensaje: this.extraerMensaje(httpErrorResponse.error),
            Strong: 'Error al procesar la solicitud - ',
            Float: MessageFloat.StickyFloat,
            Segundos: 10
          };
        }

        // // tslint:disable-next-line:no-console
        // console.info(
        //   httpErrorResponse.statusText,
        //   httpErrorResponse.status,
        //   httpErrorResponse
        // );

        if (
          !this.messageBoxService.estaVisible() ||
          !this.messageBoxService.esError()
        ) {
          if (environment.messageBoxHttpErrorActivo) {
            this.messageBoxService.show(mensaje);
          }
        }
        return throwError(httpErrorResponse); // Siempre devuelve el error para lo catchee otro pipe
      })
    );
  }

  extraerMensaje(error: any): string {
    // let mensaje = '';
    // for (const propiedad in error) {
    //   if (error.hasOwnProperty(propiedad)) {
    //     if (
    //       // Si es un arreglo de errores
    //       typeof error[propiedad] === 'object' &&
    //       error[propiedad].length !== undefined
    //     ) {
    //       error[propiedad].forEach(msj => {
    //         mensaje = mensaje + ' - ' + msj;
    //       });
    //     } else {
    //       mensaje = mensaje + ' - ' + error[propiedad];
    //     }
    //   }
    // }

    // if (mensaje.trim().length < 0) {
    //   mensaje = 'Error desconocido';
    // }

    // return mensaje;
    return error.message;
  }
}
