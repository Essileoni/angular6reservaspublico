export enum MessageFloat {
  StickyFloat,
  NotFloat
}

export interface MessageBox {
  Tipo: MessageTipo;
  Mensaje: string;
  Strong: string;
  Float: MessageFloat;
  Segundos: Number;
}


export enum MessageTipo {
  Danger,
  Warning,
  Info,
  Success
}
