import { Injectable } from '@angular/core';
import { MessageBoxComponent } from './message-box.component';
import { Subject, Observable } from 'rxjs';

import { MessageTipo, MessageBox } from './message-box.model';

@Injectable({
  providedIn: 'root'
})
export class MessageBoxService {
  private subject: Subject<MessageBox> = new Subject<MessageBox>();
  private estadoVisibilidad = false;
  private lastMessageBox: MessageBox;
  constructor() {}

  close() {
    this.subject.next(null);
  }

  estaVisible(): Boolean {
    return this.estadoVisibilidad;
  }

  esError(): Boolean {
    return this.lastMessageBox.Tipo === MessageTipo.Danger;
  }

  getObservadorMessages(): Observable<MessageBox> {
    return this.subject.asObservable();
  }

  informarVisibilidad(estaVisible: boolean) {
    this.estadoVisibilidad = estaVisible;
  }

  show(messageBox: MessageBox) {
    this.lastMessageBox = messageBox;
    this.subject.next(messageBox);
  }
}
