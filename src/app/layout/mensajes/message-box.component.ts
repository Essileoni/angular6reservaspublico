import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageBoxService } from './messagebox.service';
import { Subscription } from 'rxjs';

import { Router } from '@angular/router';

import { MessageBox, MessageFloat, MessageTipo } from './message-box.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.css']
})
export class MessageBoxComponent implements OnInit, OnDestroy {
  timeOn: boolean = environment.messageBoxTimeOn;
  private suscripcionMessages: Subscription;
  messageBox: MessageBox;
  mostrar: Boolean = false;

  success: MessageTipo = MessageTipo.Success;
  danger: MessageTipo = MessageTipo.Danger;
  info: MessageTipo = MessageTipo.Info;
  warning: MessageTipo = MessageTipo.Warning;
  stickyFloat: MessageFloat = MessageFloat.StickyFloat;
  intervalo: number;


  constructor(
    private messageBoxService: MessageBoxService,
    private router: Router
  ) {
    // messageBoxService.setComponent(this);
    this.suscripcionMessages = this.messageBoxService
      .getObservadorMessages()
      .subscribe((messageBox: MessageBox) => {
        if (!messageBox) {
          this.mostrar = false;
          return;
        }
        const milisegundo: Number = 1000 * +messageBox.Segundos;
        this.messageBox = messageBox;
        this.mostrar = true;
        this.messageBoxService.informarVisibilidad(true);
        this.registrarEvento(messageBox);
        clearInterval(this.intervalo);
        this.intervalo = +setInterval(() => {
          messageBox.Segundos = +messageBox.Segundos - 1;
          if (+messageBox.Segundos === 0) {
            this.close();
          }
        }, 1000);
      });
  }

  ngOnInit() {
    // this.messageBoxService.show({
    //   Tipo: MessageTipo.Danger,
    //   Mensaje: ' Siga trabajando',
    //   Strong: 'Todo bien - ',
    //   Float: MessageFloat.StickyFloat,
    //   Segundos: 10
    // });

    // setTimeout(() => {
    //   this.messageBoxService.show({
    //     Tipo: MessageTipo.Danger,
    //     Mensaje: ' No siga trabajando',
    //     Strong: 'Todo mal - ',
    //     Float: MessageFloat.StickyFloat,
    //     Segundos: 10
    //   });
    // }, 5000);

    // setTimeout(() => {
    //   this.messageBoxService.show({
    //     Tipo: MessageTipo.Danger,
    //     Mensaje: ' 2 No siga trabajando',
    //     Strong: 'Todo mal - ',
    //     Float: MessageFloat.StickyFloat,
    //     Segundos: 10
    //   });
    // }, 10000);

  }

  close($event?: any) {
    this.mostrar = false;
    setTimeout(() => { this.messageBox = null; }, 2000);
    this.messageBoxService.informarVisibilidad(false);
    clearInterval(this.intervalo);
  }

  registrarEvento(messageBox: MessageBox) {
    const intervalo = setInterval(() => {
      const ele: HTMLElement = document.getElementById('message-box');
      if (ele) {
        clearInterval(intervalo);
      }
    }, 100);
  }

  ngOnDestroy() {
    this.suscripcionMessages.unsubscribe();
  }
}
