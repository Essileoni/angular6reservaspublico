import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerSinInputComponent } from './datepicker-sin-input.component';

describe('DatepickerSinInputComponent', () => {
  let component: DatepickerSinInputComponent;
  let fixture: ComponentFixture<DatepickerSinInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatepickerSinInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerSinInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
