import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TimeService } from '../../services/time.service';

// // Configurar tiempo local
// import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
// import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
// // END - Configurar tiempo local



@Component({
  selector: 'app-datepicker-sin-input',
  templateUrl: './datepicker-sin-input.component.html',
  styleUrls: ['./datepicker-sin-input.component.css'],
  // // Configurar tiempo local
  // providers: [
  //   { provide: MAT_DATE_LOCALE, useValue: 'es_AR' },
  //   { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
  //   { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  // ],
  // // END - Configurar tiempo local
})
export class DatepickerSinInputComponent implements OnInit {

  // TwoWay Binding
  private _fecha: Date;
  @Input() get fecha(): Date {
    return this._fecha;
  }
  @Output() fechaChange: EventEmitter<Date> = new EventEmitter();
  set fecha(fecha: Date) {
    this._fecha = fecha;
    this.fechaChange.emit(this._fecha);
  }
  // END - TwoWay Binding

  @Input() min: number;
  @Input() placeholder: string;
  @Input() matHintLeft: string;

  appearance = environment.matFormFieldAppearance;


  constructor() {
  }

  ngOnInit() {

  }

  onDateInput(datePickerResult: Date, datePickerResultValue: Date) {
    this.fecha = datePickerResult ? datePickerResult : datePickerResultValue;
    this.fechaChange.emit(this.fecha);
  }



}
