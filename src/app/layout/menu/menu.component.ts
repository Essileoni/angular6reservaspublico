import { Component, ViewChild } from '@angular/core';

import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { MenuService } from './menu.service';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { MatSidenavContent } from '@angular/material';
import { Enlace } from '../auth/auth.model';
import { CHILDREN_AUTH_ROUTES } from 'src/app/routing/routes/children-auth.routes';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent {
  @ViewChild('matSidenavContentScroll') matSidenavContentScroll: any;

  title: string = environment.toolbar.title;
  maticon: string = environment.toolbar.materialicon;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map((result) => result.matches));

  esFullScreen = false;
  username = null;
  enlaces: Enlace[] = <Enlace[]>[];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private menuService: MenuService,
    private authService: AuthService
  ) {
    this.runIsFullScreenServices();
    this.findUserName();
    this.generarEnlacesSegurosSegunPermisosParaMenu();
  }

  // LISTENERS
  onActivate() {
    document.getElementById('matSidenavContentScroll').scrollTo(0, 0);
  }

  // PRIVATES METHODS
  private findUserName() {
    const userNameInterval = setInterval(() => {
      if (this.authService.getTokenInfoOrErrorMsg().isOk) {
        this.username = this.authService.getTokenInfoOrErrorMsg().payload.unique_name;
        clearInterval(userNameInterval);
      }
    }, 100);
  }

  private runIsFullScreenServices() {
    this.menuService
      .suscribirFullScreen()
      .subscribe((esFullScreen) => (this.esFullScreen = esFullScreen));
  }

  private generarEnlacesSegurosSegunPermisosParaMenu() {
    const pathPadre = '';

    const enlacesSinSeguridad: Enlace[] = this.procesarVariasRutasOSubrutas(
      CHILDREN_AUTH_ROUTES,
      pathPadre
    );

    const enlacesSeguros: Enlace[] = this.eliminarEnlacesNoSeguros(
      enlacesSinSeguridad
    );

    this.enlaces = enlacesSeguros;
  }

  private machearRolesConToken(roles: string[]): boolean {
    return this.authService.machearRolesConToken(roles);
  }

  private asegurarEnlace(enlaceSinSeguridad: Enlace): Enlace {
    const elEnlaceTieneRolesAVerificar: boolean =
      !!enlaceSinSeguridad.roles && enlaceSinSeguridad.roles.length > 0;
    const haySubEnlacesParaAnalizar: boolean =
      !!enlaceSinSeguridad.subEnlaces &&
      enlaceSinSeguridad.subEnlaces.length > 0;

    if (
      elEnlaceTieneRolesAVerificar &&
      !this.machearRolesConToken(enlaceSinSeguridad.roles)
    ) {
      return null;
    }

    delete enlaceSinSeguridad['roles']; // Eliminar los roles para que no estén visibles

    if (haySubEnlacesParaAnalizar) {
      enlaceSinSeguridad.subEnlaces = this.eliminarEnlacesNoSeguros(
        enlaceSinSeguridad.subEnlaces
      );

      const siAhoraNoExistenSubEnlacesSeguros =
        enlaceSinSeguridad.subEnlaces.length === 0;

      if (siAhoraNoExistenSubEnlacesSeguros) {
        return null;
      }
    }
    const enlaceSeguro = enlaceSinSeguridad; // Si llegó hasta acá pasó todos los filtros necesarios.
    return enlaceSeguro;
  }

  private eliminarEnlacesNoSeguros(enlacesSinSeguridad: Enlace[]): Enlace[] {
    const enlacesSeguros: Enlace[] = <Enlace[]>[];
    enlacesSinSeguridad.forEach((enlace: Enlace) => {
      const enlaceSeguro = this.asegurarEnlace(enlace);
      if (!!enlaceSeguro) {
        enlacesSeguros.push(enlaceSeguro);
      }
    });
    return enlacesSeguros;
  }

  private procesarVariasRutasOSubrutas(
    rutas: any,
    pathPadre: string
  ): Enlace[] {
    let enlaces: Enlace[] = <Enlace[]>[];
    rutas.forEach((ruta: any) => {
      // pathPadre = pathPadre + ruta.path;
      const masEnlaces: Enlace[] = this.procesarRuta(ruta, pathPadre);
      enlaces = enlaces.concat(masEnlaces);
    });
    return enlaces;
  }

  private reemplazarAtributosEnPath(ruta: any): string {
    // Reemplazar parametros de path por valores si existen
    const pathArray = ruta.path.split('/');
    const pathArrayOut = [];
    if (!!ruta.data) {
      pathArray.forEach((p: string, i: number) => {
        if (p.indexOf(':') !== -1 && p[0] === ':') {
          const atributoSinDosPuntos: string = p.replace(':', '');
          const valorParaAtributo: any = ruta.data[atributoSinDosPuntos];
          if (valorParaAtributo === 0 || !!valorParaAtributo) {
            pathArrayOut.push(valorParaAtributo.toString());
          }
        } else {
          pathArrayOut.push(p);
        }
      });
      return pathArrayOut.join('/');
    }
    return ruta.path;
  }

  private procesarRuta(ruta: any, pathPadre: string): Enlace[] {
    let enlaces: Enlace[] = <Enlace[]>[];
    const enlace: Enlace = <Enlace>{
      descripcion: undefined,
      url: undefined,
      subEnlaces: undefined,
    };

    const rutaPathConAtributos = this.reemplazarAtributosEnPath(ruta);

    const nuevaPathPadre =
      (pathPadre ? pathPadre : '') + '/' + rutaPathConAtributos;
    if (!!ruta.data && !!ruta.data.descripcion) {
      enlace.descripcion = ruta.data.descripcion;
      enlace.roles = ruta.data.roles;
      if (!!ruta.component) {
        enlace.url = nuevaPathPadre;
      } else {
        enlace.url = null;
      }
      if (!!ruta.children && ruta.children.length > 0) {
        enlace.subEnlaces = this.procesarVariasRutasOSubrutas(
          ruta.children,
          nuevaPathPadre
        );
        enlace.subEnlaces =
          enlace.subEnlaces.length === 0 ? null : enlace.subEnlaces;
      }
      enlaces.push(enlace);
    } else {
      if (!!ruta.children && ruta.children.length > 0) {
        // La ruta sin descripción se salta por que no va a pertenecer al menú
        enlaces = this.procesarVariasRutasOSubrutas(
          ruta.children,
          nuevaPathPadre
        );
      }
    }

    return enlaces;
  }
}
