import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private fullScreenSubject: Subject<boolean> = new Subject<boolean>();

  constructor() {}

  fullScreen(activar: boolean) {
    this.fullScreenSubject.next(activar);
  }

  suscribirFullScreen(): Observable<boolean> {
    return this.fullScreenSubject.asObservable();
  }
}
