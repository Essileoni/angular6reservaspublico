import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';

import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse
} from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const iso8601JsonDateRegEx = environment.expresionesRegulares.iso8601JsonDateRegEx;

@Injectable()
export class DateInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    if (!next.handle(req)) {
      return throwError('');
    }

    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => {
        if (event.type !== 0 && event instanceof HttpResponse) {
          // Nota: Recordar que las event.type === 0 ocurren antes de recibir la respuesta real de la api, desconozco la razón.
          const respuesta: any = event.body;
          this.convertirLasFechasDeUnObjeto(respuesta);
        }
      }),
      catchError((err: any) => {
        return throwError(err);
      })
    );
  }

  private convertirLasFechasDeUnObjeto(objeto: any) {
    if (objeto === null || objeto === undefined) {
      return objeto;
    }

    if (typeof objeto !== 'object') {
      return objeto;
    }

    if (objeto instanceof Array) {

      objeto.forEach((obj => {
        this.convertirLasFechasDeUnObjeto(obj);
      }));

    } else {

      for (const key of Object.keys(objeto)) {
        const value = objeto[key];
        if (this.esFormatoIso8601(value)) {
          objeto[key] = new Date(value.replace('Z', '').replace('z', '') + 'Z');
        } else if (typeof value === 'object') {
          this.convertirLasFechasDeUnObjeto(value);
        }
      }

    }

  }

  private esFormatoIso8601(value) {
    if (value === null || value === undefined) {
      return false;
    }

    return iso8601JsonDateRegEx.test(value);
  }

}
