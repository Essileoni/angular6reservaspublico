import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TipoFormulario } from './IForm';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(private router: Router) { }

  definirModoFomulario(): TipoFormulario {
    const pathActual = this.router.url;

    // /reservas = [''] + ['/'] + ['reservas']
    if (pathActual.split('/').length === 2) {
      console.log('todos');
      return TipoFormulario.All;
    }

    // /reservas/crear = [''] + ['/'] + ['reservas'] + ['/'] + ['crear']
    if (
      pathActual.split('/').length === 3 &&
      pathActual.split('/')[2] === 'crear'
    ) {
      console.log('crear');
      return TipoFormulario.Create;
    }

    // /reservas/borrar/{id} = [''] + ['/'] + ['reservas'] + ['/'] + ['editar'] + ['/'] + ['id']
    if (
      pathActual.split('/').length === 4 &&
      pathActual.split('/')[2] === 'editar'
    ) {
      console.log('editar');
      return TipoFormulario.Edit;
    }

    // // /reservas/borrar/{id} = [''] + ['/'] + ['reservas'] + ['/'] + ['borrar'] + ['/'] + ['id']
    // if (
    //   pathActual.split('/').length === 4 &&
    //   pathActual.split('/')[2] === 'borrar'
    // ) {
    //   console.log('borrar');
    //   return TipoFormulario.Delete; // <- El borrado se ofrece en edicion
    // }

    // /reservas/{id} = [''] + ['/'] + ['reservas'] + ['/'] + ['id']
    if (pathActual.split('/').length === 3) {
      console.log('detalle');
      return TipoFormulario.Detail;
    }
  }
}
