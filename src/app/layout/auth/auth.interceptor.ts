import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { TokenInfo } from './auth.model';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router, private authService: AuthService) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    if (environment.account.authOff) {
      return next.handle(req);
    }

    for (const notAuthUrl of environment.account.notAuthUrls) {

      if (req.url.indexOf(notAuthUrl) > -1) {
        // Rutas que no se interceptan.
        return next.handle(req);
      }
    }

    const tokenInfo = this.getTokenInfoOrErrorMsg();

    if (tokenInfo.isOk) {
      req = req.clone({
        headers: new HttpHeaders().set('authorization', tokenInfo.authorization)
      });

      return next.handle(req).pipe(
        catchError((err: HttpErrorResponse) => {
          if (err.status === 401 || err.status === 403) {
            this.limpiarTokenRedireccionar(err.status + '');
          }
          return throwError(err);
        })
      );
    } else {
      this.limpiarTokenRedireccionar(tokenInfo.error);
      return next.handle(req);
    }
  }

  limpiarTokenRedireccionar(error: String) {
    window.sessionStorage.removeItem('authorization');
    this.router.navigate(['login', error]);
  }

  getTokenInfoOrErrorMsg(): TokenInfo {
    return this.authService.getTokenInfoOrErrorMsg();
  }
}
