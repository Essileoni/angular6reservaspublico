export interface TokenHeader {
  alg: string;
  typ: string;
}

export interface TokenPayload {
  sub: string;
  unique_name: string;
  sucursalPredeterminadaId: string;
   // Custom Claim
  jti: string;
  iat: string;
  'http://schemas.microsoft.com/ws/2008/06/identity/claims/role': string[];
  nbf: number;
  exp: number;
  iss: string;
  aud: string;
}

export interface TokenInfo {
  header: TokenHeader;
  payload: TokenPayload;
  authorization: string;
  isOk: boolean;
  error: string;
}

export interface Credencials {
  Username: string;
  Password: string;
}

export interface Enlace {
  url: string;
  descripcion: string;
  subEnlaces: Enlace[];
  roles: string[];
}
