import { Component, OnInit, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Credencials } from './auth.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  showSpinner: boolean;
  username = '';
  password = '';

  constructor(private router: Router, private authService: AuthService) {}

  ngOnInit() {}

  login() {
    if (this.username && this.password) {
      this.authService.login(<Credencials>{
        Username: this.username,
        Password: this.password
      });
      // this.showSpinner = true;
    }
  }
}
