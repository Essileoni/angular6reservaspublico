import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  Credencials,
  TokenInfo,
  TokenPayload,
  TokenHeader,
  Enlace,
} from './auth.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RegisterUserModel } from '../user-admin/user-page/user.model';
import { Sucursal } from 'src/app/sucursal/componentes/reservas/reserva-page/reserva.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};
const urlRoles =
  environment.account.urlBase + environment.account.urlRolesEndPoint;
const urlCreateRole =
  environment.account.urlBase + environment.account.urlCreateRoleEndPoint;

const urlCreateUser =
  environment.account.urlBase + environment.account.urlCreateUserEndPoint;
const urlUsers =
  environment.account.urlBase + environment.account.urlUsersEndPoint;
const urlEditUser =
  environment.account.urlBase + environment.account.urlEditUserEndPoint;
const urlDeleteUser =
  environment.account.urlBase + environment.account.urlDeleteUserEndPoint;

const urlGroups =
  environment.account.urlBase + environment.account.urlGroupsEndPoint;
const urlCreateGroup =
  environment.account.urlBase + environment.account.urlCreateGroupEndPoint;
const urlEditGroup =
  environment.account.urlBase + environment.account.urlEditGroupEndPoint;
const urlDeleteGroup =
  environment.account.urlBase + environment.account.urlDeleteGroupEndPoint;

const urlGetSucursalesAdministrables =
  environment.account.urlBase +
  environment.account.urlGetSucursalesAdministrables;

const urlGetSucursalesParaUnRol =
  environment.account.urlBase + environment.account.urlGetSucursalesParaUnRol;

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  login(credencials: Credencials) {
    const urlToken =
      environment.account.urlBase + environment.account.urlLoginEndPoint;
    this.httpClient
      .post(urlToken, credencials, httpOptions)
      .subscribe((token: string) => {
        window.localStorage.setItem('authorization', token);
        const tokenFormateado: TokenInfo = this.getTokenInfoOrErrorMsg();
        if (!tokenFormateado.isOk) {
          this.router.navigate(['login', tokenFormateado.error]);
          return;
        }

        this.router.navigate(['reservas']);
      });
  }

  getTokenInfoOrErrorMsg(): TokenInfo {
    let authorization = window.localStorage.getItem('authorization');
    if (!authorization) {
      return <TokenInfo>{ isOk: false, error: 'localStorageVacio' };
    }

    const authorizationArray: string[] = authorization
      .replace('bearer ', '')
      .trim()
      .split('.');

    if (authorizationArray.length < 3) {
      return <TokenInfo>{ isOk: false, error: 'formatoTokenIncorrecto' };
    }

    const payload: TokenPayload = <TokenPayload>(
      JSON.parse(atob(authorizationArray[1]))
    );

    const fechaExpiracion: Date = new Date(payload.exp * 1000);

    if (fechaExpiracion < new Date()) {
      return <TokenInfo>{ isOk: false, error: 'tokenExpiro' };
    }

    if (authorization.toLowerCase().indexOf('bearer') === -1) {
      authorization = 'bearer ' + authorization.trim();
    }

    const tokenInfo: TokenInfo = <TokenInfo>{
      header: <TokenHeader>JSON.parse(atob(authorizationArray[0])),
      payload: payload,
      authorization: authorization,
      isOk: true,
    };

    return tokenInfo;
  }

  estaAutenticado(): boolean {
    // Que existe en local storage
    const authorization = window.localStorage.getItem('authorization');
    if (!authorization) {
      return false;
    }

    // Que tiene formato valido
    const authorizationArray: string[] = authorization
      .replace('bearer ', '')
      .trim()
      .split('.');
    if (authorizationArray.length < 3) {
      return false;
    }

    // Que es decodificable
    const payload: TokenPayload = <TokenPayload>(
      JSON.parse(atob(authorizationArray[1]))
    );
    if (
      !payload ||
      !payload.unique_name ||
      !payload.exp ||
      !payload['http://schemas.microsoft.com/ws/2008/06/identity/claims/role']
    ) {
      return false;
    }

    // Que no vencio
    const fechaExpiracion: Date = new Date(payload.exp * 1000);
    if (fechaExpiracion < new Date()) {
      return false;
    }

    return true;
  }

  machearRolesConToken(rolesData: string[]): boolean {
    const rolesToken = this.obtenerRolesDelTokenEnLocalStorage();
    this.advertirSiHayRolesStringVaciosNuloOIndefinido(rolesData);
    const rolesFiltrados: string[] = rolesToken.filter(
      (rol: string) => !!rolesData.find((r) => r === rol)
    );

    if (rolesFiltrados.length > 0) {
      return true;
    }
    // Navegar a una pagina o al login
    return false;
  }

  obtenerSucursalPredeterminadaId(): string {
    return this.getTokenInfoOrErrorMsg().payload.sucursalPredeterminadaId;
  }

  obtenerSucursalesAdministrables(): Observable<Sucursal[]> {
    return this.httpClient.get<Sucursal[]>(urlGetSucursalesAdministrables);
  }

  obtenerSucursalesParaUnRol(roleName: string, activarLoading: boolean) {
    return this.httpClient.get<Sucursal[]>(
      urlGetSucursalesParaUnRol +
        (!activarLoading ? 'NotLoading' : '') +
        '/' +
        roleName
    );
  }

  getRoles(): Observable<string[]> {
    return this.httpClient.get<string[]>(urlRoles);
  }

  createRole(role: string): Observable<string> {
    return this.httpClient.post<string>(
      urlCreateRole + '?role=' + role,
      {},
      httpOptions
    );
  }

  getUsers(): Observable<RegisterUserModel[]> {
    return this.httpClient.get<RegisterUserModel[]>(urlUsers);
  }

  createUser(registerModel: RegisterUserModel): Observable<string> {
    return this.httpClient.post<string>(
      urlCreateUser,
      registerModel,
      httpOptions
    );
  }

  editUser(editUserModel: RegisterUserModel): Observable<string> {
    return this.httpClient.patch<string>(
      urlEditUser,
      editUserModel,
      httpOptions
    );
  }

  deleteUser(username: string): Observable<string> {
    return this.httpClient.delete<string>(
      urlDeleteUser + '/' + username,
      httpOptions
    );
  }

  getGroups(): Observable<RegisterGroupModel[]> {
    return this.httpClient.get<RegisterGroupModel[]>(urlGroups);
  }

  createGroup(registerGroupModel: RegisterGroupModel): Observable<string> {
    // Asegurando que el id no viaje a la api
    registerGroupModel = <RegisterGroupModel>{
      name: registerGroupModel.name,
      description: registerGroupModel.description,
    };
    return this.httpClient.post<string>(
      urlCreateGroup,
      registerGroupModel,
      httpOptions
    );
  }

  editGroup(editGroupModel: RegisterGroupModel): Observable<string> {
    return this.httpClient.patch<string>(
      urlEditGroup,
      editGroupModel,
      httpOptions
    );
  }

  deleteGroup(id: string): Observable<string> {
    return this.httpClient.delete<string>(
      urlDeleteGroup + '/' + id,
      httpOptions
    );
  }

  // Metodos PRIVADOS

  private obtenerRolesDelTokenEnLocalStorage() {
    return this.getTokenInfoOrErrorMsg().payload[
      'http://schemas.microsoft.com/ws/2008/06/identity/claims/role'
    ];
  }

  private advertirSiHayRolesStringVaciosNuloOIndefinido(roles: string[]) {
    roles.forEach((rol: string) => {
      const esUnRolStringVacio =
        rol !== undefined && rol !== null && rol.trim() === '';
      if (esUnRolStringVacio) {
        // tslint:disable-next-line: no-console
        console.info(
          'Warning: auth.services.ts -> machearRolesConToken(): Cuidado alguno de los roles es un string vacio' +
            ', revise los archivos de rutas del proyecto'
        );
      }

      const esUnRolNuloIndefinido = rol === undefined || rol === null;
      if (esUnRolNuloIndefinido) {
        // tslint:disable-next-line: no-console
        console.info(
          'Warning: auth.services.ts -> machearRolesConToken(): Cuidado alguno de los roles es un nulo o undefined' +
            ', revise los archivos de rutas del proyecto'
        );
      }
    });
  }
}
