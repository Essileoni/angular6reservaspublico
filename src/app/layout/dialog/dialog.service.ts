import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

import { Reserva } from 'src/app/sucursal/componentes/reservas/reserva-page/reserva.model';

import { ClienteReserva } from 'src/app/sucursal/models/cliente-reserva.model';
import { BorrarReservaDialogComponent } from 'src/app/sucursal/componentes/reservas/dialog/borrar-reserva-dialog/borrar-reserva-dialog.component';
import { ClientePagaReservaDialogComponent } from 'src/app/sucursal/componentes/reservas/dialog/cliente-paga-reserva-dialog/cliente-paga-reserva-dialog.component';
import { SucursalReservas } from 'src/app/sucursal/models/surcursal-reservas.model';
import { ListadoSucursalesReservasDialogComponent } from 'src/app/sucursal/componentes/reservas/dialog/listado-sucursales-reservas-dialog/listado-sucursales-reservas-dialog.component';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  // Recordar registrar el dialog en app.module.ts
  constructor(private dialog: MatDialog) {}

  openConfirmDialog(msg: string) {
    return this.dialog.open(ConfirmDialogComponent, {
      width: '390px',
      disableClose: true,
      position: { top: '65px' },
      data: {
        message: msg,
      },
    });
  }

  openListadoSucursalesReservas(titulo: string, sucursalesReservas: SucursalReservas[]) {
    return this.dialog.open(ListadoSucursalesReservasDialogComponent, {
      width: '390px',
      disableClose: true,
      position: { top: '65px' },
      data: {
        titulo: titulo,
        sucursalesReservas: sucursalesReservas
      },
    });
  }

  openBorrarReservaDialog(reserva: Reserva) {
    return this.dialog.open(BorrarReservaDialogComponent, {
      width: '390px',
      height: 'calc(80% - 65px)',
      disableClose: true,
      position: { top: '65px' },
      data: {
        reserva: reserva,
      },
    });
  }

  openBorrarClientePagaReservaDialog(
    reserva: Reserva,
    clienteReserva: ClienteReserva,
    sucursalId: number
  ) {
    return this.dialog.open(ClientePagaReservaDialogComponent, {
      width: '390px',
      disableClose: true,
      position: { top: '65px' },
      data: {
        reserva: reserva,
        clienteReserva: clienteReserva,
        sucursalId: sucursalId,
      },
    });
  }
}
