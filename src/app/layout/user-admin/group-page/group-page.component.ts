import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { FormMode } from '../../common/form.model';
import { AuthService } from '../../auth/auth.service';
import { MessageBoxService } from '../../mensajes/messagebox.service';
import {
  MessageBox,
  MessageTipo,
  MessageFloat
} from '../../mensajes/message-box.model';

@Component({
  selector: 'app-group-page',
  templateUrl: './group-page.component.html',
  styleUrls: ['./group-page.component.css']
})
export class GroupPageComponent implements OnInit {
  fuenteDatos: MatTableDataSource<RegisterGroupModel> = new MatTableDataSource(
    []
  );
  listMode = true;
  emptyModel: RegisterGroupModel = <RegisterGroupModel>{
    id: '',
    description: '',
    name: ''
  };
  registerModel: RegisterGroupModel = this.emptyModel;

  displayedColumns: string[] = ['Nombre', 'Descripcion'];
  formCreate: FormMode = FormMode.Create;
  formEdit: FormMode = FormMode.Edit;
  formDelete: FormMode = FormMode.Delete;

  formMode: FormMode = this.formCreate;

  constructor(
    private authServices: AuthService,
    private messageBoxService: MessageBoxService
  ) {}

  ngOnInit() {
    this.getGroups();
  }

  getGroups() {
    this.authServices.getGroups().subscribe((groups: RegisterGroupModel[]) => {
      this.fuenteDatos = new MatTableDataSource(groups);
    });
  }

  filtrar(filtro: string) {
    this.fuenteDatos.filter = filtro.trim().toLowerCase();
  }

  goAddGroup() {
    this.listMode = false;
    this.formMode = this.formCreate;
    this.registerModel = this.emptyModel;
  }

  addGroup() {
    for (const propiedad in this.registerModel) {
      if (
        propiedad !== 'id' &&
        this.registerModel[propiedad].trim().length === 0
      ) {
        this.setMessageBoxFields();
        return;
      }
    }

    this.authServices.createGroup(this.registerModel).subscribe(respuesta => {
      this.getGroups();
      this.volver();
    });
  }

  goEditGroup(grupo: RegisterGroupModel) {
    this.registerModel = <RegisterGroupModel>grupo;
    this.formMode = FormMode.Edit;
    this.listMode = false;
  }

  editGroup() {

    for (const propiedad in <RegisterGroupModel>this.registerModel) {
      if (
        propiedad !== 'applicationRoles' &&
        propiedad !== 'applicationUsers' &&
        this.registerModel[propiedad].trim().length === 0
      ) {
        this.setMessageBoxFields();
        return;
      }
    }

    this.authServices.editGroup(this.registerModel).subscribe(respuesta => {
      this.getGroups();
      this.volver();
    });
  }

  deleteGroup() {
    this.authServices
      .deleteGroup(this.registerModel.id)
      .subscribe(respuesta => {
        this.getGroups();
        this.volver();
      });
  }

  setMessageBoxFields() {
    this.messageBoxService.show(<MessageBox>{
      Tipo: MessageTipo.Warning,
      Mensaje: 'Completá los campos requeridos',
      Strong: 'Cuidado - ',
      Float: MessageFloat.StickyFloat,
      Segundos: 10
    });
  }

  volver() {
    this.listMode = true;
  }
}
