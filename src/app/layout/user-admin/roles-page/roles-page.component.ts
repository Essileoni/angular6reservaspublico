import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-roles-page',
  templateUrl: './roles-page.component.html',
  styleUrls: ['./roles-page.component.css']
})
export class RolesPageComponent implements OnInit {
  fuenteDatos: MatTableDataSource<string> = new MatTableDataSource([]);
  displayedColumns: string[] = ['Descripcion'];

  listMode = true;
  descripcion = '';

  constructor(private authServices: AuthService) {}

  ngOnInit() {
    this.getRoles();
  }

  private getRoles() {
    this.authServices.getRoles().subscribe((roles: string[]) => {
      this.fuenteDatos = new MatTableDataSource(roles);
    });
  }

  filtrar(filtro: string) {
    this.fuenteDatos.filter = filtro.trim().toLowerCase();
  }

  goAddRol() {
    this.listMode = false;
    this.descripcion = '';
  }

  addRol() {
    if (this.descripcion) {
      this.authServices.createRole(this.descripcion).subscribe(respuesta => {
        this.getRoles();
        this.volver();
      });
    }
  }

  volver() {
    this.listMode = true;
  }
}
