export interface EditUserModel {
  firstName: string;
  lastName: string;
  email: string;
  username: string;
}

export interface RegisterUserModel extends EditUserModel {
  passwordConfirmation: string;
  password: string;
}
