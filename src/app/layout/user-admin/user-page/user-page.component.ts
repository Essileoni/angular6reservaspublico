import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { AuthService } from '../../auth/auth.service';
import { RegisterUserModel, EditUserModel } from './user.model';
import { MessageBoxService } from '../../mensajes/messagebox.service';
import {
  MessageTipo,
  MessageFloat,
  MessageBox
} from '../../mensajes/message-box.model';
import { FormMode } from '../../common/form.model';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {
  fuenteDatos: MatTableDataSource<EditUserModel> = new MatTableDataSource([]);
  displayedColumns: string[] = ['Usuarios'];

  listMode = true;
  emptyModel: RegisterUserModel = <RegisterUserModel>{
    firstName: '',
    lastName: '',
    email: '',
    passwordConfirmation: '',
    username: '',
    password: ''
  };
  registerModel: RegisterUserModel = this.emptyModel;

  formCreate: FormMode = FormMode.Create;
  formEdit: FormMode = FormMode.Edit;
  formDelete: FormMode = FormMode.Delete;

  formMode: FormMode = this.formCreate;

  constructor(
    private authServices: AuthService,
    private messageBoxService: MessageBoxService
  ) {}

  ngOnInit() {
    this.getUsers();
  }

  private getUsers() {
    this.authServices.getUsers().subscribe((users: EditUserModel[]) => {
      this.fuenteDatos = new MatTableDataSource(users);
    });
  }

  filtrar(filtro: string) {
    this.fuenteDatos.filter = filtro.trim().toLowerCase();
  }

  goAddUser() {
    this.listMode = false;
    this.formMode = this.formCreate;
    this.registerModel = this.emptyModel;
  }

  addUser() {
    for (const propiedad in this.registerModel) {
      if (this.registerModel[propiedad].trim().length === 0) {
        this.setMessageBoxFields();
        return;
      }
    }

    if (
      this.registerModel.password !== this.registerModel.passwordConfirmation
    ) {
      this.messageBoxService.show(<MessageBox>{
        Tipo: MessageTipo.Warning,
        Mensaje: 'La contraseña y su confirmación no coinciden.',
        Strong: 'Cuidado - ',
        Float: MessageFloat.StickyFloat,
        Segundos: 10
      });
      return;
    }

    this.authServices.createUser(this.registerModel).subscribe(respuesta => {
      this.getUsers();
      this.volver();
    });
  }

  goEditUser(usuario: EditUserModel) {
    this.registerModel = <RegisterUserModel>usuario;
    // this.registerModel.password = '';
    // this.registerModel.passwordConfirmation = '';
    this.formMode = FormMode.Edit;
    this.listMode = false;
  }

  editUser() {
    for (const propiedad in <EditUserModel>this.registerModel) {
      if (this.registerModel[propiedad].trim().length === 0) {
        this.setMessageBoxFields();
        return;
      }
    }

    this.authServices.editUser(this.registerModel).subscribe(respuesta => {
      this.getUsers();
      this.volver();
    });
  }

  deleteUser() {
    this.authServices.deleteUser(this.registerModel.username).subscribe(respuesta => {
      this.getUsers();
      this.volver();
    });
  }

  setMessageBoxFields() {
    this.messageBoxService.show(<MessageBox>{
      Tipo: MessageTipo.Warning,
      Mensaje: 'Completá los campos requeridos',
      Strong: 'Cuidado - ',
      Float: MessageFloat.StickyFloat,
      Segundos: 10
    });
  }

  volver() {
    this.listMode = true;
  }
}
