import { RolesPageComponent } from 'src/app/layout/user-admin/roles-page/roles-page.component';
import { RoleGuardService } from '../guards/role-guard.service';
import { UserPageComponent } from 'src/app/layout/user-admin/user-page/user-page.component';
import { GroupPageComponent } from 'src/app/layout/user-admin/group-page/group-page.component';
import { environment } from 'src/environments/environment';
import { GrillaComponent } from 'src/app/sucursal/componentes/reservas/grilla/grilla.component';
import { ReservaComenzadaPageComponent } from 'src/app/sucursal/componentes/reservas/reserva-comenzada-page/reserva-comenzada-page.component';
import { ReservaPageComponent } from 'src/app/sucursal/componentes/reservas/reserva-page/reserva-page.component';
import { UnidadesReservasComponent } from 'src/app/sucursal/componentes/unidades-reservas/unidades-reservas/unidades-reservas.component';
import { UnidadReservaComponent } from 'src/app/sucursal/componentes/unidades-reservas/unidad-reserva/unidad-reserva.component';
import { ClientePageComponent } from 'src/app/empresa/clientes/cliente-page/cliente-page.component';

// Reglas de renderización del menú.
// Solo van a existir dos niveles, encabezado y un nivel inferior.
// Solo apareceran en el menu aquellas rutas que tengan data:{descripcion:'Una Descripcion'}
// Una ruta con descripcion pero sin componente no será un link, solo una descripcion o encabezado.
// Solo se mostratan rutas sin roles cuando se validen la existencia de algunos de los roles de la subruta.
// Si una ruta tiene un rol, y es no valida, la ruta no aparece en el menú.
// Las rutas se arman concatenando todos los path de los elementos que tienen descripcion,
//    siempre y cuando una ruta final tenga un componente asociado.
// Las rutas sin descricion no se renderizan en el menú.

export const CHILDREN_AUTH_ROUTES = [
  {
    path: 'test0',
    data: { roles: [], descripcion: 'TEST0' },
    children: [
      {
        path: 'test1',
        component: RolesPageComponent,
        canActivate: [RoleGuardService],
        data: {
          roles: ['AccountControllerGetAllRoles2'],
          descripcion: 'Test1',
        },
      },
    ],
  },
  {
    path: 'admin',
    data: { roles: [], descripcion: 'Permisos' },
    children: [
      {
        path: 'roles',
        component: RolesPageComponent,
        canActivate: [RoleGuardService],
        data: {
          roles: ['AccountControllerGetAllRoles'],
          descripcion: 'Roles',
        },
      },
      {
        path: 'usuarios',
        component: UserPageComponent,
        canActivate: [RoleGuardService],
        data: { roles: ['AccountControllerGetUsers'], descripcion: 'Usuarios' },
      },
      {
        path: 'grupos',
        component: GroupPageComponent,
        canActivate: [RoleGuardService],
        data: {
          roles: ['AccountControllerGetAllGroups'],
          descripcion: 'Grupos',
        },
      },
    ],
  },
  {
    path: '',
    data: { roles: [], descripcion: 'Sucursales' },
    children: [
      {
        path: 'reservas',
        component: GrillaComponent,
        data: {
          roles: ['SucursalControllerModificarReserva'],
          descripcion: 'Reservas',
        },
        children: [
          { path: ':fecha', component: GrillaComponent },
          { path: ':fecha/:senia', component: GrillaComponent },
        ],
      },

      {
        path: ':idSucursal/reserva',
        children: [
          {
            path: 'comenzada',
            children: [
              { path: ':id', component: ReservaComenzadaPageComponent },
            ],
          },
          { path: ':id', component: ReservaPageComponent },
          { path: '', component: ReservaPageComponent },
        ],
      },

      {
        path: 'unidadesreservas',
        component: UnidadesReservasComponent,
        data: {
          roles: ['SucursalControllerModificarUnidadReserva'],
          descripcion: environment.Menu.unidadesreservastext,
        },
      },

      {
        path: ':idSucursal/unidadreserva',
        data: {
          idSucursal: 0,
        },
        children: [
          {
            path: ':id',
            component: UnidadReservaComponent,
            data: {
              roles: ['SucursalControllerModificarUnidadReserva'],
            },
          },
          {
            path: '',
            component: UnidadReservaComponent,
            data: {
              roles: ['SucursalControllerCrearUnidadReserva'],
              descripcion: environment.Menu.unidadreservanuevatext,
            },
          },
        ],
      },
    ],
  },
  {
    path: 'sucursales',
    data: { roles: [], descripcion: 'Administración' },
    children: [
      {
        path: 'cliente',
        children: [
          {
            path: ':id',
            component: ClientePageComponent,
            data: { roles: [], descripcion: 'Editar Cliente', id: 0 },
          },
          {
            path: '',
            component: ClientePageComponent,
            data: { roles: [], descripcion: 'Cliente Nuevo' },
          },
        ],
      },
    ],
  },
];
