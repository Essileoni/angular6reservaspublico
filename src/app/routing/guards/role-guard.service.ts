import { Injectable } from "@angular/core";
import { AuthService } from "src/app/layout/auth/auth.service";
import {
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class RoleGuardService {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    router: Router
  ): Observable<boolean> | Promise<boolean> | boolean {
    const rolesData = next.data.roles;
    const tieneRolesEnToken = this.authService.machearRolesConToken(rolesData);
    const hayQueRedireccionar = !tieneRolesEnToken;
    if (hayQueRedireccionar) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
