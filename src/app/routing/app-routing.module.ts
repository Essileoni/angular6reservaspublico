import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../layout/auth/login.component';
import { MenuComponent } from '../layout/menu/menu.component';
import { TestComponent } from '../test/test.component';
import { CHILDREN_AUTH_ROUTES } from './routes/children-auth.routes';
import { AuthGuardService } from './guards/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'reservas', pathMatch: 'full' },

  {
    path: 'login',
    children: [
      { path: '', component: LoginComponent },
      { path: ':error', component: LoginComponent },
    ],
  },

  { path: 'test', component: TestComponent },

  {
    path: '',
    component: MenuComponent,
    canActivate: [AuthGuardService],
    children: CHILDREN_AUTH_ROUTES,
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
