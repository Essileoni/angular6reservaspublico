import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from '../layout/menu/menu.component';

import { AuthGuardService } from './guards/auth-guard.service';
import { CHILDREN_AUTH_ROUTES } from './routes/children-auth.routes';

const authRoutes: Routes = [
  {
    path: '',
    component: MenuComponent,
    canActivate: [AuthGuardService],
    children: CHILDREN_AUTH_ROUTES,
  },
];

@NgModule({
  imports: [RouterModule.forChild(authRoutes)],
  exports: [RouterModule],
  providers: [AuthGuardService],
})
export class AppAuthRoutingModule {}
