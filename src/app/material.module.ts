import { NgModule } from '@angular/core';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatCardModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatProgressSpinnerModule,
  MatInputModule,
  MatSelectModule,
  MatNativeDateModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';
import { MatRadioModule } from '@angular/material/radio';
import { MatTableModule } from '@angular/material/table';
import { LayoutModule } from '@angular/cdk/layout';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSliderModule } from '@angular/material/slider';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfirmDialogComponent } from './layout/dialog/confirm-dialog/confirm-dialog.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatExpansionModule } from '@angular/material/expansion';

// https://www.npmjs.com/package/ngx-material-timepicker
// +  yarn add ngx-material-timepicker@3.3.1
// + https://agranom.github.io/ngx-material-timepicker/
@NgModule({
  imports: [
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatInputModule,
    LayoutModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSliderModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatTooltipModule,
    MatStepperModule,
    NgxMaterialTimepickerModule,
    MatExpansionModule
  ],
  exports: [
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatInputModule,
    LayoutModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSliderModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatTooltipModule,
    MatStepperModule,
    NgxMaterialTimepickerModule,
    MatExpansionModule
  ],
  providers: [MatDatepickerModule],
  entryComponents: [ConfirmDialogComponent],
})
export class MaterialModule {}
