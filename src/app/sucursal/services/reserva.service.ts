import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as cloneDeep from 'lodash/cloneDeep';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Cliente } from 'src/app/empresa/clientes/clientes.model';
import { TimeService } from 'src/app/layout/services/time.service';

import { PrecioPorHora } from '../models/precio-por-hora.model';

import {
  Reserva,
  MotivoReserva,
  Sucursal,
} from '../componentes/reservas/reserva-page/reserva.model';
import { ClienteReserva } from '../models/cliente-reserva.model';
import { UnidadReserva } from '../models/unidad-reserva.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

const urlBaseCreate =
  environment.ReservaCrear.urlBase + environment.ReservaCrear.pathReservaCreate;

const urlBaseModify =
  environment.ReservaCrear.urlBase + environment.ReservaCrear.pathReservaModify;

const urlBaseChangeState =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathReservaChangeState;

const urlBaseDelete =
  environment.ReservaCrear.urlBase + environment.ReservaCrear.pathReservaDelete;

const urlBaseMotivosReservaList =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathMotivosReservaList;

const urlBaseUnidadesReservaList =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathUnidadesReservaList;

const urlUnidadReservaGet =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathUnidadReservaGet;

const pathBaseUnidadReservaEdit =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathUnidadReservaEdit;

const pathBaseUnidadReservaCreate =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathUnidadReservaCreate;

const urlBasePrecioPorHoraList =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathPrecioPorHoraList;

const pathBasePrecioPorHoraCreate =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathPrecioPorHoraCreate;

const pathBasePrecioPorHoraEdit =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathPrecioPorHoraEdit;

const pathBasePrecioPorHoraRemove =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathPrecioPorHoraRemove;

const urlBaseReservasParaUnaFecha =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathReservasParaUnaFecha;

const urlBaseReservasEliminadasParaUnaFecha =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathReservasEliminadasParaUnaFecha;

const urlBaseGetReserva =
  environment.ReservaCrear.urlBase + environment.ReservaCrear.pathReservaGet;

const urlBaseGetReservaEstaLiberadaParaUnaFechaHoraCancha =
  environment.ReservaCrear.urlBase +
  environment.ReservaCrear.pathGetReservaEstaLiberadaParaUnaFechaHoraCancha;

const urlGetSucursales =
  environment.empresaController.urlBase +
  environment.empresaController.pathGetSucursales;

const comienzoMinutosHora = environment.ReservaCrear.comienzoDia;

const diasSemana: string[] = cloneDeep(environment.ReservaCrear.diasSemana);

@Injectable({
  providedIn: 'root',
})
export class ReservaService {
  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private timeService: TimeService
  ) { }

  // FUNCIONES PUBLICAS
  obtenerFechaGrillaDeUnaReserva(reserva: Reserva): Date {
    const hora =
      reserva.fechaHoraComienzo.getHours() +
      reserva.fechaHoraComienzo.getMinutes() / 60;
    const horaReservaEnMinutos = this.calcularhoraReservaEnMinutos(
      reserva.fechaHoraComienzo
    );
    const esDiaAnterior = hora < horaReservaEnMinutos / 60;
    const fechaHoraComienzo = reserva.fechaHoraComienzo;

    if (esDiaAnterior) {
      const f: Date = this.timeService.sumarRestarDias(fechaHoraComienzo, -1);
      // return new Date(f.getFullYear(), f.getMonth(), f.getDate(), 0, 0, 0, 0);
      return this.timeService.extraerFechaDeUnaFechaYHora(f);
    }
    return this.timeService.extraerFechaDeUnaFechaYHora(fechaHoraComienzo);
  }

  calcularhoraReservaEnMinutos(fecha: Date) {
    if (!fecha) {
      return 0;
    }
    return fecha.getHours() * 60 + fecha.getMinutes();
  }

  // Calcula el importe y modifica la reserva.
  calcularImporte(reserva: Reserva, preciosPorHora: PrecioPorHora[]) {
    if (!this.validadAntesDeCalcularImporte(reserva, preciosPorHora)) {
      return;
    }
    this.generarHorariosAdaptados(preciosPorHora);
    this.ordernarPreciosPorHora(preciosPorHora);
    this.calcularTotalImporteUnidadReservada(reserva, preciosPorHora);
    return;
  }

  noFaltaNadiePorPagar(reserva: Reserva) {
    const clienteImpago = reserva.clientesReserva.find((c: ClienteReserva) => {
      return !this.clienteReservaPago(c, reserva);
    });
    return !clienteImpago;
  }

  clienteReservaPago(cR: ClienteReserva, reserva): boolean {
    return (
      cR.estadoCumplimiento === 'Cumplió' &&
      cR.importeUnidadReservaParte ===
      reserva.importeUnidadReservada / reserva.clientesReserva.length
    );
  }

  crear(reserva: Reserva, sucursalId: number): Observable<number> {
    const urlCreateParaUnaSucursal = urlBaseCreate.replace(
      environment.ReservaCrear.stringToReplace,
      sucursalId.toString()
    );
    return this.httpClient.post<number>(
      urlCreateParaUnaSucursal,
      this.preperarReserva(reserva),
      httpOptions
    );
  }

  cambiarEstadoReserva(reserva: Reserva, sucursalId: number) {
    const urlChangeStateParaUnaSucursal =
      urlBaseChangeState.replace(
        environment.ReservaCrear.stringToReplace,
        sucursalId.toString()
      ) +
      '/' +
      reserva.id;
    return this.httpClient.patch<void>(
      urlChangeStateParaUnaSucursal,
      this.preperarReserva(reserva),
      httpOptions
    );
  }

  modificar(reserva: Reserva, sucursalId: number): Observable<void> {
    const urlModifyParaUnaSucursal =
      urlBaseModify.replace(
        environment.ReservaCrear.stringToReplace,
        sucursalId.toString()
      ) +
      '/' +
      reserva.id;
    return this.httpClient.patch<void>(
      urlModifyParaUnaSucursal,
      this.preperarReserva(reserva),
      httpOptions
    );
  }

  eliminarReserva(reserva: Reserva, sucursalId: number) {
    const urlDeleteParaUnaSucursal = urlBaseDelete.replace(
      environment.ReservaCrear.stringToReplace,
      sucursalId.toString()
    );
    return this.httpClient.post<number>(
      urlDeleteParaUnaSucursal + '/' + reserva.id,
      reserva,
      httpOptions
    );
  }

  obtenerMotivosReserva(sucursalId: number): Observable<MotivoReserva[]> {
    const urlMotivosReservaSucursalList = urlBaseMotivosReservaList.replace(
      environment.ReservaCrear.stringToReplace,
      sucursalId.toString()
    );
    return this.httpClient.get<MotivoReserva[]>(urlMotivosReservaSucursalList);
  }

  obtenerUnidadesReserva(sucursal: Sucursal): Observable<UnidadReserva[]> {
    const urlUnidadesReservaSucursalList = urlBaseUnidadesReservaList.replace(
      environment.ReservaCrear.stringToReplace,
      sucursal.id.toString()
    );
    return this.httpClient.get<UnidadReserva[]>(urlUnidadesReservaSucursalList);
  }

  // Funcion Reemplazada: Esta funcion trae reservas de más y no trae todas las reservas eliminadas.
  obtenerReservasParaUnaFechaYUnidadesDeReservaBAK(
    fechaReserva: Date,
    sucursal: Sucursal
  ): Observable<[Reserva[], Reserva[], UnidadReserva[]]> {
    const fechaReservaProximoDia: Date = this.timeService.sumarRestarDias(
      fechaReserva,
      1
    );

    return forkJoin([
      this.ObtenerReservasParaUnaFecha(fechaReserva, sucursal, true),
      this.ObtenerReservasParaUnaFecha(fechaReservaProximoDia, sucursal, true),
      this.obtenerReservasEliminadasParaUnaFecha(fechaReserva, sucursal),
      this.obtenerUnidadesReserva(sucursal),
    ]).pipe(
      map((arreglo: [Reserva[], Reserva[], Reserva[], UnidadReserva[]]) => {
        return <[Reserva[], Reserva[], UnidadReserva[]]>[
          arreglo[0].concat(arreglo[1]),
          arreglo[2],
          arreglo[3],
        ];
      })
    );
  }

  obtenerReservasParaUnaFechaUnaUnidadesDeReservaYUnaSucursal(
    fechaReserva: Date,
    sucursal: Sucursal
  ): Observable<[Reserva[], Reserva[], UnidadReserva[]]> {
    const fechaReservaProximoDia: Date = this.timeService.sumarRestarDias(
      fechaReserva,
      1
    );
    return forkJoin([
      this.ObtenerReservasParaUnaFecha(fechaReserva, sucursal, true),
      this.ObtenerReservasParaUnaFecha(fechaReservaProximoDia, sucursal, true),
      this.obtenerReservasEliminadasParaUnaFecha(fechaReserva, sucursal),
      this.obtenerReservasEliminadasParaUnaFecha(
        fechaReservaProximoDia,
        sucursal
      ),
      this.obtenerUnidadesReserva(sucursal),
    ]).pipe(
      map(
        (
          arreglo: [Reserva[], Reserva[], Reserva[], Reserva[], UnidadReserva[]]
        ) => {
          const reservas = arreglo[0].concat(arreglo[1]);
          const reservasEliminadas = arreglo[2].concat(arreglo[3]);
          const unidadesReserva = arreglo[4];
          const reservasDeLaJornada: Reserva[] = this.limpiarReservasParaFechaJornada(
            fechaReserva,
            reservas
          );
          const reservasEliminadasDeLaJornada: Reserva[] = this.limpiarReservasParaFechaJornada(
            fechaReserva,
            reservasEliminadas
          );
          return <[Reserva[], Reserva[], UnidadReserva[]]>[
            reservasDeLaJornada,
            reservasEliminadasDeLaJornada,
            unidadesReserva,
          ];
        }
      )
    );
  }

  obtenerMatrizPreciosUnidadReserva(
    idUnidadReserva: number,
    sucursalId: number
  ): Observable<PrecioPorHora[]> {
    const urlPrecioPorHoraSucursalList =
      urlBasePrecioPorHoraList.replace(
        environment.ReservaCrear.stringToReplace,
        sucursalId.toString()
      ) +
      '/' +
      idUnidadReserva;
    return this.httpClient.get<PrecioPorHora[]>(urlPrecioPorHoraSucursalList);
  }

  obtenerUnidadReserva(
    id: number,
    sucursal: Sucursal
  ): Observable<UnidadReserva> {
    const urlUnidadReservaParaSucursalGet =
      urlUnidadReservaGet.replace(
        environment.ReservaCrear.stringToReplace,
        sucursal.id.toString()
      ) +
      '/' +
      id;
    return this.httpClient.get<UnidadReserva>(urlUnidadReservaParaSucursalGet);
  }

  modificarUnidadReserva(
    unidadDeReserva: UnidadReserva,
    sucursal: Sucursal
  ): Observable<UnidadReserva> {
    const pathUnidadReservaEditConSucursal =
      pathBaseUnidadReservaEdit.replace(
        environment.ReservaCrear.stringToReplace,
        sucursal.id.toString()
      ) +
      '/' +
      unidadDeReserva.id.toString();

    const unidadDeReservaEnvio: UnidadReserva = cloneDeep(unidadDeReserva);
    unidadDeReservaEnvio.preciosPorHora = null;

    return this.httpClient.patch<UnidadReserva>(
      pathUnidadReservaEditConSucursal,
      unidadDeReservaEnvio,
      httpOptions
    );
  }

  crearUnidadReserva(
    unidadDeReserva: UnidadReserva,
    sucursal: Sucursal
  ): Observable<number> {
    const pathUnidadReservaCreateConSucursal = pathBaseUnidadReservaCreate.replace(
      environment.ReservaCrear.stringToReplace,
      sucursal.id.toString()
    );
    const unidadDeReservaEnvio: UnidadReserva = cloneDeep(unidadDeReserva);
    unidadDeReservaEnvio.preciosPorHora = null;
    unidadDeReservaEnvio.id = 0;
    return this.httpClient.post<number>(
      pathUnidadReservaCreateConSucursal,
      unidadDeReservaEnvio,
      httpOptions
    );
  }

  crearPrecioPorHora(
    precioPorHora: PrecioPorHora,
    sucursalId: number
  ): Observable<number> {
    const pathPrecioPorHorSucursalCreate = pathBasePrecioPorHoraCreate.replace(
      environment.ReservaCrear.stringToReplace,
      sucursalId.toString()
    );
    precioPorHora.id = 0;
    return this.httpClient.post<number>(
      pathPrecioPorHorSucursalCreate,
      precioPorHora,
      httpOptions
    );
  }

  modificarPrecioPorHora(
    precioPorHora: PrecioPorHora,
    sucursalId: number
  ): Observable<PrecioPorHora> {
    const pathPrecioPorHoraSucursalEdit = pathBasePrecioPorHoraEdit.replace(
      environment.ReservaCrear.stringToReplace,
      sucursalId.toString()
    );
    return this.httpClient.patch<PrecioPorHora>(
      pathPrecioPorHoraSucursalEdit + '/' + precioPorHora.id,
      precioPorHora,
      httpOptions
    );
  }

  eliminarPrecioPorHora(
    precioPorHora: PrecioPorHora,
    sucursalId: number
  ): Observable<void> {
    const pathPrecioPorHoraSucursalRemove = pathBasePrecioPorHoraRemove.replace(
      environment.ReservaCrear.stringToReplace,
      sucursalId.toString()
    );
    return this.httpClient.delete<void>(
      pathPrecioPorHoraSucursalRemove + '/' + precioPorHora.id,
      httpOptions
    );
  }

  getReservaVacia(): Reserva {
    return <Reserva>{
      id: 0,
      importeUnidadReservada: 0,
      seniaTotal: 0,
      deudaTotal: 0,
      totalReserva: 0,
      comentario: '',
      descuento: 0,
      esFeriado: false,
      fechaHoraCierre: null,
      fechaHoraComienzo: null,
      fechaHoraComienzoReal: null,
      fechaHoraCreacion: null,
      fechaHoraFin: null,
      fechaHoraFinPaso: null,
      fechaHoraFinReal: null,
      fechaHoraFinTentativa: null,
      unidadDeReserva: null,
      motivo: null,
      motivoId: null,
      unidadDeReservaId: null,
      usuario: null,
      // clientesReserva: <ClienteReserva[]>[{ esReservaFija: false, saldoAFavor: 0 }]
      clientesReserva: [this.getClienteReservaVacio()],
    };
  }

  getClienteReservaVacio(): ClienteReserva {
    return <ClienteReserva>{
      clienteId: 0,
      cliente: this.getClienteVacio(),
      senia: 0,
      comentarioBaja: null,
      consumos: [],
      deuda: 0,
      saldoAFavor: 0,
      esReservaFija: false,
      estadoCumplimiento: null,
      importeUnidadReservaParte: 0,
      usarSaldo: false,
    };
  }

  getClienteVacio(): Cliente {
    return <Cliente>{
      id: 0,
      nombreYApellido: null,
      documento: null,
      eliminado: false,
      email: null,
      cumplimientos: 0,
      infracciones: 0,
      infraccionesGraves: 0,
      saldo: 0,
      esTelefonoFijo: false,
      telefonoArea: null,
      telefonoNumero: null,
    };
  }

  getReserva(id: number, sucursalId: number): Observable<Reserva> {
    const urlGetReservaSucursal =
      urlBaseGetReserva.replace(
        environment.ReservaCrear.stringToReplace,
        sucursalId.toString()
      ) +
      '/' +
      id;
    return this.httpClient.get<Reserva>(urlGetReservaSucursal);
  }

  obtenerReservaEstaLiberadaParaUnaFechaHoraCancha(
    fechaHoraComienzo: Date,
    fechaHoraFin: Date,
    unidadDeReservaId: number,
    sucursalId: number
  ) {
    const urlGetReservaEstaLiberadaParaUnaFechaHoraCanchaSucursal =
      urlBaseGetReservaEstaLiberadaParaUnaFechaHoraCancha.replace(
        environment.ReservaCrear.stringToReplace,
        sucursalId.toString()
      ) +
      '/' +
      fechaHoraComienzo.toISOString().replace('Z', '') +
      '/' +
      fechaHoraFin.toISOString().replace('Z', '') +
      '/' +
      unidadDeReservaId;
    return this.httpClient.get<boolean>(
      urlGetReservaEstaLiberadaParaUnaFechaHoraCanchaSucursal
    );
  }

  ObtenerReservasParaUnaFecha(
    fechaReserva: Date,
    sucursal: Sucursal,
    activarLoading: boolean
  ): Observable<Reserva[]> {
    const urlReservasParaUnaFechaYUnaSucursal =
      urlBaseReservasParaUnaFecha.replace(
        environment.ReservaCrear.stringToReplace,
        sucursal.id.toString()
      ) + (!activarLoading ? 'NotLoading' : '');
    return this.httpClient
      .post<Reserva[]>(
        urlReservasParaUnaFechaYUnaSucursal,
        fechaReserva,
        httpOptions
      )
      .pipe(
        map((reservas: Reserva[]) => {
          this.agregarFechaHoraFinTentativo(reservas);
          this.validarSiAgotoElTiempoFinalTentativo(reservas);
          this.definirCuandoEsElProximoAviso(reservas);
          return reservas;
        })
      );
  }

  // FUNCIONES PUBLICAS PARA VALIDACIONES

  seAgotoElTiempo(res: Reserva, fechaHoraMomentoActual?: Date): boolean {
    const _fechaHoraMomentoActual: Date = fechaHoraMomentoActual || new Date();
    return res.fechaHoraFinTentativa < _fechaHoraMomentoActual && this.estaCorriendo(res);
  }

  estaPorFinalizar(res: Reserva, fechaHoraMomentoActual?: Date): boolean {
    const _fechaHoraMomentoActual: Date = fechaHoraMomentoActual || new Date();
    const minutosAvisoAntesDeFinalizarUnaReserva =
      environment.ReservaCrear.minutosAvisoAntesDeFinalizarUnaReserva;
    const diferenciaMinutos = this.timeService.obtenerMinutosEntreFechas(
      res.fechaHoraFinTentativa,
      _fechaHoraMomentoActual
    );
    return diferenciaMinutos < minutosAvisoAntesDeFinalizarUnaReserva;
  }

  finalizoRequiereNuevoAviso(reserva: Reserva): boolean {
    return this.estaCorriendo(reserva) && reserva.numeroAvisoTiempoAgotado > 0;
  }



  cuandoVolverAvisar(res: Reserva, fechaHoraMomentoActual?: Date) {
    const _fechaHoraMomentoActual: Date = fechaHoraMomentoActual || new Date();
    const minutosProximosAvisos = [].concat(environment.ReservaCrear.minutosProximosAvisos);
    const minutosAvisoAntesDeFinalizarUnaReserva =
      environment.ReservaCrear.minutosAvisoAntesDeFinalizarUnaReserva;
    const diferenciaMinutos = this.timeService.obtenerMinutosEntreFechas(
      res.fechaHoraFinTentativa,
      _fechaHoraMomentoActual
    );

    for (let i = 0; i < minutosProximosAvisos.length; i++) {
      // Entre el tiempo de proximo aviso y un minuto despues.
      if (
        diferenciaMinutos >= minutosProximosAvisos[i]
        && diferenciaMinutos < minutosProximosAvisos[i] + minutosAvisoAntesDeFinalizarUnaReserva
      ) {
        return i + 1;
      }
    }

    return 0;
  }

  estaDentroDeJornada(res: Reserva): boolean {
    const minutosComienzoDia = environment.ReservaCrear.comienzoDia;
    const horaComienzoDia = minutosComienzoDia / 60;
    const fechaComienzo = this.timeService.obtenerFechaInicialParaFranjaHoraria();
    const fechaHoraComienzoDia = this.timeService.sumarRestarHoras(
      fechaComienzo,
      horaComienzoDia
    );
    const fechaHoraFinalizacionDia = this.timeService.sumarRestarDias(
      fechaHoraComienzoDia,
      1
    );
    return (
      res.fechaHoraComienzo.getTime() >= fechaHoraComienzoDia.getTime() &&
      res.fechaHoraFin.getTime() > fechaHoraComienzoDia.getTime() &&
      res.fechaHoraFin.getTime() <= fechaHoraFinalizacionDia.getTime() &&
      res.fechaHoraComienzo.getTime() < fechaHoraFinalizacionDia.getTime()
    );
  }

  estaCorriendo(reserva: Reserva): boolean {
    return (
      !!reserva && !!reserva.fechaHoraComienzoReal && !reserva.fechaHoraFinReal
    );
  }

  estaFinalizada(reserva: Reserva): boolean {
    return !!reserva && !!reserva.fechaHoraFinReal && !reserva.fechaHoraCierre;
  }

  estaCerrada(reserva: Reserva): boolean {
    return !!reserva && !!reserva.fechaHoraCierre;
  }

  estaPendiente(reserva: Reserva): boolean {
    return (
      !!reserva && !reserva.fechaHoraComienzoReal && !!reserva.fechaHoraFin
    );
  }

  estaVacia(reserva: Reserva): boolean {
    return (
      !!reserva &&
      !(
        this.estaCorriendo(reserva) ||
        this.estaPendiente(reserva) ||
        this.estaFinalizada(reserva) ||
        this.estaCerrada(reserva)
      )
    );
  }

  esReservaConInfraccion(reservaEliminada: Reserva) {
    return (
      !!reservaEliminada &&
      reservaEliminada.estado === 'eliminada' &&
      !!reservaEliminada.clientesReserva.find(
        (clienteReserva) => clienteReserva.estadoCumplimiento === 'Infracción'
      )
    );
  }

  esReservaConInfraccionGrave(reservaEliminada: Reserva) {
    return (
      !!reservaEliminada &&
      reservaEliminada.estado === 'eliminada' &&
      !!reservaEliminada.clientesReserva.find(
        (clienteReserva) =>
          clienteReserva.estadoCumplimiento === 'Infracción Grave'
      )
    );
  }

  esReservaMenorUnaHora(reserva: Reserva): boolean {
    return (
      !this.estaVacia(reserva) &&
      this.calcularMinutosEntreDateTime(
        reserva.fechaHoraComienzo,
        reserva.fechaHoraFin
      ) /
      60 <
      1
    );
  }

  esReservaMultiple(reserva: Reserva): boolean {
    return !this.estaVacia(reserva) && reserva.clientesReserva.length > 1;
  }

  // Funciones Globales
  calcularMinutosReserva(reserva: Reserva): number {
    return this.calcularMinutosEntreDateTime(
      reserva.fechaHoraComienzo,
      reserva.fechaHoraFin
    );
  }

  obtenerSucursales(): Observable<Sucursal[]> {
    return this.httpClient.get<Sucursal[]>(urlGetSucursales).pipe(
      map((sucursales: Sucursal[]) => {
        sucursales.sort((a, b) => {
          return a.id < b.id ? -1 : 1;
        });
        return sucursales;
      })
    );
  }

  // FUNCIONES PRIVADAS

  private validadAntesDeCalcularImporte(
    reserva: Reserva,
    preciosPorHora: PrecioPorHora[]
  ) {
    if (!reserva.motivo && !reserva.motivoId) {
      return false;
    }
    if (reserva.motivo && reserva.motivo.esReservaCustom) {
      return false;
    }

    if (
      !(reserva.unidadDeReserva || reserva.unidadDeReservaId) ||
      !preciosPorHora ||
      preciosPorHora.length === 0
    ) {
      reserva.importeUnidadReservada = 0;
      return false;
    }

    return true;
  }

  private generarHorariosAdaptados(preciosPorHora: PrecioPorHora[]) {
    preciosPorHora.forEach((pPH) => {
      // Es más facil hacer los calculos con horas adaptadas.
      pPH.horaFinAdaptada =
        pPH.horaFin <= comienzoMinutosHora / 60
          ? pPH.horaFin + 24
          : pPH.horaFin;
      pPH.horaInicioAdaptada =
        pPH.horaInicio < comienzoMinutosHora / 60
          ? pPH.horaInicio + 24
          : pPH.horaInicio;
    });
  }

  private calcularTotalImporteUnidadReservada(
    reserva: Reserva,
    preciosPorHora: PrecioPorHora[]
  ) {
    const horaInicio = this.obtenerHoraAdaptada(reserva.fechaHoraComienzo);
    const horaFin = this.obtenerHoraAdaptada(reserva.fechaHoraFin);
    const diaSemana = reserva.esFeriado
      ? 'feriado'
      : diasSemana[this.obtenerFechaGrillaDeUnaReserva(reserva).getDay()];

    let total = 0;

    preciosPorHora
      .filter((pPH) => pPH.diaSemana === diaSemana)
      .forEach((pPH) => {
        if (diaSemana === pPH.diaSemana) {
          if (total === 0 && pPH.horaFinAdaptada > horaInicio) {
            if (horaFin < pPH.horaFinAdaptada) {
              total += (horaFin - horaInicio) * pPH.precioUnitarioPorHora;
            } else {
              total +=
                (pPH.horaFinAdaptada - horaInicio) * pPH.precioUnitarioPorHora;
            }
          } else if (total > 0 && pPH.horaInicioAdaptada < horaFin) {
            if (pPH.horaFinAdaptada < horaFin) {
              total +=
                (pPH.horaFinAdaptada - pPH.horaInicioAdaptada) *
                pPH.precioUnitarioPorHora;
            } else {
              total +=
                (horaFin - pPH.horaInicioAdaptada) * pPH.precioUnitarioPorHora;
            }
          }
        }
      });

    reserva.importeUnidadReservada = total;
  }

  private obtenerHoraAdaptada(fechaHora: Date): number {
    if (!fechaHora) {
      return null;
    }
    let hora = fechaHora.getHours() + fechaHora.getMinutes() / 60;
    const esDiaAnterior = hora < comienzoMinutosHora / 60;
    if (esDiaAnterior) {
      hora = hora + 24;
    }
    return hora;
  }

  private ordernarPreciosPorHora(preciosPorHora: PrecioPorHora[]) {
    // Se ordenan de menor a mayor
    // Nota: Es necesario filtrar el arreglo por día antes de usarlo.
    preciosPorHora.sort((a, b) => {
      if (a.horaInicioAdaptada > b.horaInicioAdaptada) {
        return 1;
      }
      // tslint:disable-next-line: one-line
      else if (a.horaInicioAdaptada < b.horaInicioAdaptada) {
        return -1;
      }
      // tslint:disable-next-line: one-line
      else {
        return 0;
      }
    });
  }

  private preperarReserva(reserva: Reserva): Reserva {
    const reservaEnvio = cloneDeep(reserva);

    if (reservaEnvio && reservaEnvio.clientesReserva) {
      reservaEnvio.clientesReserva.forEach((cR: ClienteReserva) => {
        cR.clienteId = cR.clienteId || cR.cliente.id;
        cR.cliente = null;
      });
    }
    if (reservaEnvio && reservaEnvio.unidadDeReserva) {
      reservaEnvio.unidadDeReservaId = reservaEnvio.unidadDeReserva.id;
      reservaEnvio.unidadDeReserva = null;
    }
    if (reservaEnvio && reservaEnvio.motivo) {
      reservaEnvio.motivoId = reservaEnvio.motivo.id;
      reservaEnvio.motivo = null;
    }
    return reservaEnvio;
  }

  private obtenerReservasEliminadasParaUnaFecha(
    fechaReserva: Date,
    sucursal: Sucursal
  ): Observable<Reserva[]> {
    const urlReservasEliminadasParaUnaFechaYUnaSucursal = urlBaseReservasEliminadasParaUnaFecha.replace(
      environment.ReservaCrear.stringToReplace,
      sucursal.id.toString()
    );
    return this.httpClient
      .post<Reserva[]>(
        urlReservasEliminadasParaUnaFechaYUnaSucursal,
        fechaReserva,
        httpOptions
      )
      .pipe(
        map((reservasEliminadas: Reserva[]) => {
          this.agregarFechaHoraFinTentativo(reservasEliminadas);
          return reservasEliminadas;
        })
      );
  }

  private definirCuandoEsElProximoAviso(reservas: Reserva[]) {
    reservas.forEach(r => {
      if (this.estaCorriendo(r)) {
        r.numeroAvisoTiempoAgotado = this.cuandoVolverAvisar(r);
      }
    });
  }

  private validarSiAgotoElTiempoFinalTentativo(reservas: Reserva[]): Reserva[] {
    reservas.forEach((r) => {
      if (this.seAgotoElTiempo(r)) {
        r.elTiempoSeAgoto = true;
      } else {
        r.elTiempoSeAgoto = false;
      }
    });
    return reservas;
  }

  private agregarFechaHoraFinTentativo(reservas: Reserva[]): Reserva[] {
    reservas.forEach((r) => {
      if (!!r.fechaHoraComienzoReal && !r.fechaHoraFinReal) {
        const duracion =
          r.fechaHoraFin.getTime() - r.fechaHoraComienzo.getTime();
        r.fechaHoraFinTentativa = this.timeService.sumarRestarMilisegundos(
          r.fechaHoraComienzoReal,
          duracion
        );
      }
    });
    return reservas;
  }

  private limpiarReservasParaFechaJornada(
    fechaReserva: Date,
    reservas: Reserva[]
  ): Reserva[] {
    const fechaReservaEntadarizada: Date = this.timeService.extraerFechaDeUnaFechaYHora(
      fechaReserva
    );
    const minutosComienzoDia = environment.ReservaCrear.comienzoDia;
    const fechaHoraComienzoDia = this.timeService.sumarRestarMinutos(
      fechaReservaEntadarizada,
      minutosComienzoDia
    );
    const fechaHoraFinalDia = this.timeService.sumarRestarDias(
      fechaHoraComienzoDia,
      1
    );

    const reservasParaFechaJornada = reservas.filter((r: Reserva) => {
      return (
        r.fechaHoraComienzo.getTime() >= fechaHoraComienzoDia.getTime() &&
        r.fechaHoraFin.getTime() < fechaHoraFinalDia.getTime()
      );
    });

    return reservasParaFechaJornada;
  }

  private calcularMinutosEntreDateTime(date1, date2): number {
    if (!date1 || !date2) {
      return 0;
    }
    if (date1 > date2) {
      const temp = new Date(date2.getTime());
      date2 = date1;
      date1 = temp;
    }
    return (date2.getTime() - date1.getTime()) / (1000 * 60);
  }
}
