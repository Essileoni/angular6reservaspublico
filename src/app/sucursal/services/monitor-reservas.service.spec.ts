import { TestBed, inject } from '@angular/core/testing';

import { MonitorReservasService } from './monitor-reservas.service';

describe('MonitorReservasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MonitorReservasService]
    });
  });

  it('should be created', inject([MonitorReservasService], (service: MonitorReservasService) => {
    expect(service).toBeTruthy();
  }));
});
