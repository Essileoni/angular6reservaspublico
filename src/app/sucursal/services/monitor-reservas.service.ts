import { Injectable } from '@angular/core';
import {
  Sucursal,
  Reserva,
} from '../componentes/reservas/reserva-page/reserva.model';
import { Observable, forkJoin, of } from 'rxjs';
import { SucursalReservas } from '../models/surcursal-reservas.model';
import { map, finalize, catchError } from 'rxjs/operators';
import { TimeService } from 'src/app/layout/services/time.service';
import { ReservaService } from './reserva.service';
import { AuthService } from 'src/app/layout/auth/auth.service';
import { environment } from 'src/environments/environment';
import { DialogService } from 'src/app/layout/dialog/dialog.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class MonitorReservasService {
  // PRIVATES
  private sucursalesReservasPorFinalizar: SucursalReservas[] = [];
  private modalListadoUnidadesReservasPorFinalizarEstaAbierto = false;

  constructor(
    private router: Router,
    private timeService: TimeService,
    private reservaService: ReservaService,
    private authService: AuthService,
    private dialogService: DialogService
  ) {
    this.ejecutarMonitorConsultarReservas();
  }

  // PRIVATES
  private ejecutarMonitorConsultarReservas() {
    if (this.authService.estaAutenticado()) {
      this.authService
        .obtenerSucursalesParaUnRol('SucursalControllerModificarReserva', false)
        .subscribe(
          (sucursalesParaUnRol: Sucursal[]) => {
            this.ObtenerReservasCorriendoPorTerminarParaMasDeUnaSucursal(
              sucursalesParaUnRol
            )
              .pipe(
                // El finalize ocurre despues del subscribe
                finalize(() => {
                  this.repetirConsultarReservas();
                })
              )
              .subscribe((sucursalesReservas: SucursalReservas[]) => {
                this.actualizarSucursalesReservasPorFinalizar(
                  this.sucursalesReservasPorFinalizar,
                  sucursalesReservas
                );
                this.procesarSucursalesReservas(
                  this.sucursalesReservasPorFinalizar
                );
              });
          },
          (error) => {
            this.repetirConsultarReservas();
          }
        );
    } else {
      this.repetirConsultarReservas();
    }
  }

  private actualizarSucursalesReservasPorFinalizar(
    sucursalesReservasParaActualizar: SucursalReservas[],
    sucursalesReservasNuevas: SucursalReservas[]
  ) {
    while (sucursalesReservasParaActualizar.length > 0) {
      sucursalesReservasParaActualizar.pop();
    }
    while (sucursalesReservasNuevas.length > 0) {
      sucursalesReservasParaActualizar.push(sucursalesReservasNuevas.shift());
    }
  }

  private procesarSucursalesReservas(
    sucursalesReservas: SucursalReservas[]
  ): void {
    if (!!sucursalesReservas && sucursalesReservas.length > 0) {
      this.mostrarModalListadoUnidadesReservas(sucursalesReservas);
    }
  }

  private esRutaValida(): boolean {
    const pathActual = this.router.url.toLowerCase().trim();
    const rutasQueNoAplican =
      environment.ReservaCrear.rutasNoAvisarFinalizanReservas;
    for (const rutasQueNoAplica of rutasQueNoAplican) {
      if (pathActual.indexOf(rutasQueNoAplica.toLowerCase().trim()) !== -1) {
        return false;
      }
    }
    return true;
  }

  private mostrarModalListadoUnidadesReservas(
    sucursalesReservas: SucursalReservas[]
  ) {
    if (!this.esRutaValida()) {
      return;
    }
    if (!this.modalListadoUnidadesReservasPorFinalizarEstaAbierto) {
      this.modalListadoUnidadesReservasPorFinalizarEstaAbierto = true;
      this.dialogService
        .openListadoSucursalesReservas(
          'Finalización',
          sucursalesReservas
        )
        .beforeClose()
        .subscribe((response) => {
          this.modalListadoUnidadesReservasPorFinalizarEstaAbierto = false;
          if (!response) {
            return;
          }
          // if response es ir a grilla
        });
    }
  }

  private repetirConsultarReservas() {
    const minutosAvisoAntesDeFinalizarUnaReserva = environment.ReservaCrear.minutosAvisoAntesDeFinalizarUnaReserva;
    const tiempoRepeticion = minutosAvisoAntesDeFinalizarUnaReserva * 60;

    setTimeout(() => {
      this.ejecutarMonitorConsultarReservas();
    }, tiempoRepeticion * 1000);
  }

  private repetirConsultarReservasBak() {
    const tercioSegundosAntesDeFinalizarUnaReserva =
      (environment.ReservaCrear.minutosAvisoAntesDeFinalizarUnaReserva * 60) /
      3;
    const inicio = new Date();
    const fin = new Date();
    const segundosDiff = this.timeService.obtenerSegundosEntreFechas(
      inicio,
      fin
    );

    // ¿Que tiempo usar y porque?
    const tiempoRepeticion =
      tercioSegundosAntesDeFinalizarUnaReserva < segundosDiff
        ? segundosDiff
        : tercioSegundosAntesDeFinalizarUnaReserva;

    setTimeout(() => {
      this.ejecutarMonitorConsultarReservas();
    }, tiempoRepeticion * 1000);
  }

  // PUBLICS
  ObtenerReservasCorriendoPorTerminarParaMasDeUnaSucursal(
    sucursales: Sucursal[]
  ): Observable<SucursalReservas[]> {
    const requests: Observable<SucursalReservas>[] = [];
    sucursales.forEach((sucursal) => {
      requests.push(
        this.ObtenerReservasCorriendoPorTerminarParaUnaSucursal(sucursal).pipe(
          map((reservas: Reserva[]) => {
            return <SucursalReservas>{ reservas: reservas, sucursal: sucursal };
          })
        )
      );
    });

    return forkJoin(requests).pipe(
      map((arregloSucursalReservas: SucursalReservas[]) => {
        return arregloSucursalReservas.filter((asr) => asr.reservas.length > 0);
      })
    );
  }

  FiltrarReservasCorriendoPorTerminarDentroJornada(
    reservas: Reserva[]
  ): Reserva[] {
    const esDev = environment.development;
    const fechaParaVerificar = new Date();
    return reservas
      .filter((res: Reserva) => this.reservaService.estaCorriendo(res))
      .filter((res: Reserva) => this.reservaService.estaDentroDeJornada(res))
      .filter(
        (res: Reserva) =>
          esDev
          || this.reservaService.estaPorFinalizar(res, fechaParaVerificar)
          || this.reservaService.finalizoRequiereNuevoAviso(res)
      );
  }

  ObtenerReservasCorriendoPorTerminarParaUnaSucursal(
    sucursal: Sucursal
  ): Observable<Reserva[]> {
    // Que pasa con las reservas que superan terminan a las 00, o empiezan a las 00 empiezan a las 23:30 y terminan a las 00:30
    // Que pasa si ya pase la 00 pero las reservas no se cerraron.
    const fechaEnCursoAdaptada = this.timeService.obtenerFechaInicialParaFranjaHoraria();
    const fechaPosteriorAdaptada = this.timeService.sumarRestarDias(
      fechaEnCursoAdaptada,
      1
    );
    const request: Observable<Reserva[]>[] = [];

    request.push(
      this.reservaService.ObtenerReservasParaUnaFecha(
        fechaEnCursoAdaptada,
        sucursal,
        false
      )
    );

    request.push(
      this.reservaService.ObtenerReservasParaUnaFecha(
        fechaPosteriorAdaptada,
        sucursal,
        false
      )
    );

    return forkJoin<Reserva[]>(request).pipe(
      map((respuesta: [Reserva[], Reserva[]]) => {
        const respuestaConcat = respuesta[0].concat(respuesta[1]);
        return this.FiltrarReservasCorriendoPorTerminarDentroJornada(
          respuestaConcat
        );
      })
    );
  }
}
