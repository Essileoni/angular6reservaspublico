import { Cliente } from "src/app/empresa/clientes/clientes.model";
// import { Reserva } from "../../reserva-page/reserva.model";

export interface ClienteReserva {
  // id: number;
  clienteId: number;
  cliente: Cliente;
  // reservaId:number;
  // reserva:Reserva;
  estadoCumplimiento: string;
  comentarioBaja: string;
  esReservaFija: boolean;
  consumos: Consumo[];
  senia: number; // seniaSaldo + seniaAdicional
  // seniaSaldo: number; // saldo a favor usado en la seña
  // seniaAdicional: number; // senia adicional ingresada manualmente
  saldoAFavor: number; // saldo historico a favor, que tiene el cliente, al momento de cerrar la reserva.
  deuda: number; // saldo historico negativo a pagar al momento del cierre de la reserva.
  descuentoReservaParte: number; // Fracción del descuento que beneficia al cliente.
  importeUnidadReservaParte: number; // historico de la fracción del importe a pagar por el cliente al momento de cerrar la reserva.
  usarSaldo: boolean; // historico que indica si el saldo fue realmente usado.
  clienteAbonoCon: number;
}

export interface Consumo {
  idArticulo: number;
  cantidad: number;
  total: number;
  // clienteReservaId;
}
