import {
  Sucursal,
  Reserva,
} from '../componentes/reservas/reserva-page/reserva.model';

export interface SucursalReservas {
  sucursal: Sucursal;
  reservas: Reserva[];
}
