import { ClienteReserva } from './cliente-reserva.model';


export interface ClientesReservaExtension {
  clienteReserva: ClienteReserva;
  cancela: boolean;
}
