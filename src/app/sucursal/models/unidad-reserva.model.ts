import { PrecioPorHora } from './precio-por-hora.model';


export interface UnidadReserva {
  id: number;
  preciosPorHora: PrecioPorHora[];
  descripcion: string;
  activo: boolean;
  tipo: string;
}
