export interface PrecioPorHora {
  id: number;
  horaInicio: number;
  horaFin: number;
  horaInicioAdaptada: number;
  horaFinAdaptada: number;
  precioUnitarioPorHora: number;
  diaSemana: string;
  unidadDeReservaId: number;
}
