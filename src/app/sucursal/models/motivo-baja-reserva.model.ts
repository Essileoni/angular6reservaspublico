export interface MotivoBajaReserva {
  id: number;
  descripcion: string;
  semaforo: string;
}
