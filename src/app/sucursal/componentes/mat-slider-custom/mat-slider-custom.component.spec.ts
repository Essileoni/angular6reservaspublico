import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatSliderCustomComponent } from './mat-slider-custom.component';

describe('MatSliderCustomComponent', () => {
  let component: MatSliderCustomComponent;
  let fixture: ComponentFixture<MatSliderCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatSliderCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatSliderCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
