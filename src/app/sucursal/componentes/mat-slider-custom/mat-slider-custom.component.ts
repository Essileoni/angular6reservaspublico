import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { min } from 'rxjs/operators';

@Component({
  selector: 'app-mat-slider-custom',
  templateUrl: './mat-slider-custom.component.html',
  styleUrls: ['./mat-slider-custom.component.css']
})
export class MatSliderCustomComponent implements OnInit {
  @Input() max: number;
  @Input() min: number;
  @Input() step: number;
  @Input() textoLabelAntes = '';

  // 2Way Binding
  private _minutos: number;
  @Input() get minutos(): number {
    return this._minutos;
  }
  @Output() minutosChange: EventEmitter<number> = new EventEmitter();
  set minutos(m: number) {
    this._minutos = m;
    this.minutosChange.emit(this._minutos);
  }
  // END - 2Way Binding

  color = 'primary';
  labelBind: string;
  styleColor: string;

  constructor() { }

  ngOnInit() {
    this.minutos = this.minutos || this.min;
    this.actualizarLabel();
  }

  onInput(evento: any) {
    this.minutos = evento.value;
    this.actualizarLabel();
    this.minutosChange.emit(this.minutos);
  }

  private actualizarLabel() {
    this.actualizarTextoLabel();
    if (this.minutos / 60 >= 24) {
      this.styleColor = '#ff4081 !important';
    } else if (this.minutos / 60 > this.min) {
      this.styleColor = '#3f51b5 !important';
    } else {
      this.styleColor = 'darkgray !important';
    }
  }

  private actualizarTextoLabel() {
    this.labelBind = this.textoLabelAntes + this.formatLabel(this.minutos);
  }

  formatLabel(value: number | null): string {
    // IMPORTANTE: Solo usar esta funcion para formatear el thump/tag del slider
    if (!value) {
      value = this.min ? this.min : 0;
    }

    let hora: number = Math.floor(value / 60);
    const minutos: string = ('00' + (value % 60).toString()).slice(-2);

    if (hora >= 24) {
      hora = hora - 24;
      this.color = 'accent';
    } else {
      this.color = 'primary';
    }

    return hora + ':' + minutos;
  }

  // SOLUCION PARA EVITAR CAMBIAR VALOR AL INTENTAR SCROLLEAR

  // tslint:disable-next-line: member-ordering
  timeOutTouch = undefined;
  // tslint:disable-next-line: member-ordering
  disabled = false;

  onTouchEnd() {
    this.resetearTouchVars();
  }

  onTouchMove() {
    if (this.disabled) {
      clearTimeout(this.timeOutTouch);
    }
  }

  onTouchStart() {
    this.disabled = true;
    this.timeOutTouch = setTimeout(() => {
      this.disabled = false;
      // Permitir el control del slider, tal vez pasar el click y las coordenadas.
    }, 500);
  }

  resetearTouchVars() {
    clearTimeout(this.timeOutTouch);
    this.timeOutTouch = undefined;
    this.disabled = true;
  }

  // END - SOLUCION PARA EVITAR CAMBIAR VALOR AL INTENTAR SCROLLEAR
}
