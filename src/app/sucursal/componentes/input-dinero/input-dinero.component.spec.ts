import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputDineroComponent } from './input-dinero.component';

describe('InputDineroComponent', () => {
  let component: InputDineroComponent;
  let fixture: ComponentFixture<InputDineroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputDineroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDineroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
