import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input-dinero',
  templateUrl: './input-dinero.component.html',
  styleUrls: ['./input-dinero.component.css']
})
export class InputDineroComponent implements OnInit {
  @Input() placeholder = '';

  constructor() { }

  ngOnInit() {
  }

  onInputToDinero(valor: string, input) {
    valor = valor + '';
    valor = valor
      .replace(',', '.')
      .replace('$', '')
      .trim();

    valor = this.limpiar(valor);

    input.value = '$ ' + (+valor).toFixed(2);
  }

  limpiar(valor: string): string {
    const tam = valor.length;
    let puntos = 0;
    let salida = '';
    for (let i = 0; i < tam; i++) {
      if (!+valor[i] && valor[i] === '.' && i > 0) {
        if(puntos === 1){
          break;
        }
        salida = salida + valor[i];
        puntos++;
      } else if (!!+valor[i] || valor[i] === '0') {
        salida = salida + valor[i];
      }
    }
    return salida;
  }

}
