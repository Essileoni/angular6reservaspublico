import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { min } from 'rxjs/operators';

@Component({
  selector: 'app-mat-slider-custom-duracion',
  templateUrl: './mat-slider-custom-duracion.component.html',
  styleUrls: ['./mat-slider-custom-duracion.component.css']
})
export class MatSliderCustomDuracionComponent implements OnInit {
  @Input() max: number;
  @Input() min: number;

  @Input() step: number;
  @Input() textoLabelAntes = '';
  @Input() error = false;

  _tiempoReferencia = 0;

  @Input() get tiempoReferencia(): number {
    return this._tiempoReferencia;
  }
  set tiempoReferencia(tr: number) {
    this._tiempoReferencia = tr;
    if (this.onInitPass) {
      this.actualizarTiempoAdicional();
    }
  }

  _tiempoAdicional = 0;
  @Input() get tiempoAdicional(): number {
    return this._tiempoAdicional;
  }
  set tiempoAdicional(tr: number) {
    this._tiempoAdicional = tr;
    if (this.onInitPass) {
      this.actualizarTiempoAdicional();
    }
  }

  // 2Way Binding
  private _minutos: number;
  @Input() get minutos(): number {
    return this._minutos;
  }
  @Output() minutosChange: EventEmitter<number> = new EventEmitter();
  set minutos(m: number) {
    this._minutos = m;
    this.actualizarTextoLabel();
    this.minutosChange.emit(this._minutos);
  }
  // END - 2Way Binding

  color = 'primary';
  labelBind: string;
  valueSlider: number;

  // private tiempoAdicional = 0;
  private textoLabelDespues = '';
  private maxBackup: number;
  private onInitPass = false;
  constructor() { }

  ngOnInit() {
    this.onInitPass = true;
    // this.minutos = this.min;
    this.actualizarTextoLabelDespues();
    this.maxBackup = this.max;
  }

  onInput(evento: any) {
    this.error = false;
    this.minutos = evento.value;
    this.actualizarTextoLabelDespues();
    this.minutosChange.emit(this.minutos);
  }

  private actualizarTextoLabel() {
    this.labelBind =
      this.textoLabelAntes +
      this.formatLabel(this.minutos) +
      this.textoLabelDespues;
  }

  formatLabel(value: number | null): string {
    if (!value) {
      value = this.min ? this.min : 0;
    }

    const hora: number = Math.floor(value / 60);
    const minutos: string = ('00' + (value % 60).toString()).slice(-2);

    return hora + ':' + minutos;
  }


  actualizarTiempoAdicional() {

    if (this.max !== 0 && !this.max) {
      return;
    }

    const avanceReal = this.tiempoAdicional - this.tiempoReferencia;

    const restoDelDia = 24 * 60 - avanceReal;

    if (restoDelDia < this.maxBackup) {
      this.max = restoDelDia;
    } else {
      this.max = this.maxBackup;
    }

    if (this.max <= 0) {
      this.max = this.step; // el paso es el tiempo minimo posible
    }

    if (this.minutos > this.max) {
      this.minutos = this.max;
    }

    this.actualizarTextoLabelDespues();
  }

  actualizarTextoLabelDespues() {
    const tiempoTotal = this.tiempoAdicional + this.minutos;

    let hora: number = Math.floor(tiempoTotal / 60);
    const minutos: string = ('00' + (tiempoTotal % 60).toString()).slice(-2);

    if (hora >= 24) {
      hora = hora - 24;
    }

    this.textoLabelDespues = ' / ' + hora + ':' + minutos;
    this.actualizarTextoLabel();
  }


  // SOLUCION PARA EVITAR CAMBIAR VALOR AL INTENTAR SCROLLEAR

  // tslint:disable-next-line: member-ordering
  timeOutTouch = undefined;
  // tslint:disable-next-line: member-ordering
  disabled = false;

  onTouchEnd() {
    this.resetearTouchVars();
  }

  onTouchMove() {
    if (this.disabled) {
      clearTimeout(this.timeOutTouch);
    }
  }

  onTouchStart() {
    this.disabled = true;
    this.timeOutTouch = setTimeout(() => {
      this.disabled = false;
      // Permitir el control del slider, tal vez pasar el click y las coordenadas.
    }, 500);
  }

  resetearTouchVars() {
    clearTimeout(this.timeOutTouch);
    this.timeOutTouch = undefined;
    this.disabled = true;
  }

  // END - SOLUCION PARA EVITAR CAMBIAR VALOR AL INTENTAR SCROLLEAR

}
