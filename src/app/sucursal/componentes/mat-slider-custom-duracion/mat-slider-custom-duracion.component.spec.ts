import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatSliderCustomDuracionComponent } from './mat-slider-custom-duracion.component';

describe('MatSliderCustomComponent', () => {
  let component: MatSliderCustomDuracionComponent;
  let fixture: ComponentFixture<MatSliderCustomDuracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatSliderCustomDuracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatSliderCustomDuracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
