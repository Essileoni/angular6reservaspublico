import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservaComenzadaPageComponent } from './reserva-comenzada-page.component';

describe('ReservaComenzadaPageComponent', () => {
  let component: ReservaComenzadaPageComponent;
  let fixture: ComponentFixture<ReservaComenzadaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservaComenzadaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservaComenzadaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
