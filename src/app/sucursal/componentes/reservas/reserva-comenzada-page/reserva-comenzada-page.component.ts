import { Component, OnInit, Input } from '@angular/core';
import { RouterExtenderService } from 'src/app/layout/services/router-extender.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxMaterialTimepickerTheme } from 'ngx-material-timepicker';
import { Reserva, Sucursal } from '../reserva-page/reserva.model';
import { ClienteReserva, Consumo } from '../../../models/cliente-reserva.model';
import { MessageBoxService } from 'src/app/layout/mensajes/messagebox.service';
import {
  MessageBox,
  MessageTipo,
  MessageFloat,
} from 'src/app/layout/mensajes/message-box.model';
import { DialogService } from 'src/app/layout/dialog/dialog.service';
import { ClientesService } from 'src/app/empresa/clientes/clientes.service';
import { faLock } from '@fortawesome/free-solid-svg-icons';
import * as cloneDeep from 'node_modules/lodash/cloneDeep';
import { TimeService } from 'src/app/layout/services/time.service';
import { AuthService } from 'src/app/layout/auth/auth.service';
import { ReservaService } from 'src/app/sucursal/services/reserva.service';

@Component({
  selector: 'app-reserva-comenzada-page',
  templateUrl: './reserva-comenzada-page.component.html',
  styleUrls: ['./reserva-comenzada-page.component.css'],
})
export class ReservaComenzadaPageComponent implements OnInit {
  // ENVIRONMENT

  appearance = environment.matFormFieldAppearance;
  importeReservaText = environment.ReservaCrear.ImporteReservaText;
  finalizarReservaText = environment.ReservaCrear.FinalizarReservaText;

  // CONFIGURACION
  primary: NgxMaterialTimepickerTheme = {
    container: {
      buttonColor: '#3f51b5',
    },
    dial: {
      dialBackgroundColor: '#3f51b5',
    },
    clockFace: {
      clockHandColor: '#3f51b5',
    },
  };

  // PRIVATES
  private paramsSubs: Subscription;
  private _fechaHoraComienzoReal: Date;
  sucursalesCambiarEstadoReserva: Sucursal[] = [];
  puedeCambiarEstadosReservas = false;
  subscripcionCambioRuta: Subscription;
  private get fechaHoraComienzoReal(): Date {
    if (this.estaPendiente()) {
      if (!this._fechaHoraComienzoReal) {
        this._fechaHoraComienzoReal = new Date(this.reserva.fechaHoraComienzo);
      }
      return this._fechaHoraComienzoReal;
    } else {
      return this.reserva.fechaHoraComienzoReal;
    }
  }
  private set fechaHoraComienzoReal(t: Date) {
    if (this.estaPendiente()) {
      this._fechaHoraComienzoReal = t;
    } else {
      this.reserva.fechaHoraComienzoReal = t;
    }
  }
  private _sucursalId: number;

  // PUBLICS
  estaDetallesExpandido = false;
  estaDetallesDeshabilitado = false;
  reserva: Reserva;
  faLock = faLock;
  get horaInicio(): string {
    let horas: string = this.fechaHoraComienzoReal.getHours() + '';
    horas = '00'.substring(horas.length) + horas;
    return horas;
  }

  set horaInicio(t: string) {
    this.fechaHoraComienzoReal.setHours(+t);
    this.actualizarFechaHoraFinTentativa();
  }

  get minutosInicio(): string {
    let minutos: string = this.fechaHoraComienzoReal.getMinutes() + '';
    minutos = '00'.substring(minutos.length) + minutos;
    return minutos;
  }

  set minutosInicio(t: string) {
    this.fechaHoraComienzoReal.setMinutes(+t);
    this.actualizarFechaHoraFinTentativa();
  }

  get horaFin(): string {
    const horas: number =
      (this.reserva.fechaHoraFinTentativa &&
        this.reserva.fechaHoraFinTentativa.getHours()) ||
      this.reserva.fechaHoraFin.getHours();
    let horasStr: string = horas + '';
    horasStr = '00'.substring(horasStr.length) + horasStr;
    return horasStr;
  }
  set horaFin(t: string) {
    // tslint:disable-next-line: no-unused-expression
    this.reserva.fechaHoraFinTentativa.setHours(+t);
  }
  get minutosFin(): string {
    const minutos: number =
      (this.reserva.fechaHoraFinTentativa &&
        this.reserva.fechaHoraFinTentativa.getMinutes()) ||
      this.reserva.fechaHoraFin.getMinutes();
    let minutosStr: string = minutos + '';
    minutosStr = '00'.substring(minutosStr.length) + minutosStr;
    return minutosStr;
  }

  set minutosFin(t: string) {
    // tslint:disable-next-line: no-unused-expression
    this.reserva.fechaHoraFinTentativa.setMinutes(+t);
  }

  get totalConsumos(): number {
    // El total de consumo se calcula en base a los consumos de los clientes, en momento que se lo requiera.
    // tslint:disable-next-line: no-inferrable-types
    let _totalConsumos: number = 0;
    if (this.reserva.clientesReserva) {
      this.reserva.clientesReserva.forEach((cr: ClienteReserva) => {
        if (cr.consumos) {
          cr.consumos.forEach((c: Consumo) => {
            _totalConsumos += c.total;
          });
        }
      });
    }
    return _totalConsumos;
  }

  get totalReserva(): number {
    this.calcularTotalReserva();
    return this.reserva.totalReserva;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private reservaService: ReservaService,
    private clientesService: ClientesService,
    private authService: AuthService,
    private routerExtenderService: RouterExtenderService,
    private messageBoxService: MessageBoxService,
    private dialogService: DialogService,
    private timeService: TimeService
  ) {}

  ngOnInit() {
    this.inicializar();
    this.subscribirCambioRuta();
  }

  inicializar() {
    this.obtenerSucursalId();
    this.ObtenerSucursalesParaCambiarEstadoReserva();
    this.obtenerReserva();
  }

  // LISTENERS
  onKeyPressNumero(event: KeyboardEvent): boolean {
    return ['e', 'E', '+', '-', '.', ','].indexOf(event.key) === -1;
  }

  onVolverCancelar() {
    this.volver();
  }

  onSubmit() {
    if (!this.puedeCambiarEstadosReservas) {
      return;
    }
    if (!this.esReservaValida()) {
      return;
    }

    if (this.estaPendiente()) {
      this.comenzar();
    } else if (this.estaCorriendo()) {
      this.finalizar();
    } else if (this.estaFinalizada()) {
      this.cerrar();
    }
  }

  // PUBLICAS

  log(obj: any) {
    console.log(obj);
  }

  estaCorriendo(): boolean {
    return this.reservaService.estaCorriendo(this.reserva);
  }

  estaFinalizada(): boolean {
    return this.reservaService.estaFinalizada(this.reserva);
  }

  estaPendiente(): boolean {
    return this.reservaService.estaPendiente(this.reserva);
  }

  estaCerrada(): boolean {
    return this.reservaService.estaCerrada(this.reserva);
  }

  estaVacia(): boolean {
    return this.reservaService.estaVacia(this.reserva);
  }

  esReservaMenorUnaHora(): boolean {
    return this.reservaService.esReservaMenorUnaHora(this.reserva);
  }

  esReservaMultiple(): boolean {
    return this.reservaService.esReservaMultiple(this.reserva);
  }

  // PRIVADOS

  private comenzar() {
    this.reserva.fechaHoraComienzoReal = new Date(this.fechaHoraComienzoReal);

    // Más Adelante el metodo aplicarSemaforo coloca semaforos verdes ambos clientes.
    this.reserva.clientesReserva.forEach((cR: ClienteReserva) => {
      cR.estadoCumplimiento = 'Cumplió';
    });

    this.clientesService
      .aplicarSemaforo(this.reserva.clientesReserva)
      .subscribe(() => {
        this.reservaService
          .cambiarEstadoReserva(this.reserva, this._sucursalId)
          .subscribe(() => {
            this.messageBoxService.show(<MessageBox>{
              Tipo: MessageTipo.Success,
              Mensaje: 'Se inició la reserva número ' + this.reserva.id,
              Strong: 'Perfecto. ',
              Float: MessageFloat.StickyFloat,
              Segundos: 10,
            });
            this.goToReservas();
          });
      });
  }

  // Finaliza la reserva, pero no la cobra.
  private finalizar() {
    // NOTA: el semaforo se aplica cuando comienza la reserva, por que la persona ya está acá.
    // Actualiza la reserva.
    this.reserva.fechaHoraFinReal = new Date(
      this.reserva.fechaHoraFinTentativa
    );
    this.reservaService
      .cambiarEstadoReserva(this.reserva, this._sucursalId)
      .subscribe(() => {
        this.messageBoxService.show(<MessageBox>{
          Tipo: MessageTipo.Success,
          Mensaje: 'Finalizó la reserva número ' + this.reserva.id,
          Strong: 'Listo. ',
          Float: MessageFloat.StickyFloat,
          Segundos: 10,
        });
        this.volver();
      });
  }

  private cerrar() {
    if (!this.noFaltaNadiePorPagar()) {
      return;
    }

    const reservaClone = cloneDeep(this.reserva);
    reservaClone.fechaHoraCierre = new Date();
    this.reservaService
      .cambiarEstadoReserva(reservaClone, this._sucursalId)
      .subscribe(() => {
        this.reserva = reservaClone;
        this.goToReservas();
      });
  }

  noFaltaNadiePorPagar(): boolean {
    return this.reservaService.noFaltaNadiePorPagar(this.reserva);
  }

  clienteReservaPago(cR: ClienteReserva, reserva): boolean {
    return (
      cR.estadoCumplimiento === 'Cumplió' &&
      cR.importeUnidadReservaParte ===
        reserva.importeUnidadReservada / reserva.clientesReserva.length
    );
  }

  clienteQuierePagar(reserva: Reserva, clienteReserva: ClienteReserva) {
    this.dialogService
      .openBorrarClientePagaReservaDialog(
        reserva,
        clienteReserva,
        this._sucursalId
      )
      .beforeClose()
      .subscribe((reservaModificada: Reserva) => {
        this.reserva = reservaModificada;
      });

    // Ofrece pasar a la próxima semana al primer cliente.
    // Si hay lugar crea la reserva para la misma duración;
    // -1- Una reserva termina, pero pueden seguir consumiendo
    // 0- Cuando para el primer cliente ofrecer pasar a la próxima semana al primer cliente.
    // 1- Un Cliente Paga
    // 2- Un Cliente Paga y Pero el otro sigue.
    // 3- Clientes Pagan por separado
    //    3a- Un cliente no tiene el total
    //    3b- Un cliente debía de antes.
    //    3c- Un cliente tiene saldo a favor.
    // Activar casilla de pago de clientes
    // Cobrar a cada cliente por separado
    // Ingreso de dinero pagado por los clientes
    // Mostrar saldo a favor o en contra del cliente.
    // Calcular saldo cliente (a favor o en contra)
  }

  private ObtenerSucursalesParaCambiarEstadoReserva() {
    this.authService
      .obtenerSucursalesParaUnRol(
        'SucursalControllerCambiarEstadoReserva',
        true
      )
      .subscribe((sucursales: Sucursal[]) => {
        this.sucursalesCambiarEstadoReserva = sucursales;
        this.puedeCambiarEstadosReservas = !!this.sucursalesCambiarEstadoReserva.find(
          (sucursal: Sucursal) =>
            sucursal.id.toString() === this._sucursalId.toString()
        );
      });
  }

  private goToReservas() {
    this.routerExtenderService.sucursalIdDeReservaComenzadaAGrilla = this._sucursalId;
    this.router.navigate(['reservas']);
  }

  private volver() {
    this.routerExtenderService.sucursalIdDeReservaComenzadaAGrilla = this._sucursalId;
    this.routerExtenderService.volver();
  }

  private esReservaValida(): // reservaForm: NgForm
  boolean {
    let respuesta = true;

    if (!this.reserva || !this.fechaHoraComienzoReal) {
      return false;
    }

    // Validar que exista comentario de almenos 10 caracteres para un descuento numerico mayor a 0.
    if (
      +this.reserva.descuento &&
      +this.reserva.descuento > 0 &&
      (!this.reserva.comentario || this.reserva.comentario.length < 10)
    ) {
      respuesta = false;
    }

    return respuesta;
  }

  private calcularTotalReserva() {
    this.reserva.totalReserva =
      this.reserva.importeUnidadReservada -
        this.reserva.descuento -
        this.reserva.seniaTotal >
      0
        ? this.reserva.importeUnidadReservada -
          this.reserva.descuento -
          this.reserva.seniaTotal
        : 0;
  }

  private actualizarFechaHoraFinTentativa() {
    const duracion =
      this.reserva.fechaHoraFin.getTime() -
      this.reserva.fechaHoraComienzo.getTime();

    const fechaHora = this.timeService.sumarRestarMilisegundos(
      this.fechaHoraComienzoReal,
      duracion
    );
    this.reserva.fechaHoraFinTentativa = fechaHora;
  }

  private obtenerReserva() {
    if (
      this.router.url.indexOf('/reserva/comenzada/') !== -1 &&
      this.routerExtenderService.reservaDeReservaPageAReservaComenzada
    ) {
      this.reserva = this.routerExtenderService.reservaDeReservaPageAReservaComenzada;
      this.routerExtenderService.reservaDeReservaPageAReservaComenzada = undefined;
      this.actualizarFechaHoraFinTentativa();
    } else if (this.router.url.indexOf('/reserva/comenzada/') !== -1) {
      this.paramsSubs = this.activatedRoute.params.subscribe((params) => {
        if (params.id) {
          this.reservaService
            .getReserva(<number>params.id, this._sucursalId)
            .subscribe((reserva: Reserva) => {
              this.reserva = reserva;
              this.actualizarFechaHoraFinTentativa();
            });
        }
        // tslint:disable-next-line: no-unused-expression
        this.paramsSubs && this.paramsSubs.unsubscribe();
      });
    }
  }

  private obtenerSucursalId() {
    this._sucursalId = +this.activatedRoute.snapshot.params['idSucursal'];
  }

  private subscribirCambioRuta() {
    this.subscripcionCambioRuta = this.activatedRoute.paramMap.subscribe(
      (paramsMap: ParamMap) => {
        this.inicializar();
      }
    );
  }
}
