import { ClienteReserva } from '../../../models/cliente-reserva.model';
import { UnidadReserva } from 'src/app/sucursal/models/unidad-reserva.model';



export class Reserva {
  id: number;
  fechaHoraCreacion: Date;
  fechaHoraComienzo: Date;
  fechaHoraFinPaso: Date; // No existe en backend
  fechaHoraFin: Date;
  fechaHoraComienzoReal: Date;
  fechaHoraFinReal: Date;
  fechaHoraFinTentativa: Date;
  fechaHoraCierre: Date;
  numeroAvisoTiempoAgotado = 0;
  seniaTotal: number;
  deudaTotal: number;
  importeUnidadReservada: number;
  totalReserva: number;
  descuento: number;
  comentario: string;
  usuario: Usuario;
  motivoId: number; // Lo que se envia al servidor en post.
  motivo: MotivoReserva; // Nunca se envia al servidor.
  unidadDeReservaId: number; // Lo que se envia al servidor en post.
  unidadDeReserva: UnidadReserva; // Nunca se envia al servidor.
  esFeriado: boolean;
  clientesReserva: ClienteReserva[];
  estado: string;
  estaPorAgotarseElTiempo = false;
  elTiempoSeAgoto = false;
}



export interface MotivoReserva {
  id: number;
  descripcion: string;
  esReservaCustom: boolean;
  tiempoEstandar: number;
  color: string;
}


export interface Usuario {
  lastname: string;
  firstname: string;
}

export interface Sucursal {
  id: number;
  nombre: string; // Nombre de la sucursal, editable
  calle: string;
  numero: string;
  comoLlegar: string;
  telefonos: string; /// Telefonos separados por una pipe/pipa/barra |
  coordenadas: string;
}
