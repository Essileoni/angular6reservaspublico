import { FormGroup, NgForm } from '@angular/forms';
import { ClienteReserva } from '../../../models/cliente-reserva.model';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Reserva, MotivoReserva, Sucursal } from './reserva.model';
import { Cliente } from 'src/app/empresa/clientes/clientes.model';

import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import {
  MessageBox,
  MessageTipo,
  MessageFloat,
} from 'src/app/layout/mensajes/message-box.model';
import { MessageBoxService } from 'src/app/layout/mensajes/messagebox.service';
import { DialogService } from 'src/app/layout/dialog/dialog.service';
import { RouterExtenderService } from 'src/app/layout/services/router-extender.service';

import { ClientesService } from 'src/app/empresa/clientes/clientes.service';
import { TimeService } from 'src/app/layout/services/time.service';
import { AuthService } from 'src/app/layout/auth/auth.service';
import { ReservaService } from 'src/app/sucursal/services/reserva.service';
import { UnidadReserva } from 'src/app/sucursal/models/unidad-reserva.model';
import { PrecioPorHora } from 'src/app/sucursal/models/precio-por-hora.model';

@Component({
  selector: 'app-reserva-page',
  templateUrl: './reserva-page.component.html',
  styleUrls: ['./reserva-page.component.css'],
})
export class ReservaPageComponent implements OnInit {
  @Input() reserva: Reserva;
  @ViewChild('reservaForm') reservaForm: NgForm;

  // Form Controls
  reservaFormGroup: FormGroup;
  dateFormGroup: FormGroup;

  // Colecciones
  motivosReserva: MotivoReserva[] = [];
  // motivosReserva: MotivoReserva[] = [
  //   {
  //     id: 1,
  //     descripcion: 'cancha',
  //     esReservaCustom: false,
  //     tiempoEstandar: 120,
  //     color: 'red'
  //   }
  // ];
  unidadesReserva: UnidadReserva[] = [];
  clientesReserva: ClienteReserva[] = [];
  clientesAFiltrarR: Cliente[] = [];
  sucursales: Sucursal[] = [];

  private _fechaReservaListaDesplegableReal: Date;
  private _fechaAnteriorReserva: Date;
  private sucursalesCambiarEstadoReserva: Sucursal[] = [];
  private _reservaId: number;
  private _sucursalId: number;

  sucursal: Sucursal = null;
  puedeCambiarEstadosReservas: boolean = false;

  get fechaReservaMostrar(): Date {
    if (this._fechaReservaListaDesplegableReal.getHours() < this.comienzoMinutosHora / 60) {
      return this._fechaAnteriorReserva;
    }
    return this._fechaReservaListaDesplegableReal;
  }
  set fechaReservaMostrar(t: Date) {
    this._fechaAnteriorReserva = this.timeService.sumarRestarDias(t, -1);
    this._fechaReservaListaDesplegableReal = t;
  }

  restaTotal = 0;

  // Constante de configuracion
  appearance = environment.matFormFieldAppearance;
  comienzoMinutosHora = environment.ReservaCrear.comienzoDia;
  step = environment.ReservaCrear.step;
  maximoMinutosHora = 24 * 60 + this.comienzoMinutosHora - this.step;

  _duracionMaximaMinutos = environment.ReservaCrear.duracionMaximaMinutos;
  get duracionMaximaMinutos(): number {
    const dMaxMin = this._duracionMaximaMinutos;
    if (dMaxMin < this.duracionFinalJornadaMinutos) {
      return dMaxMin;
    }
    return this.duracionFinalJornadaMinutos;
  }

  get duracionFinalJornadaMinutos(): number {
    return this.horaReservaEnMinutosParaMostrar
      ? this.comienzoMinutosHora +
          24 * 60 -
          this.horaReservaEnMinutosParaMostrar
      : this.comienzoMinutosHora + 24 * 60;
  }

  horaReservaEnMinutosParaMostrar = 0;
  duracionEnMinutos: number;

  esReservaFija = false;

  isCreateForm = false;
  minDate = null;
  // momentoActual = new Date();

  tiempoAdicional = 0;
  duracionError = false;
  camposCorrectos = false;
  disabledDuracion: boolean;

  constructor(
    private reservaService: ReservaService,
    private clientesService: ClientesService,
    private authService: AuthService,
    private router: Router,
    private messageBoxService: MessageBoxService,
    private dialogService: DialogService,
    private routerExtenderService: RouterExtenderService,
    private activatedRoute: ActivatedRoute,
    private timeService: TimeService
  ) {
    this.obtenerSucursalId();
    this.obtenerReservaId();
    this.ObtenerSucursalesParaCambiarEstadoReserva();
  }

  ngOnInit() {
    this.obtenerReserva();
    this.obtenerMotivosReserva();
    this.ObtenerSucursalesYUnidadDeReserva();
    this.obtenerMatrizParaCalcularTodo();
    this.validarFormInterval();
  }

  private obtenerSucursalId() {
    this._sucursalId = +this.activatedRoute.snapshot.params['idSucursal'];
  }

  private obtenerReservaId() {
    this._reservaId = +this.activatedRoute.snapshot.params['id'];
  }

  onCrearCliente() {
    // TODO revisar que pasa al crear un cliente nuevo en una reserva nueva
    this.routerExtenderService.reservaDeReservaACliente = this.reserva;
    this.router.navigate(['sucursales/cliente']);
  }

  onEditarCliente(c: Cliente) {
    this.routerExtenderService.reservaDeReservaACliente = this.reserva;
    this.routerExtenderService.clienteDeReservaACliente = c;
    // Este cliente está dentro de la reserva, si el objeto cambia. Ya queda asociado.
    this.router.navigate(['sucursales/cliente', c.id]);
  }

  onClickAgregarQuitarCliente() {
    if (this.reserva.clientesReserva.length <= 1) {
      this.reserva.clientesReserva[0].esReservaFija = false;
      this.reserva.clientesReserva.push(<ClienteReserva>{});
    } else if (this.reserva.clientesReserva.length > 1) {
      this.dialogService
        .openConfirmDialog('¿Querés eliminar el cliente?')
        .beforeClose()
        .subscribe((response) => {
          if (!response) {
            return;
          }
          this.reserva.clientesReserva.pop();
        });
    }
    this.calcularTodo();
  }

  onClickComenzar() {
    this.irAComienzoODetalle();
  }

  onClickDetalles() {
    this.irAComienzoODetalle();
  }

  onclienteReservaModelChange() {
    this.camposCorrectos = false;

    // evitar el Expression has changed after it was checked error
    setTimeout(() => {
      this.clientesAFiltrarR = []; // fuera del timeout ocurre un error 'Expression has changed after it was checked error'
      this.reserva.clientesReserva.forEach((cR) => {
        if (cR.cliente) {
          this.clientesAFiltrarR.push(cR.cliente);
        }
      });
    }, 0);
  }

  irAComienzoODetalle() {
    this.routerExtenderService.reservaDeReservaPageAReservaComenzada = this.reserva;
    this.router.navigate([
      this._sucursalId + '/reserva/comenzada/' + this.reserva.id,
    ]);
  }

  validarFormInterval() {
    setInterval(() => {
      let esValido = true;

      this.reserva.clientesReserva.forEach((cr) => {
        esValido = esValido && !!cr.cliente;
      });

      esValido = esValido && !!this.fechaReservaMostrar;

      esValido = esValido && this.duracionEnMinutos > 0;

      this.camposCorrectos = this.reservaForm.valid && esValido;
    }, 1000);
  }

  // obtenerDuracionMaxima() {
  //   return this.duracionMaxima;
  // }

  log(obj: any) {
    console.log(obj);
  }

  procesarReserva(reservaForm: NgForm) {
    if (!this.reserva.id) {
      this.crearReserva(reservaForm);
      return;
    }
    this.modificarReserva(reservaForm);
  }

  eliminarReserva() {
    this.dialogService
      .openBorrarReservaDialog(this.reserva)
      .beforeClose()
      .subscribe((response) => {
        if (!response) {
          return;
        }

        this.reservaService
          .eliminarReserva(this.reserva, this._sucursalId)
          .subscribe(() => {
            this.clientesService
              .aplicarSemaforo(this.reserva.clientesReserva)
              .subscribe(() => {
                this.messageBoxService.show(<MessageBox>{
                  Tipo: MessageTipo.Success,
                  Mensaje: 'Se eliminó la reserva número ' + this.reserva.id,
                  Strong: 'Listo. ',
                  Float: MessageFloat.StickyFloat,
                  Segundos: 10,
                });
                this.volver();
              });
          });
      });
  }

  obtenerMatrizParaCalcularTodo() {
    if (!this.reserva.unidadDeReserva) {
      return;
    }
    this.reservaService
      .obtenerMatrizPreciosUnidadReserva(
        this.reserva.unidadDeReserva.id,
        this._sucursalId
      )
      .subscribe((preciosPorHora: PrecioPorHora[]) => {
        this.reserva.unidadDeReserva.preciosPorHora = preciosPorHora;
        this.calcularTodo();
      });
  }

  calcularTodo() {
    // TODO: Arreglar error ExpressionChangedAfterItHasBeenCheckedError

    this.setFechaHoraComienzoReserva();
    this.setFechaHoraFinReserva();
    setTimeout(() => {
      this.calcularSenia();
      this.calcularDeuda();
      this.calcularImporte();
      this.calcularTotalReserva();
    }, 1);
  }

  verificarKey(evento: any) {
    return (
      evento.key !== '-' &&
      evento.key !== '+' &&
      evento.key !== '.' &&
      evento.key !== 'e' &&
      evento.key !== 'E'
    );
  }

  private modificarReserva(reservaForm: NgForm) {
    if (!this.esReservaValida(reservaForm) || !this.camposCorrectos) {
      return;
    }
    //TODO !!IMPORTANTE AL MODIFICAR UNA RESERVA NO SE PUEDE ALTERAR EL DIA Y LA HORA
    this.reservaService
      .modificar(this.reserva, this._sucursalId)
      .subscribe(() => {
        this.messageBoxService.show(<MessageBox>{
          Tipo: MessageTipo.Success,
          Mensaje: 'Se modificó la reserva número ' + this.reserva.id,
          Strong: 'Perfecto. ',
          Float: MessageFloat.StickyFloat,
          Segundos: 10,
        });
        this.volver();
      });
  }

  private crearReserva(reservaForm: NgForm) {
    if (!this.esReservaValida(reservaForm) || !this.camposCorrectos) {
      return;
    }
    this.reserva.id = 0;
    this.reserva.fechaHoraCreacion = new Date();
    this.reservaService
      .crear(this.reserva, this._sucursalId)
      .subscribe((id: number) => {
        this.messageBoxService.show(<MessageBox>{
          Tipo: MessageTipo.Success,
          Mensaje: 'Se guardo la reserva numero ' + id,
          Strong: 'Bien. ',
          Float: MessageFloat.StickyFloat,
          Segundos: 10,
        });
        this.volver();
      });
  }

  volver() {
    this.routerExtenderService.sucursalIdDeReservaPageAGrilla = this._sucursalId;
    this.routerExtenderService.volver();
  }

  private ObtenerSucursalesParaCambiarEstadoReserva() {
    this.authService
      .obtenerSucursalesParaUnRol('SucursalControllerCambiarEstadoReserva', true)
      .subscribe((sucursales: Sucursal[]) => {
        this.sucursalesCambiarEstadoReserva = sucursales;
        console.log(this._sucursalId);
        this.puedeCambiarEstadosReservas = !!this.sucursalesCambiarEstadoReserva.find(
          (sucursal: Sucursal) =>
            sucursal.id.toString() === this._sucursalId.toString()
        );
      });
  }

  private ObtenerSucursalesYUnidadDeReserva() {
    this.reservaService
      .obtenerSucursales()
      .subscribe((sucursales: Sucursal[]) => {
        this.sucursales = sucursales;
        this.sucursal = sucursales.find(
          (s: Sucursal) => s.id.toString() === this._sucursalId.toString()
        );
        this.obtenerUnidadesReserva();
      });
  }

  private obtenerUnidadesReserva() {
    this.reservaService
      .obtenerUnidadesReserva(this.sucursal)
      .subscribe((unidadesReserva) => {
        this.unidadesReserva = unidadesReserva;
        this.reserva.unidadDeReserva = unidadesReserva.find(
          (u) => u.id === this.reserva.unidadDeReservaId
        );
        if (
          this.reserva &&
          this.reserva.unidadDeReserva &&
          this.reserva.unidadDeReserva.id > 0
        ) {
          this.obtenerMatrizParaCalcularTodo();
        }
      });
  }

  private obtenerMotivosReserva() {
    this.reservaService
      .obtenerMotivosReserva(this._sucursalId)
      .subscribe((motivosReserva) => {
        this.motivosReserva = motivosReserva;
        this.reserva.motivo = motivosReserva.find(
          (m) => m.id === this.reserva.motivoId
        );
      });
  }

  private calcularSenia() {
    let senia = 0;
    this.reserva.clientesReserva.forEach((cR) => {
      senia += cR.senia;
    });
    this.reserva.seniaTotal = senia;
  }

  private calcularDeuda() {
    let deuda = 0;
    this.reserva.clientesReserva.forEach((cR) => {
      deuda += cR.deuda;
    });
    this.reserva.deudaTotal = deuda * -1;
  }

  private calcularTotalReserva() {
    this.reserva.totalReserva =
      this.reserva.importeUnidadReservada -
        this.reserva.descuento -
        this.reserva.seniaTotal >
      0
        ? this.reserva.importeUnidadReservada -
          this.reserva.descuento -
          this.reserva.seniaTotal
        : 0;
  }

  private calcularImporte() {
    if (!this.reserva || !this.reserva.unidadDeReserva) {
      return;
    }
    // this._calcularImporte(
    //   this.reserva,
    //   this.reserva.unidadDeReserva.preciosPorHora
    // );
    this.reservaService.calcularImporte(
      this.reserva,
      this.reserva.unidadDeReserva.preciosPorHora
    );
  }

  private esReservaValida(reservaForm: NgForm): boolean {
    let respuesta = true;

    if (!this.reserva) {
      return false;
    }

    // Validar Duración Slider
    if (this.duracionEnMinutos <= 0) {
      this.duracionError = true;
      respuesta = false;
    }

    // Validar que exista comentario de almenos 10 caracteres para un descuento numerico mayor a 0.
    if (
      +this.reserva.descuento &&
      +this.reserva.descuento > 0 &&
      (!this.reserva.comentario || this.reserva.comentario.length < 10)
    ) {
      respuesta = false;
    }

    // Validar Motivo.
    if (!this.reserva.motivo || !this.reserva.motivo.id) {
      return false;
    }

    // Validad Unidad Reserva
    if (!this.reserva.unidadDeReserva || !this.reserva.unidadDeReserva.id) {
      return false;
    }

    // Validar Clientes.
    if (
      !this.reserva.clientesReserva ||
      this.reserva.clientesReserva.length === 0
    ) {
      return false;
    } else {
      for (const c of this.reserva.clientesReserva) {
        if (!c.cliente || !c.cliente.id) {
          return false;
        }
      }
    }

    return respuesta;
  }

  private obtenerReserva() {
    if (
      // this.router.url.indexOf('/ reservas/crear') !== -1 ||
      // this.router.url.indexOf('/ reservas/editar') !== -1
      this.router.url.indexOf('/reserva') !== -1 &&
      this.router.url.indexOf('/reservas') === -1
    ) {
      // Cuando vuelve de clientes
      if (
        // this.routerExtenderService.getPreviousUrl().indexOf('/ clientes/crear') !== -1 ||
        // this.routerExtenderService.getPreviousUrl().indexOf('/ clientes/editar') !== -1
        this.routerExtenderService.getPreviousUrl().indexOf('/cliente') !==
          -1 &&
        this.routerExtenderService.getPreviousUrl().indexOf('/clientes') === -1
      ) {
        if (this.routerExtenderService.reservaDeReservaACliente) {
          this.reserva = this.routerExtenderService.reservaDeReservaACliente;
          this.iniciarReserva();
          this.routerExtenderService.reservaDeReservaACliente = undefined;
          // En caso de que se elimine el cliente
          const filtro = this.reserva.clientesReserva.filter(
            (cR) => cR.cliente && cR.cliente.eliminado
          );
          if (filtro.length > 0) {
            filtro.forEach((cR) => (cR.cliente = undefined));
          }
        }

        if (this.routerExtenderService.clienteCreadoDeClienteAReserva) {
          const clienteReserva = this.reserva.clientesReserva.find(
            (cR) => !cR.cliente || cR.cliente.id === 0
          );
          clienteReserva.cliente = this.routerExtenderService.clienteCreadoDeClienteAReserva;
          clienteReserva.clienteId = clienteReserva.cliente.id;
          clienteReserva.usarSaldo = false;
          clienteReserva.esReservaFija = false;
          this.routerExtenderService.clienteCreadoDeClienteAReserva = undefined;
        }
      }

      // Cuando viene de la grilla
      if (
        this.routerExtenderService.getPreviousUrl().indexOf('/reservas') !== -1
      ) {
        this.reserva = this.routerExtenderService.reservaDeGrillaAReservaPage;
        this.iniciarReserva();
      }

      if (
        (!this.reserva || !this.reserva.id) &&
        this.router.url.indexOf('/reserva/') !== -1
      ) {
        if (this._reservaId && this._sucursalId) {
          this.reservaService
            .getReserva(this._reservaId, this._sucursalId)
            .subscribe((reserva: Reserva) => {
              this.reserva = reserva;
              this.iniciarReserva();
            });
        }
      }
    }
  }

  private iniciarReserva() {
    this.reserva = this.reserva || this.reservaService.getReservaVacia();

    this.fechaReservaMostrar = this.reserva.fechaHoraComienzo || new Date();
    this.minDate = this.reserva.fechaHoraComienzo || new Date();
    this.reserva.fechaHoraFin =
      this.reserva.fechaHoraFin || this.reserva.fechaHoraComienzo || new Date();

    const horaReservaFinEnMinutos = this.reservaService.calcularhoraReservaEnMinutos(
      this.reserva.fechaHoraFin
    );

    this.horaReservaEnMinutosParaMostrar = this.reservaService.calcularhoraReservaEnMinutos(
      this.reserva.fechaHoraComienzo
    );

    this.duracionEnMinutos =
      horaReservaFinEnMinutos > 0
        ? horaReservaFinEnMinutos - this.horaReservaEnMinutosParaMostrar
        : 0;
  }

  private setFechaHoraComienzoReserva(): Date {
    let f = new Date(this._fechaReservaListaDesplegableReal);
    let horas = Math.floor(this.horaReservaEnMinutosParaMostrar / 60);
    if (horas >= 24) {
      horas = horas - 24;
      f = this.timeService.sumarRestarDias(f, 1);
    }
    const minutos = this.horaReservaEnMinutosParaMostrar % 60;
    this.reserva.fechaHoraComienzo = new Date(
      f.getFullYear(),
      f.getMonth(),
      f.getDate(),
      horas,
      minutos
    );
    return this.reserva.fechaHoraComienzo;
  }

  private setFechaHoraFinReserva(): Date {
    const minutos =
      (this.horaReservaEnMinutosParaMostrar + this.duracionEnMinutos) % 60;

    // ¿Qué es la fecha real? Es la fecha que aparece en la lista desplegable de fecha de reserva
    let fecha = new Date(this._fechaReservaListaDesplegableReal);

    let horas = Math.floor(
      (this.horaReservaEnMinutosParaMostrar + this.duracionEnMinutos) / 60
    );

    if (horas >= 24) {
      horas = horas - 24;
      fecha = this.timeService.sumarRestarDias(fecha, 1);
    }

    this.reserva.fechaHoraFin = new Date(
      fecha.getFullYear(),
      fecha.getMonth(),
      fecha.getDate(),
      horas,
      minutos
    );

    return this.reserva.fechaHoraFin;
  }
}
