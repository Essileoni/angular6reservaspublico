import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoSucursalesReservasDialogComponent } from './listado-sucursales-reservas-dialog.component';

describe('ListadoSucursalesReservasDialogComponent', () => {
  let component: ListadoSucursalesReservasDialogComponent;
  let fixture: ComponentFixture<ListadoSucursalesReservasDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoSucursalesReservasDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoSucursalesReservasDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
