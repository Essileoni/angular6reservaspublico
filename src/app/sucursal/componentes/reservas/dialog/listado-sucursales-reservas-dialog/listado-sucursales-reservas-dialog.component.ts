import { Component, OnInit, Inject, defineInjectable } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SucursalReservas } from 'src/app/sucursal/models/surcursal-reservas.model';
import { NgForm } from '@angular/forms';
import { Reserva, Sucursal } from '../../reserva-page/reserva.model';
import { Router } from '@angular/router';
import { RouterExtenderService } from 'src/app/layout/services/router-extender.service';

@Component({
  selector: 'app-listado-sucursales-reservas-dialog',
  templateUrl: './listado-sucursales-reservas-dialog.component.html',
  styleUrls: ['./listado-sucursales-reservas-dialog.component.css'],
})
export class ListadoSucursalesReservasDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<ListadoSucursalesReservasDialogComponent>,
    private router: Router,
    private routerExtenderService: RouterExtenderService
  ) {}

  // PRIVATES

  // PUBLICAS
  titulo = '';
  sucursalesReservas: SucursalReservas[] = [];
  panelNumber = 0;

  ngOnInit() {
    setInterval(() => {
      console.log(this.data);
    }, 10000);
    this.titulo = this.data && this.data.titulo;
    this.sucursalesReservas = this.data && this.data.sucursalesReservas;
  }
  // LISTERNERS
  onSubmit(form: NgForm) {}

  onClickIrGrilla(evento: MouseEvent, sucursal: Sucursal) {
    evento.stopPropagation();
    evento.stopImmediatePropagation();
    this.irAGrilla(sucursal);
  }

  onClickIrAReserva(evento: MouseEvent, reserva: Reserva, sucursal: Sucursal) {
    // evento.stopPropagation();
    // evento.stopImmediatePropagation();
    this.irAReserva(sucursal, reserva);
  }

  // PUBLIC
  irAGrilla(sucursal: Sucursal) {
    if (this.esReservasUrl()) {
      this.routerExtenderService.subjectSucursalIdDeModalPorFinalizarPageAGrilla.next(
        sucursal.id
      );
    } else {
      this.routerExtenderService.sucursalIdDeModalPorFinalizarPageAGrilla =
        sucursal.id;
      this.router.navigate(['reservas']);
    }
    this.dialogRef.close();
  }

  irAReserva(sucursal: Sucursal, reserva: Reserva) {
    this.router.navigate([sucursal.id, 'reserva', 'comenzada', reserva.id]);
    this.dialogRef.close();
  }

  setPanelNumber(index: number) {
    this.panelNumber = index;
  }

  // PRIVATE
  private esReservasUrl(): boolean {
    return location.href.indexOf('/reservas') !== -1;
  }
}
