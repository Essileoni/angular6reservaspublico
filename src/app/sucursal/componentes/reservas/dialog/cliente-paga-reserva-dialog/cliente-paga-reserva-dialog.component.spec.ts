import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientePagaReservaDialogComponent } from './cliente-paga-reserva-dialog.component';

describe('ClientePagaReservaDialogComponent', () => {
  let component: ClientePagaReservaDialogComponent;
  let fixture: ComponentFixture<ClientePagaReservaDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientePagaReservaDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientePagaReservaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
