import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  ElementRef
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { environment } from 'src/environments/environment';
import { NgForm, FormControl, NgControl } from '@angular/forms';
import { ClienteReserva } from '../../../../models/cliente-reserva.model';
import { Reserva } from '../../reserva-page/reserva.model';

import { ClientesService } from 'src/app/empresa/clientes/clientes.service';
import * as cloneDeep from 'node_modules/lodash/cloneDeep';
import {
  MessageBox,
  MessageTipo,
  MessageFloat
} from 'src/app/layout/mensajes/message-box.model';
import { MessageBoxService } from 'src/app/layout/mensajes/messagebox.service';

import { Router, ActivatedRoute } from '@angular/router';
import { TimeService } from 'src/app/layout/services/time.service';
import { ReservaService } from 'src/app/sucursal/services/reserva.service';
import { PrecioPorHora } from 'src/app/sucursal/models/precio-por-hora.model';

@Component({
  selector: 'app-cliente-paga-reserva-dialog',
  templateUrl: './cliente-paga-reserva-dialog.component.html',
  styleUrls: ['./cliente-paga-reserva-dialog.component.css']
})
export class ClientePagaReservaDialogComponent implements OnInit {
  /* Este dialog permite gestionar el pago de una reserva por un cliente*/

  // FORMULARIO
  @ViewChild('clientePagaForm') clientePagaForm: NgForm;
  @ViewChild('pagoInput') pagaInputControl: ElementRef;

  // CONFIGURATION
  appearance = environment.matFormFieldAppearance;

  // PUBLICAS
  pago = 0;
  testVar = null;
  reserva: Reserva;
  clienteReserva: ClienteReserva;
  usarSaldo = true;
  reservaEstaLibreSemanaProxima = false;
  seDecidePasarReserva = null;
  comentarioSaldoEnContra = '';
  esFeriado = false;
  preciosPorHora: PrecioPorHora[];
  private _sucursalId: number;


  get saldoClienteSiemprePositivo(): number {
    return this.clienteReserva.cliente.saldo >= 0 ? this.clienteReserva.cliente.saldo : -this.clienteReserva.cliente.saldo;
  }

  get totalPorPagarPorCliente(): number {
    return (
      this.reserva.importeUnidadReservada /
      this.reserva.clientesReserva.length -
      this.reserva.descuento / this.reserva.clientesReserva.length -
      this.clienteReserva.senia
    );
  }

  get saldoRestante(): number {
    const saldo = (this.usarSaldo && this.clienteReserva.cliente.saldo) || 0;
    return (
      this.clienteReserva.clienteAbonoCon - this.totalPorPagarPorCliente + saldo
    );
  }

  get saldoRestantePositivo(): number {
    if (this.saldoRestante > 0) {
      return this.saldoRestante;
    } else if (this.saldoRestante < 0) {
      return this.saldoRestante * -1;
    }
    return 0;
  }

  get pagoString(): string {
    // return this.pago + '';
    return this.clienteReserva.clienteAbonoCon + '';
  }

  set pagoString(p: string) {
    this.clienteReserva.clienteAbonoCon = +p.trim() || 0;
    // this.pago = +p.trim() || 0;
  }

  private _opcionSiSaldoEsPositivo: string = null;
  get opcionSiSaldoEsPositivo(): string {
    return (
      (this.esSaldoRestantePositivo() &&
        (this._opcionSiSaldoEsPositivo || 'usarComoSenia')) ||
      null
    );
  }
  set opcionSiSaldoEsPositivo(opcion: string) {
    this._opcionSiSaldoEsPositivo = opcion;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<ClientePagaReservaDialogComponent>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private reservaService: ReservaService,
    private messageBoxService: MessageBoxService,
    private clientesService: ClientesService,
    private timeService: TimeService
  ) {
    this.reserva = data.reserva;
    this.clienteReserva = data.clienteReserva;
  }

  ngOnInit() {
    this.obtenerSucursalId();
    this.obtenerReservaEstaLiberadaParaUnaFechaHoraCancha();
    this.reservaService
      .obtenerMatrizPreciosUnidadReserva(this.reserva.unidadDeReservaId, this._sucursalId)
      .subscribe((preciosPorHora: PrecioPorHora[]) => {
        this.preciosPorHora = preciosPorHora;
      });
  }

  // PUBLICAS
  proximaSemana(): Date {
    const fechaGrillaDeLaReserva = this.reservaService.obtenerFechaGrillaDeUnaReserva(
      this.reserva
    );
    return this.timeService.sumarRestarDias(fechaGrillaDeLaReserva, 7);
  }

  // LISTERNERS
  onSubmit(form: NgForm, pagoInput: any) {
    if (
      form.invalid ||
      !this.preciosPorHora ||
      this.clienteReserva.clienteAbonoCon < 0
    ) {
      return;
    }
    const reservaClone = this.actualizarReserva();
    this.guardarReservaModificada(reservaClone);
  }

  onNumberKeyDown(event: KeyboardEvent) {
    const arreglo = ['e', 'E', '-', '+', '.', ',', 'ArrowUp', 'ArrowDown'];
    if (arreglo.indexOf(event.key) !== -1) {
      return false;
    }
  }

  // VALIDACIONES
  esSaldoRestantePositivo() {
    return (
      this.clienteReserva.clienteAbonoCon +
      (this.usarSaldo && this.clienteReserva.cliente.saldo) -
      this.totalPorPagarPorCliente >
      0
    );
  }

  esSaldoRestanteNegativo() {
    return (
      this.clienteReserva.clienteAbonoCon +
      (this.usarSaldo && this.clienteReserva.cliente.saldo) -
      this.totalPorPagarPorCliente <
      0
    );
  }

  clienteReservaPago(cR: ClienteReserva): boolean {
    return this.reservaService.clienteReservaPago(cR, this.reserva);
  }

  // PRIVATE
  private obtenerReservaEstaLiberadaParaUnaFechaHoraCancha() {
    const fechaHoraComienzoProximaSemana = this.timeService.sumarRestarDias(this.reserva.fechaHoraComienzo, 7);
    // const fechaHoraComienzoProximaSemana = new Date(
    //   this.reserva.fechaHoraComienzo.getTime() + 1000 * 60 * 60 * 24 * 7
    // );
    const fechaHoraFinProximaSemana = this.timeService.sumarRestarDias(this.reserva.fechaHoraFin, 7);
    // const fechaHoraFinProximaSemana = new Date(
    //   this.reserva.fechaHoraFin.getTime() + 1000 * 60 * 60 * 24 * 7
    // );
    this.reservaService
      .obtenerReservaEstaLiberadaParaUnaFechaHoraCancha(
        fechaHoraComienzoProximaSemana,
        fechaHoraFinProximaSemana,
        this.reserva.unidadDeReservaId,
        this._sucursalId
      )
      .subscribe((respuesta: boolean) => {
        this.reservaEstaLibreSemanaProxima = respuesta;
      });
  }

  private guardarReservaModificada(reservaClone: Reserva) {
    const clienteReservaClone: ClienteReserva = this.buscarClienteReserva(
      reservaClone,
      this.clienteReserva.cliente.id
    );

    this.reservaService.cambiarEstadoReserva(reservaClone, this._sucursalId).subscribe(() => {
      this.clientesService
        .actualizarSaldoCliente(clienteReservaClone.cliente)
        .subscribe((msj: string) => {
          // NOTA: el semaforo se aplica cuando comienza la reserva, por que la persona ya está acá.
          // this.clientesService
            // .aplicarSemaforo([clienteReservaClone])
            // .subscribe((msjs: string[]) => {
              // Si salió todo bien piso el nuevo objeto por el viejo.
              this.reserva = reservaClone;
              if (this.seDecidePasarReserva) {
                this.pasarReserva();
              } else {
                this.messageBoxService.show(<MessageBox>{
                  Tipo: MessageTipo.Success,
                  Mensaje:
                    'Finalizó el pago de ' +
                    this.clienteReserva.cliente.nombreYApellido,
                  Strong: 'Listo. ',
                  Float: MessageFloat.StickyFloat,
                  Segundos: 10
                });
                if (this.noFaltaNadiePorPagar()) {
                  // this.router.navigate(["reservas"]);
                }
              }

              this.dialogRef.close(this.reserva);
            // });
        });
    });
  }

  private obtenerSucursalId() {
    this._sucursalId = +this.data.sucursalId;
  }

  private esGuardarSaldoCliente(): boolean {
    return this.opcionSiSaldoEsPositivo === 'guardarSaldo';
  }

  private esDarVueltoACliente(): boolean {
    return this.opcionSiSaldoEsPositivo === 'darVuelto';
  }

  private pasarReserva() {
    // Verificar si existe posibilidad de reserva directa.
    if (this.reservaEstaLibreSemanaProxima) {
      const reservaSemanaProxima: Reserva = this.reservaService.getReservaVacia();
      const clienteReservaSemanaProxima: ClienteReserva = this.reservaService.getClienteReservaVacio();

      clienteReservaSemanaProxima.clienteId = this.clienteReserva.clienteId;
      clienteReservaSemanaProxima.senia = this.obtenerSeniaParaUnSaldoSiCorresponde();

      reservaSemanaProxima.fechaHoraComienzo = this.timeService.sumarRestarDias(
        this.reserva.fechaHoraComienzo,
        7
      );
      reservaSemanaProxima.fechaHoraFin = this.timeService.sumarRestarDias(
        this.reserva.fechaHoraFin,
        7
      );
      reservaSemanaProxima.fechaHoraCreacion = new Date();

      reservaSemanaProxima.motivoId = this.reserva.motivoId;
      reservaSemanaProxima.unidadDeReservaId = this.reserva.unidadDeReservaId;
      reservaSemanaProxima.seniaTotal = clienteReservaSemanaProxima.senia;

      reservaSemanaProxima.comentario = this.seDecidePasarReserva
        ? 'Se pasó la reserva del ' +
        this.reserva.fechaHoraComienzo.getDate() +
        '/' +
        (this.reserva.fechaHoraComienzo.getMonth() + 1) +
        '/' +
        this.reserva.fechaHoraComienzo.getFullYear()
        : null;

      reservaSemanaProxima.clientesReserva = [clienteReservaSemanaProxima];

      reservaSemanaProxima.esFeriado = this.esFeriado;
      this.reservaService.calcularImporte(
        reservaSemanaProxima,
        this.preciosPorHora
      );
      reservaSemanaProxima.totalReserva =
        reservaSemanaProxima.importeUnidadReservada -
        reservaSemanaProxima.seniaTotal;

      this.reservaService
        .crear(reservaSemanaProxima, this._sucursalId)
        .subscribe((id: number) => {
          this.messageBoxService.show(<MessageBox>{
            Tipo: MessageTipo.Success,
            Mensaje:
              'Se creó la reserva número ' + id + ' para la próxima semana',
            Strong: 'Bien. ',
            Float: MessageFloat.StickyFloat,
            Segundos: 10
          });
          // En caso de ser seña y hay posibilidad re reservar
          // Verificar si es seña, saldo, vuelto antes de redireccionar

          // En caso de ser saldo guardarlo siempre
          // En caso de ser vuelto no hacer nada

          // No hay Redireccion, por que debe volver a pagar el otro cliente o cerrar la reserva.
        });
    } else {
      // En caso de ser seña y no hay posbilidad de reserva, pasar a grilla con seña.
      // Redireccion
      const arregloRedireccion: any[] = [
        'reservas',
        this.timeService.sumarRestarDias(this.reserva.fechaHoraComienzo, 7).toISOString()
      ];
      if (this.obtenerSeniaParaUnSaldoSiCorresponde()) {
        arregloRedireccion.push(this.obtenerSeniaParaUnSaldoSiCorresponde());
      }
      this.router.navigate(arregloRedireccion);
    }

    // Si es reserva directa reservar derecho sin intervencion (con seña o saldo si correcponde.) Redireccionar a grilla y mostrar reserva.

    // Si no existe una cancha pone ¿Quiere pasar para la próxima semana? en caso de si redirecciona con la senia fija, no se puede "editar";
    // Pero si existe ya hace la reserva y redirecciona para visualizar la misma.
  }

  private obtenerSeniaParaUnSaldoSiCorresponde(): number {
    return this.opcionSiSaldoEsPositivo === 'usarComoSenia'
      ? this.saldoRestantePositivo
      : 0;
  }

  private noFaltaNadiePorPagar(): boolean {
    return this.reservaService.noFaltaNadiePorPagar(this.reserva);
  }

  private actualizarReserva() {
    // Trabajo con copias y no lo originales.
    const reservaClone: Reserva = cloneDeep(this.reserva);
    const clienteReservaClone = this.buscarClienteReserva(
      reservaClone,
      this.clienteReserva.cliente.id
    );

    // Sumar comentario si corresponde
    if (this.esSaldoRestanteNegativo()) {
      reservaClone.comentario +=
        ' Saldo En Contra: ' +
        this.clienteReserva.cliente.nombreYApellido +
        ' :' +
        this.comentarioSaldoEnContra;
    }

    this.actualizarReservaCliente(clienteReservaClone);

    return reservaClone;
  }

  private buscarClienteReserva(reserva: Reserva, clienteId: number) {
    return reserva.clientesReserva.find(
      (c: ClienteReserva) => c.cliente.id === clienteId
    );
  }

  private actualizarReservaCliente(clienteReserva: ClienteReserva) {
    clienteReserva.importeUnidadReservaParte =
      this.reserva.importeUnidadReservada / this.reserva.clientesReserva.length;
    clienteReserva.usarSaldo = this.usarSaldo;

    clienteReserva.deuda = 0;
    clienteReserva.saldoAFavor = 0;

    // Determinar si suma saldo o reemplaza el anterior.
    const saldoAnteriorOCero = this.usarSaldo
      ? 0
      : this.clienteReserva.cliente.saldo;

    // Registrar saldoAFavor usado al final en la reserva si corresponde.
    clienteReserva.saldoAFavor =
      this.clienteReserva.cliente.saldo > 0 && this.usarSaldo
        ? this.clienteReserva.cliente.saldo
        : 0;

    // Registrar deuda pagada al final de la reserva si corresponde.
    clienteReserva.deuda =
      this.clienteReserva.cliente.saldo <= 0 && this.usarSaldo
        ? this.clienteReserva.cliente.saldo
        : 0;

    // Determinar nuevo saldo.
    clienteReserva.cliente.saldo =
      (this.saldoRestante > 0 && this.esGuardarSaldoCliente()) ||
        this.saldoRestante < 0
        ? this.saldoRestante + saldoAnteriorOCero
        : 0;

    if (this.esDarVueltoACliente()) {
      clienteReserva.clienteAbonoCon -= this.saldoRestante;
    }

    clienteReserva.estadoCumplimiento = 'Cumplió';
  }
}

