import { Component, OnInit, Inject } from '@angular/core';
import { environment } from 'src/environments/environment';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ClienteReserva } from '../../../../models/cliente-reserva.model';
import { ClientesReservaExtension } from '../../../../models/clientes-reserva-extension.model';
import { Reserva } from '../../reserva-page/reserva.model';
import { MotivoBajaReserva } from 'src/app/sucursal/models/motivo-baja-reserva.model';


@Component({
  selector: 'app-borrar-reserva-dialog',
  templateUrl: './borrar-reserva-dialog.component.html',
  styleUrls: ['./borrar-reserva-dialog.component.css']
})
export class BorrarReservaDialogComponent implements OnInit {
  /* Este dialog permite gestionar la eliminacion de una reserva*/

  // Configuracion
  appearance = environment.matFormFieldAppearance;

  // Variables Globales
  comentario = '';
  motivo: MotivoBajaReserva = null;
  motivosDeBaja: MotivoBajaReserva[] = [
    <MotivoBajaReserva>{
      id: 1,
      descripcion: 'Decisión de la sucursal',
      semaforo: null
    },
    <MotivoBajaReserva>{
      id: 2,
      descripcion: 'Antes de las 24hs',
      semaforo: null
    },
    <MotivoBajaReserva>{
      id: 3,
      descripcion: 'Dentro de las 24 hs',
      semaforo: 'naranja'
    },
    <MotivoBajaReserva>{
      id: 4,
      descripcion: 'Sin aviso',
      semaforo: 'rojo'
    }
  ];
  clientesReservaExt: ClientesReservaExtension[] = [];
  errorForm: boolean;
  reserva: Reserva;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<BorrarReservaDialogComponent>
  ) {
    this.reserva = data.reserva;
    this.reserva.clientesReserva.forEach((cR: ClienteReserva) => {
      this.clientesReservaExt.push({
        clienteReserva: cR,
        cancela: false
      });
    });
  }

  ngOnInit() {
  }

  esDecisionDeLaSucursal(): boolean {
    return this.motivo && this.motivo.descripcion === 'Decisión de la sucursal';
  }

  onSubmit(form: NgForm) {
    if (form.invalid) {
      return;
    }

    // Si se va a aplicar un semaforo y Ningún cliente está seleccionado -> Mostrar Error
    if (this.motivo.semaforo && !this.clientesReservaExt.find(cRE => cRE.cancela)) {
      this.errorForm = true;
      return;
    }

    // Para cada cliente
    this.clientesReservaExt.forEach(cRE => {

      if (this.motivo.semaforo && cRE.cancela) {
        cRE.clienteReserva.estadoCumplimiento = this.determinarEstadoCumplimiento();
        cRE.clienteReserva.comentarioBaja = null;
      }

      if (!this.motivo.semaforo) {
        cRE.clienteReserva.comentarioBaja = this.comentario.trim() || null;
        cRE.clienteReserva.estadoCumplimiento = 'No Aplica';
      }

    });
    // if (!this.esDecisionDeLaSucursal() && !this.clientesReservaExt.find(cRE => cRE.cancela)) {
    //   this.errorForm = true;
    //   return;
    // }

    // this.clientesReservaExt.forEach(cRE => {
    //   if (!this.esDecisionDeLaSucursal() || cRE.cancela) {
    //     cRE.clienteReserva.estadoCumplimiento = this.determinarEstadoCumplimiento();
    //     cRE.clienteReserva.comentarioBaja = null;
    //   }

    //   if (this.esDecisionDeLaSucursal()) {
    //     cRE.clienteReserva.comentarioBaja = this.comentario.trim() || null;
    //     cRE.clienteReserva.estadoCumplimiento = 'No Aplica';
    //   }

    // });

    this.dialogRef.close(true);
  }

  private determinarEstadoCumplimiento(): string {
    // const time = new Date().getTime() - this.reserva.fechaHoraComienzo.getTime();
    // const horas = time / (1000 * 60 * 60);

    // if (horas >= 24) {
    //   return 'No Aplica';
    // } else if (horas > 0) {
    //   return 'Infracción';
    // } else if (horas <= 0) {
    //   return 'Infracción Grave';
    // }

    // return null;

    if (this.motivo.semaforo === null) {
      return 'No Aplica';
    } else if (this.motivo.semaforo === 'naranja') {
      return 'Infracción';
    } else if (this.motivo.semaforo === 'rojo') {
      return 'Infracción Grave';
    }

    return null;
  }



}
