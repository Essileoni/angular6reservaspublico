import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrarReservaDialogComponent } from './borrar-reserva-dialog.component';

describe('BorrarReservaDialogComponent', () => {
  let component: BorrarReservaDialogComponent;
  let fixture: ComponentFixture<BorrarReservaDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrarReservaDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrarReservaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
