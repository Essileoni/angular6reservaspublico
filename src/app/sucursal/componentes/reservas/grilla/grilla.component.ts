import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Reserva, Sucursal } from '../reserva-page/reserva.model';
import { environment } from 'src/environments/environment';
import { MenuService } from 'src/app/layout/menu/menu.service';
import { RouterExtenderService } from 'src/app/layout/services/router-extender.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { TimeService } from 'src/app/layout/services/time.service';
import { AuthService } from 'src/app/layout/auth/auth.service';
import { finalize } from 'rxjs/operators';
import { ReservaService } from 'src/app/sucursal/services/reserva.service';
import { UnidadReserva } from 'src/app/sucursal/models/unidad-reserva.model';
import { Subscription } from 'rxjs';
import { MonitorReservasService } from 'src/app/sucursal/services/monitor-reservas.service';

@Component({
  selector: 'app-grilla',
  templateUrl: './grilla.component.html',
  styleUrls: ['./grilla.component.css'],
})
export class GrillaComponent implements OnInit, OnDestroy {
  // Config
  tiempoRefrescoGrilla: number = environment.ReservaCrear.tiempoRefrescoGrilla;
  appearance: string = environment.matFormFieldAppearance;
  private step = environment.ReservaCrear.step;
  private horaComienzoDia = environment.ReservaCrear.comienzoDia / 60;

  @Input() hours = ['6:00', '7:00', '8:00', '9:00'];
  @Input() unidadesDeReservaActivas: UnidadReserva[] = [];

  private reservas: Reserva[] = [];

  fechaReserva: Date;
  stepsPerHour = 60 / this.step;
  unidadesDeReservaFakeArray = [];
  stepsPerHourFakeArray = [];
  hoursAdaptado = [];
  lastDates: any[];

  reservasVacias = [];
  filas: Reserva[][] = [];
  reservasEliminadas: Reserva[] = [];
  esFullScreen = false;
  configThead = { suppressScrollX: false, suppressScrollY: true };

  reservaACrear = null;
  nombreDiaSemana = '';

  semanasDesdeLaSemanaCero = 0;
  diasDesdeElDiaCero = 0;
  matHintLeftDatePickerSinInput = '';
  showGrilla = true;
  intervaloGrilla: any;
  intervaloUnidadesReserva: any;

  reservasPendientes: Reserva[] = [];
  reservasCorriendo: Reserva[] = [];
  reservasFinalizadas: Reserva[] = [];
  reservasCerradas: Reserva[] = [];
  reservasConAviso: Reserva[] = [];
  reservasSinAviso: Reserva[] = [];
  sucursales: Sucursal[] = [];
  sucursalSeleccionada: Sucursal = null;
  sucursalPredeterminada: Sucursal;
  private _sucursalIdDeUltimaReserva: number;
  private actualizarGrillaTimeOut: any;
  subscripcionModalPorFinalizar: Subscription;
  subscripcionCambioRuta: Subscription;

  constructor(
    private menuService: MenuService,
    private reservaService: ReservaService,
    private authService: AuthService,
    private routerExtenderService: RouterExtenderService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private timeService: TimeService,
    private monitorReservasService: MonitorReservasService
  ) {
    this.obtenerSucursalIdAlVolver();
    this.obtenerFechaAlVolver();
    this.configurarFullScreen();
  }

  ngOnInit() {
    // this.actualizarGrillaCuandoExistanUnidadesDeReserva();
    this.inicializarSucuralesUnidadesReservaGrilla();
  }

  ngOnDestroy() {
    if (this.intervaloGrilla) {
      clearInterval(this.intervaloGrilla);
    }
    if (this.actualizarGrillaTimeOut) {
      clearTimeout(this.actualizarGrillaTimeOut);
    }
    this.subscripcionModalPorFinalizar.unsubscribe();
  }

  calcularFilaHora(fila: Reserva[]): string {
    return fila[0].fechaHoraComienzo.getHours() + ':00';
  }

  isFilaHora(fila: Reserva[]): boolean {
    if (fila[0].fechaHoraComienzo.getMinutes() === 0) {
      return true;
    }
    return false;
  }

  saltarDias(dias: number) {
    this.fechaReserva = this.timeService.sumarRestarDias(
      this.fechaReserva,
      dias
    );

    this.onDateInput();
  }

  // LISTENERS

  onClickFullScreen() {
    this.fullscreenToggle();
  }

  onClickQuitarfullScreen() {
    this.quitarFullScreen();
  }

  onClickReserva(reserva: Reserva) {
    this.quitarFullScreen();

    this.routerExtenderService.reservaDeGrillaAReservaPage = reserva;

    const idSucursal = this.sucursalSeleccionada.id;

    if (reserva.id) {
      if (this.reservaService.estaPendiente(reserva)) {
        this.router.navigate([idSucursal + '/reserva', reserva.id]);
      } else {
        this.router.navigate([idSucursal + '/reserva/comenzada', reserva.id]);
      }
    } else {
      this.router.navigate([idSucursal + '/reserva']);
    }

    const estaCorriendo =
      !!reserva.fechaHoraComienzoReal && !reserva.fechaHoraFinReal;
    const estaFinalizada = !!reserva.fechaHoraFinReal;
    const estaPendiente =
      !reserva.fechaHoraComienzoReal && !!reserva.fechaHoraFin;
    const estaVacia = !(estaCorriendo || estaPendiente || estaFinalizada);
  }

  onDateInput() {
    const fechaGrillaActualAdaptada = this.timeService.obtenerFechaInicialParaFranjaHoraria();

    const dias =
      (this.fechaReserva.getTime() - fechaGrillaActualAdaptada.getTime()) /
      (1000 * 60 * 60 * 24);

    this.diasDesdeElDiaCero = dias;
    this.semanasDesdeLaSemanaCero = Math.trunc(dias / 7);

    const diasDesdeElDiaCeroText = this.diasDesdeElDiaCero
      ? 'D ' +
        (this.diasDesdeElDiaCero > 0 ? '+' : '') +
        this.diasDesdeElDiaCero
      : '';
    const semanasDesdeLaSemanaCeroText = this.semanasDesdeLaSemanaCero
      ? 'S ' +
        (this.semanasDesdeLaSemanaCero > 0 ? '+' : '') +
        this.semanasDesdeLaSemanaCero
      : '';
    this.matHintLeftDatePickerSinInput =
      semanasDesdeLaSemanaCeroText +
      (semanasDesdeLaSemanaCeroText ? ' / ' : '') +
      diasDesdeElDiaCeroText;

    this.nombreDiaSemana =
      environment.ReservaCrear.nombresMostrarDiasSemana[
        this.fechaReserva.getDay()
      ];

    this.actualizarGrillaUnaSolaVez();
  }

  onScrollX() {
    // Si movemos el cuerpo mueve los header, si intentamos mover lo headers vuelve a su posición normal
    let pSBody = null;
    let pSRailX = null;
    for (let i = 0; i < document.getElementsByClassName('ps').length; i++) {
      if (
        (
          document
            .getElementsByClassName('ps')
            [i].parentElement.getAttribute('class') + ''
        ).split('ps-tbody').length === 2
      ) {
        pSBody = document.getElementsByClassName('ps')[i];
      }
    }

    for (const child of pSBody.children) {
      if ((child.getAttribute('class') + '').split('ps__rail-x').length === 2) {
        pSRailX = child;
      }
    }

    const left = pSRailX
      .getAttribute('style')
      .split(';')
      .find((s: any) => s.indexOf('left') !== -1)
      .split(':')[1]
      .replace('px', '')
      .trim();

    let pSRailXHeader = null;
    for (let i = 0; i < document.getElementsByClassName('ps').length; i++) {
      if (
        (
          document
            .getElementsByClassName('ps')
            [i].parentElement.getAttribute('class') + ''
        ).split('ps-thead').length === 2
      ) {
        pSRailXHeader = document.getElementsByClassName('ps')[i];
      }
    }
    pSRailXHeader.scrollLeft = +left;
  }

  onScrollY() {
    // Si movemos el cuerpo mueve las horas, si intentamos mover las horas vuelven a su posición normal
    let pSBody = null;
    let pSRailY = null;
    for (let i = 0; i < document.getElementsByClassName('ps').length; i++) {
      if (
        (
          document
            .getElementsByClassName('ps')
            [i].parentElement.getAttribute('class') + ''
        ).split('ps-tbody').length === 2
      ) {
        pSBody = document.getElementsByClassName('ps')[i];
      }
    }

    for (const child of pSBody.children) {
      if ((child.getAttribute('class') + '').split('ps__rail-y').length === 2) {
        pSRailY = child;
      }
    }

    const top = pSRailY
      .getAttribute('style')
      .split(';')
      .find((s: any) => s.indexOf('top') !== -1)
      .split(':')[1]
      .replace('px', '')
      .trim();

    let pSRailYHours = null;
    for (let i = 0; i < document.getElementsByClassName('ps').length; i++) {
      if (
        (
          document
            .getElementsByClassName('ps')
            [i].parentElement.getAttribute('class') + ''
        ).split('ps-thours').length === 2
      ) {
        pSRailYHours = document.getElementsByClassName('ps')[i];
      }
    }

    pSRailYHours.scrollTop = +top;
  }

  selectSucursalChange(sucursalSelected: Sucursal) {
    this.sucursalSeleccionada = sucursalSelected;
    this.actualizarGrilla();
  }

  log(obj: any) {
    console.log(obj);
  }

  // PRIVATES

  private actualizarGrillaPeriodicamente() {
    this.actualizarGrilla();
  }

  private inicializarFechasAlInicio() {
    this.fechaReserva =
      this.fechaReserva ||
      this.obtenerFechaURL() ||
      // this.timeService.obtenerFechaHoySinHora();
      this.timeService.obtenerFechaInicialParaFranjaHoraria();
    this.nombreDiaSemana =
      environment.ReservaCrear.nombresMostrarDiasSemana[
        this.fechaReserva.getDay()
      ];
  }

  private obtenerFechaURL(): Date {
    const fechaJSON = this.activatedRoute.snapshot.params['fecha'];
    if (fechaJSON) {
      return new Date(fechaJSON);
    }
    return undefined;
  }

  private obtenerSucursalIdAlVolver() {
    if (this.laUrlAnteriorEraReserva()) {
      this._sucursalIdDeUltimaReserva =
        this.routerExtenderService.sucursalIdDeReservaPageAGrilla ||
        this.routerExtenderService.sucursalIdDeReservaComenzadaAGrilla;
      this.routerExtenderService.sucursalIdDeReservaPageAGrilla = undefined;
      this.routerExtenderService.sucursalIdDeReservaComenzadaAGrilla = undefined;
      return;
    }

    if (this.routerExtenderService.sucursalIdDeModalPorFinalizarPageAGrilla) {
      this._sucursalIdDeUltimaReserva = this.routerExtenderService.sucursalIdDeModalPorFinalizarPageAGrilla;
      this.routerExtenderService.sucursalIdDeModalPorFinalizarPageAGrilla = undefined;
      return;
    }
  }

  private laUrlAnteriorEraReserva(): boolean {
    return (
      this.routerExtenderService.getPreviousUrl().indexOf('/reserva') !== -1 &&
      this.routerExtenderService.getPreviousUrl().indexOf('/reservas') === -1
    );
  }

  private obtenerFechaAlVolver() {
    if (this.laUrlAnteriorEraReserva()) {
      const reserva = this.routerExtenderService.reservaDeGrillaAReservaPage;
      this.routerExtenderService.reservaDeGrillaAReservaPage = undefined;

      let fechaGrilla: Date;

      if (reserva) {
        fechaGrilla = this.timeService.extraerFechaDeUnaFechaYHora(
          reserva.fechaHoraComienzo
        );

        const horaComienzoReserva =
          reserva.fechaHoraComienzo.getHours() +
          reserva.fechaHoraComienzo.getMinutes() / 60;

        if (horaComienzoReserva < this.horaComienzoDia) {
          fechaGrilla = this.timeService.sumarRestarDias(fechaGrilla, -1);
        }
      }
      this.fechaReserva = fechaGrilla;
    }
  }

  private obtenerUnidadesDeReserva() {
    this.reservaService
      .obtenerUnidadesReserva(this.sucursalSeleccionada)
      .subscribe((unidades) => {
        this.unidadesDeReservaActivas = unidades;
      });
  }

  private configurarFullScreen() {
    this.menuService
      .suscribirFullScreen()
      .subscribe((esFullScreen) => (this.esFullScreen = esFullScreen));
  }

  private actualizarGrillaUnaSolaVez() {
    if (!this.sucursalSeleccionada) {
      return;
    }
    this.reservaService
      .obtenerReservasParaUnaFechaUnaUnidadesDeReservaYUnaSucursal(
        this.fechaReserva,
        this.sucursalSeleccionada
      )
      .subscribe((resultado: [Reserva[], Reserva[], UnidadReserva[]]) => {
        this.subscribirParaActualizarGrilla(resultado);
      });
  }

  private actualizarGrilla() {
    if (!this.sucursalSeleccionada) {
      return;
    }
    this.reservaService
      .obtenerReservasParaUnaFechaUnaUnidadesDeReservaYUnaSucursal(
        this.fechaReserva,
        this.sucursalSeleccionada
      )
      .pipe(
        // El finalize ocurre despues del subscribe
        finalize(() => {
          this.VolverActualizarGrillaMasTarde();
        })
      )
      .subscribe((resultado: [Reserva[], Reserva[], UnidadReserva[]]) => {
        this.subscribirParaActualizarGrilla(resultado);
      });
  }

  private subscribirParaActualizarGrilla(
    resultado: [Reserva[], Reserva[], UnidadReserva[]]
  ) {
    const reservasParaUnaFecha = resultado[0];
    const reservasEliminadasParaUnaFecha = resultado[1];
    const unidadesDeReserva = resultado[2];

    this.unidadesDeReservaActivas = unidadesDeReserva.filter(
      (ur: UnidadReserva) => ur.activo
    );
    this.reservas = reservasParaUnaFecha;
    this.registrarReservasPorFinalizar(this.reservas);
    this.reservasEliminadas = reservasEliminadasParaUnaFecha;
    this.generarReservasVacia();
    this.transponerArray();
    this.generarEstadisticas();
  }

  private registrarReservasPorFinalizar(reservas: Reserva[]) {
    const reservasPorFinalizar = this.monitorReservasService.FiltrarReservasCorriendoPorTerminarDentroJornada(
      reservas
    );
    reservasPorFinalizar.forEach((r) => (r.estaPorAgotarseElTiempo = true));
  }

  private VolverActualizarGrillaMasTarde() {
    this.actualizarGrillaTimeOut = setTimeout(() => {
      this.actualizarGrilla();
    }, this.tiempoRefrescoGrilla * 1000);
  }

  private generarEstadisticas() {
    this.reservasPendientes = this.reservas.filter((reserva) =>
      this.reservaService.estaPendiente(reserva)
    );
    this.reservasCorriendo = this.reservas.filter((reserva) =>
      this.reservaService.estaCorriendo(reserva)
    );
    this.reservasFinalizadas = this.reservas.filter((reserva) =>
      this.reservaService.estaFinalizada(reserva)
    );
    this.reservasCerradas = this.reservas.filter((reserva) =>
      this.reservaService.estaCerrada(reserva)
    );
    this.reservasConAviso = this.reservasEliminadas.filter((reserva) =>
      this.reservaService.esReservaConInfraccion(reserva)
    );
    this.reservasSinAviso = this.reservasEliminadas.filter((reserva) =>
      this.reservaService.esReservaConInfraccionGrave(reserva)
    );
  }

  private generarReservasVacia() {
    this.reservasVacias = [];
    const horaFin = this.horaComienzoDia; // los dias son de 24 hs, por lo tanto termina cuando comienzan.
    const horaFinAdaptada = horaFin + 24;
    for (const unidad of this.unidadesDeReservaActivas) {
      const reservasPorUnidad: Reserva[] = [];
      for (
        let h = this.horaComienzoDia;
        h < horaFinAdaptada;
        h = h + this.step / 60
      ) {
        let hora = h;
        let fecha = this.fechaReserva;

        if (h >= 24) {
          hora = h - 24;
          fecha = this.timeService.sumarRestarDias(fecha, 1); // Sumar un dia;
        }

        const reservaVacia = this.reservaService.getReservaVacia();
        reservaVacia.fechaHoraComienzo = this.timeService.sumarRestarHoras(
          fecha,
          hora
        );
        reservaVacia.fechaHoraFinPaso = this.timeService.sumarRestarHoras(
          fecha,
          hora + this.step / 60
        );

        reservaVacia.unidadDeReservaId = unidad.id;
        reservaVacia.unidadDeReserva = unidad;
        const reservaBuscada: Reserva = this.buscarReserva(reservaVacia);
        if (reservaBuscada) {
          reservasPorUnidad.push(reservaBuscada);
          continue;
        }

        reservasPorUnidad.push(reservaVacia);
      }
      this.reservasVacias.push(reservasPorUnidad);
    }
  }

  private buscarReserva(reservaVacia: Reserva): Reserva {
    const reservaBuscada = this.reservas.find(
      (r) =>
        reservaVacia.unidadDeReservaId === r.unidadDeReservaId &&
        this.isEqualDateTime(
          r.fechaHoraComienzo,
          reservaVacia.fechaHoraComienzo
        )
    );

    return reservaBuscada;
  }

  private transponerArray() {
    this.filas = [];
    for (let i = 0; i < this.reservasVacias[0].length; i++) {
      const fila = [];
      for (const reservasPorUnidad of this.reservasVacias) {
        fila.push(reservasPorUnidad[i]);
      }
      this.filas.push(fila);
    }
  }

  private isEqualDateTime(time1: Date, time2: Date) {
    if (time1 && time2) {
      return time1.getTime() === time2.getTime();
    }
  }

  private fullscreenToggle() {
    this.menuService.fullScreen(!this.esFullScreen);
  }

  private quitarFullScreen() {
    this.menuService.fullScreen(false);
  }

  private inicializarSucursales(sucursales: Sucursal[]) {
    const idSucursalPredeterminada = this.authService.obtenerSucursalPredeterminadaId();
    this.sucursales = sucursales;
    this.sucursalPredeterminada = sucursales.find(
      (s) => s.id.toString() === idSucursalPredeterminada
    );
    const existeSucursalDeLaUltimaReserva = !!this._sucursalIdDeUltimaReserva;
    const sucursalDeLaUltimaReserva: Sucursal = existeSucursalDeLaUltimaReserva
      ? sucursales.find(
          (s) => s.id.toString() === this._sucursalIdDeUltimaReserva.toString()
        )
      : null;
    this.sucursalSeleccionada =
      sucursalDeLaUltimaReserva || this.sucursalPredeterminada;
    if (!this.sucursalSeleccionada && sucursales.length > 0) {
      this.sucursalSeleccionada = sucursales[0];
    }
  }

  private inicializarSucuralesUnidadesReservaGrilla() {
    this.inicializarFechasAlInicio();
    this.reservaService
      .obtenerSucursales()
      .subscribe((sucursales: Sucursal[]) => {
        this.inicializarSucursales(sucursales);
        this.obtenerUnidadesDeReserva();
        this.actualizarGrillaPeriodicamente();
      });
    this.subscribirModalPorFinalizar();
  }

  private subscribirModalPorFinalizar() {
    this.subscripcionModalPorFinalizar = this.routerExtenderService.subjectSucursalIdDeModalPorFinalizarPageAGrilla
      .asObservable()
      .subscribe((sucursalId: number) => {
        const sucursalSeleccionada = this.sucursales.find(
          (s) => s.id === sucursalId
        );
        this.selectSucursalChange(sucursalSeleccionada);
      });
  }
}
