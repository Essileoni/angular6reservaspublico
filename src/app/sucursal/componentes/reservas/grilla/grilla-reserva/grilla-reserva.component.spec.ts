import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrillaReservaComponent } from './grilla-reserva.component';

describe('GrillaReservaComponent', () => {
  let component: GrillaReservaComponent;
  let fixture: ComponentFixture<GrillaReservaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrillaReservaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrillaReservaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
