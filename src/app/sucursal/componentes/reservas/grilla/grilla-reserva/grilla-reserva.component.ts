import { Component, OnInit, Input } from '@angular/core';
import { Reserva } from '../../reserva-page/reserva.model';
import { environment } from 'src/environments/environment';
import { faFutbol } from '@fortawesome/free-solid-svg-icons';
import { ReservaService } from 'src/app/sucursal/services/reserva.service';

@Component({
  selector: 'app-grilla-reserva',
  templateUrl: './grilla-reserva.component.html',
  styleUrls: ['./grilla-reserva.component.css'],
})
export class GrillaReservaComponent implements OnInit {
  F;
  @Input() reserva: Reserva;



  private stepTime = environment.ReservaCrear.step;
  // private minutos = 0;
  // estaCorriendo = false;
  // estaFinalizada = false;
  // estaPendiente = false;
  // estaVacia = true;
  // esReservaMenorUnaHora = false;
  // esReservaMultiple = false;
  faFutbol = faFutbol;

  constructor(private reservaService: ReservaService) {

  }

  ngOnInit() {
    // this.inicializarVariables();
  }

  inicializarVariables() {
    // this.minutos = this.reservaService.calcularMinutosEntreDateTime(
    //   this.reserva.fechaHoraComienzo,
    //   this.reserva.fechaHoraFin
    // );
    // this.estaCorriendo = !!this.reserva.fechaHoraComienzoReal && !this.reserva.fechaHoraFinReal;
    // this.estaFinalizada = !!this.reserva.fechaHoraFinReal;
    // this.estaPendiente = !this.reserva.fechaHoraComienzoReal && !!this.reserva.fechaHoraFin;
    // this.estaVacia = !(this.estaCorriendo || this.estaPendiente || this.estaFinalizada);
    // this.esReservaMenorUnaHora = !this.estaVacia && (this.minutos / 60) < 1;
    // this.esReservaMultiple = !this.estaVacia && (this.reserva.clientesReserva.length > 1);
  }

  estaCorriendo(): boolean {
    return this.reservaService.estaCorriendo(this.reserva);
  }

  estaFinalizada(): boolean {
    return this.reservaService.estaFinalizada(this.reserva);
  }

  estaCerrada(): boolean {
    return this.reservaService.estaCerrada(this.reserva);
  }

  estaPendiente(): boolean {
    return this.reservaService.estaPendiente(this.reserva);
  }

  estaVacia(): boolean {
    return this.reservaService.estaVacia(this.reserva);
  }

  esReservaMenorUnaHora(): boolean {
    return this.reservaService.esReservaMenorUnaHora(this.reserva);
  }

  esReservaMultiple(): boolean {
    return this.reservaService.esReservaMultiple(this.reserva);
  }

  calcularPasosCssReserva(): number {
    return (
      this.reservaService.calcularMinutosReserva(this.reserva) / this.stepTime
    );
  }

  // calcularMinutosEntreDateTime(date1, date2): number {
  //   if (!date1 || !date2) {
  //     return 0;
  //   }
  //   if (date1 > date2) {
  //     const temp = new Date(date2.getTime());
  //     date2 = date1;
  //     date1 = temp;
  //   }
  //   return (date2.getTime() - date1.getTime()) / (1000 * 60);
  // }
}
