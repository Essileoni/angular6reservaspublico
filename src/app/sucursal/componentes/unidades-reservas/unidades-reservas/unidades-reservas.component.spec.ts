import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidadesReservasComponent } from './unidades-reservas.component';

describe('UnidadesReservasComponent', () => {
  let component: UnidadesReservasComponent;
  let fixture: ComponentFixture<UnidadesReservasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnidadesReservasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnidadesReservasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
