import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { AuthService } from 'src/app/layout/auth/auth.service';
import { Sucursal } from '../../reservas/reserva-page/reserva.model';
import { ReservaService } from 'src/app/sucursal/services/reserva.service';
import { UnidadReserva } from 'src/app/sucursal/models/unidad-reserva.model';

@Component({
  selector: 'app-unidades-reservas',
  templateUrl: './unidades-reservas.component.html',
  styleUrls: ['./unidades-reservas.component.css'],
})
export class UnidadesReservasComponent implements OnInit {
  displayedColumns = ['descripcion', 'tipo', 'acciones'];
  unidadesReservas: UnidadReserva[] = [];
  appearance: string = environment.matFormFieldAppearance;
  sucursales: Sucursal[] = [];
  sucursalSeleccionada: Sucursal = null;
  private roles: string[] = [];
  private roleName: string = null;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private reservaService: ReservaService,
    private authService: AuthService
  ) {
    this.inicializarCascada();
  }

  ngOnInit() {}

  onClickUnidadReserva(unidadReserva: UnidadReserva) {
    this.router.navigate([
      '/' + this.sucursalSeleccionada.id + '/unidadreserva/' + unidadReserva.id,
    ]);
  }

  onChangeSucursal(sucursal: Sucursal) {
    this.sucursalSeleccionada = sucursal;
    this.obtenerUnidadesDeReserva();
  }

  // Privates
  private inicializarCascada() {
    this.obtenerRoleName();
  }

  private obtenerSucursalesYUnidadesReserva() {
    this.authService
      .obtenerSucursalesParaUnRol(this.roleName, true)
      .subscribe((sucursales: Sucursal[]) => {
        this.sucursales = sucursales;
        const idSucursalPredeterminada = this.authService.obtenerSucursalPredeterminadaId();
        if (sucursales.length === 0) {
          return;
        } else {
          const sucursalPredeterminada = sucursales.find(
            (s) => s.id.toString() === idSucursalPredeterminada.toString()
          );
          if (!!sucursalPredeterminada) {
            this.sucursalSeleccionada = sucursalPredeterminada;
          } else {
            this.sucursalSeleccionada = sucursales[0];
          }
        }

        this.obtenerUnidadesDeReserva();
      });
  }

  private obtenerRoleName() {
    this.activatedRoute.data.subscribe((data) => {
      this.roles = data.roles;
      this.roleName = this.roles[0];
      if (this.roleName) {
        this.obtenerSucursalesYUnidadesReserva();
      }
    });
  }

  private obtenerUnidadesDeReserva() {
    this.reservaService
      .obtenerUnidadesReserva(this.sucursalSeleccionada)
      .subscribe((unidadesReservas: UnidadReserva[]) => {
        this.unidadesReservas = unidadesReservas;
      });
  }
}
