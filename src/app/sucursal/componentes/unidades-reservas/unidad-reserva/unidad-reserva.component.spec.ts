import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidadReservaComponent } from './unidad-reserva.component';

describe('UnidadReservaComponent', () => {
  let component: UnidadReservaComponent;
  let fixture: ComponentFixture<UnidadReservaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnidadReservaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnidadReservaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
