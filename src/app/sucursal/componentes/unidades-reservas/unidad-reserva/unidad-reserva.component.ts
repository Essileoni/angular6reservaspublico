import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MessageBoxService } from 'src/app/layout/mensajes/messagebox.service';
import {
  MessageBox,
  MessageTipo,
  MessageFloat,
} from 'src/app/layout/mensajes/message-box.model';
import { Location } from '@angular/common';
import { Sucursal } from '../../reservas/reserva-page/reserva.model';
import { AuthService } from 'src/app/layout/auth/auth.service';
import * as cloneDeep from 'node_modules/lodash/cloneDeep';
import { RouterExtenderService } from 'src/app/layout/services/router-extender.service';
import { ReservaService } from 'src/app/sucursal/services/reserva.service';
import { UnidadReserva } from 'src/app/sucursal/models/unidad-reserva.model';
import { PrecioPorHora } from 'src/app/sucursal/models/precio-por-hora.model';

@Component({
  selector: 'app-unidad-reserva',
  templateUrl: './unidad-reserva.component.html',
  styleUrls: ['./unidad-reserva.component.css'],
})
export class UnidadReservaComponent implements OnInit {
  // Configuration
  appearance = environment.matFormFieldAppearance;
  unidadReservaTitle = environment.ReservaCrear.UnidadReservaTitle;
  tiposUnidadesReserva = cloneDeep(environment.ReservaCrear.TipoUnidadesReserva);
  private diasSemana = cloneDeep(environment.ReservaCrear.diasSemana);

  // Publicas
  sucursales: Sucursal[] = [];
  sucursalSeleccionada: Sucursal = null;

  unidadReserva: UnidadReserva = null;
  disableAgregarPrecioPorHora: boolean;
  paramsSubs: Subscription;
  sucursalSeleccionadaBloqueda: boolean = true;
  roles: string[] = [];
  roleName: string = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private routerExtenderService: RouterExtenderService,
    private _location: Location,
    private messageBoxService: MessageBoxService,
    private reservaService: ReservaService,
    private authService: AuthService
  ) {
    this.inicializarCascada();
  }

  ngOnInit() {
    this.unidadReserva = this.unidadReservaVacia();
  }

  convertirHoraString(hora: number): string {
    const horas = Math.trunc(hora || 0) + '';
    const minutos = Math.round(((hora || 0) - +horas) * 60) + '';
    return (
      '00'.substring(horas.length) +
      horas +
      ':' +
      '00'.substring(minutos.length) +
      minutos
    );
  }

  onClickAddPrecioPorHora() {
    if (!this.unidadReserva.preciosPorHora.find((pPH) => pPH.id === 0)) {
      const precioPorHoraNuevo = this.precioPorHoraVacio();
      precioPorHoraNuevo.id = null;
      this.unidadReserva.preciosPorHora.unshift(precioPorHoraNuevo);
      this.disableAgregarPrecioPorHora = true;
      setTimeout(() => {
        precioPorHoraNuevo.id = 0;
      }, 200);
    }
  }

  onEliminarPrecioPorHora(id: number) {
    if (!!id) {
      setTimeout(() => {
        this.unidadReserva.preciosPorHora = this.unidadReserva.preciosPorHora.filter(
          (pPH) => pPH.id !== id
        );
        this.disableAgregarPrecioPorHora = false;
      }, 200);
    }
  }

  onGuardarPrecioPorHora() {
    this.disableAgregarPrecioPorHora = false;
    this.ordenarPreciosPorHora();
  }

  onCrearPrecioPorHora(id: number) {
    this.disableAgregarPrecioPorHora = false;
    this.ordenarPreciosPorHora();
  }

  onClickGuardarUnidadReserva() {
    if (!this.validadUnidadReserva() || this.unidadReserva.id) {
      return;
    }
    this.reservaService
      .crearUnidadReserva(this.unidadReserva, this.sucursalSeleccionada)
      .subscribe((id: number) => {
        this.messageBoxService.show(<MessageBox>{
          Tipo: MessageTipo.Success,
          Mensaje: 'Se creó ' + this.unidadReserva.descripcion + ' con exito',
          Strong: 'Perfecto. ',
          Float: MessageFloat.StickyFloat,
          Segundos: 10,
        });
        this.router.navigate(['', this.sucursalSeleccionada.id.toString(), 'unidadreserva', id]);
      });
  }

  onClickModificarUnidadReserva() {
    if (!this.validadUnidadReserva() || !this.unidadReserva.id) {
      return;
    }
    this.reservaService
      .modificarUnidadReserva(this.unidadReserva, this.sucursalSeleccionada)
      .subscribe((unidadReserva: UnidadReserva) => {
        this.unidadReserva = unidadReserva;
        this.ordenarPreciosPorHora();
        this.reservaService
          .modificarUnidadReserva(this.unidadReserva, this.sucursalSeleccionada)
          .subscribe(() => {
            this.messageBoxService.show(<MessageBox>{
              Tipo: MessageTipo.Success,
              Mensaje:
                'Se modificó ' + this.unidadReserva.descripcion + ' con exito',
              Strong: 'Perfecto. ',
              Float: MessageFloat.StickyFloat,
              Segundos: 10,
            });
            this.routerExtenderService.navigateForced(
              ['', this.sucursalSeleccionada.id.toString(), 'unidadreserva', unidadReserva.id],
              'unidadesreservas'
            );
          });
      });
  }

  onGoBack() {
    this._location.back();
  }

  // PRIVATES

  private inicializarCascada() {
    this.obtenerRoleName();
  }

  private obtenerSucursalesYUnidadesReserva() {
    this.authService
      .obtenerSucursalesParaUnRol(this.roleName, true)
      .subscribe((sucursales: Sucursal[]) => {
        this.sucursales = sucursales;
        const idSucursalPredeterminada = this.authService.obtenerSucursalPredeterminadaId();
        const sucursalSeleccionadaId = +this.activatedRoute.snapshot.params[
          'idSucursal'
        ];

        if (sucursalSeleccionadaId === 0) {
          this.sucursalSeleccionadaBloqueda = false;
        }

        if (sucursales.length === 0) {
          return;
        } else if (sucursalSeleccionadaId === 0) {
          const sucursalPredeterminada = sucursales.find(
            (s) => s.id.toString() === idSucursalPredeterminada.toString()
          );
          if (!!sucursalPredeterminada) {
            this.sucursalSeleccionada = sucursalPredeterminada;
          } else {
            this.sucursalSeleccionada = sucursales[0];
          }
        } else {
          this.sucursalSeleccionada = sucursales.find(
            (s) => s.id.toString() === sucursalSeleccionadaId.toString()
          );
        }

        this.obtenerUnidadReserva();
        this.ordenarDiasSemana();
      });
  }

  private obtenerRoleName() {
    this.activatedRoute.data.subscribe((data) => {
      this.roles = data.roles;
      this.roleName = this.roles[0];
      if (this.roleName) {
        this.obtenerSucursalesYUnidadesReserva();
      }
    });
  }

  // private inicializarCascada() {
  //   const sucursalSeleccionadaId = +this.activatedRoute.snapshot.params[
  //     'idSucursal'
  //   ];
  //   this.authService
  //     .obtenerSucursalesAdministrables()
  //     .subscribe((sucursales: Sucursal[]) => {
  //       this.sucursales = sucursales;

  //       if (sucursalSeleccionadaId === 0) {
  //         this.sucursalSeleccionadaBloqueda = false;
  //       }
  //       this.sucursalSeleccionada = sucursales.find(
  //         (s) => s.id.toString() === sucursalSeleccionadaId.toString()
  //       );
  //       this.obtenerUnidadReserva();
  //       this.ordenarDiasSemana();
  //     });
  // }

  private validadUnidadReserva(): boolean {
    return (
      !!this.sucursalSeleccionada &&
      this.unidadReserva.id >= 0 &&
      !!this.unidadReserva.descripcion &&
      !!this.unidadReserva.descripcion.trim() &&
      !!this.unidadReserva.tipo
    );
  }

  private unidadReservaVacia(): UnidadReserva {
    return <UnidadReserva>{
      id: 0,
      activo: true,
      descripcion: '',
      preciosPorHora: <PrecioPorHora[]>[],
      tipo: '',
    };
  }

  private precioPorHoraVacio(): PrecioPorHora {
    return <PrecioPorHora>{
      id: 0,
      diaSemana: null,
      horaFin: null,
      horaInicio: null,
      horaFinAdaptada: null,
      horaInicioAdaptada: null,
      precioUnitarioPorHora: null,
      unidadDeReservaId: this.unidadReserva.id,
    };
  }

  private obtenerUnidadReserva() {
    if (this.router.url.indexOf('/unidadreserva/') !== -1) {
      this.paramsSubs = this.activatedRoute.params.subscribe((params) => {
        if (params.id) {
          this.reservaService
            .obtenerUnidadReserva(<number>params.id, this.sucursalSeleccionada)
            .subscribe((unidadReserva) => {
              this.unidadReserva = unidadReserva;
              this.ordenarPreciosPorHora();
            });
        }
        // tslint:disable-next-line: no-unused-expression
        this.paramsSubs && this.paramsSubs.unsubscribe();
      });
    }
  }

  private ordenarDiasSemana() {
    this.diasSemana.push(this.diasSemana.shift());
  }

  private ordenarPreciosPorHora() {
    let preciosPorHoraOrdenado: PrecioPorHora[] = <PrecioPorHora[]>[];
    this.unidadReserva.preciosPorHora = this.unidadReserva.preciosPorHora;

    // Primero ordenar por día
    for (const dia of this.diasSemana) {
      preciosPorHoraOrdenado = preciosPorHoraOrdenado.concat(
        this.unidadReserva.preciosPorHora
          .filter((pPH) => pPH.diaSemana === dia)
          // Luego ordenar por hora de inicio
          .sort((a, b) => {
            return (
              (a.horaInicioAdaptada < b.horaInicioAdaptada && -1) ||
              (a.horaInicioAdaptada > b.horaInicioAdaptada && 1) ||
              0
            );
          })
      );
    }
    this.unidadReserva.preciosPorHora = preciosPorHoraOrdenado;
  }
}
