import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecioPorHoraComponent } from './precio-por-hora.component';

describe('PrecioPorHoraComponent', () => {
  let component: PrecioPorHoraComponent;
  let fixture: ComponentFixture<PrecioPorHoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrecioPorHoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecioPorHoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
