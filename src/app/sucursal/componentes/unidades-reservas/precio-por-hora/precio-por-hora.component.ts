import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { NgxMaterialTimepickerTheme } from 'ngx-material-timepicker';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import {
  MessageTipo,
  MessageBox,
  MessageFloat,
} from 'src/app/layout/mensajes/message-box.model';
import { MessageBoxService } from 'src/app/layout/mensajes/messagebox.service';
import * as cloneDeep from 'node_modules/lodash/cloneDeep';
import { ReservaService } from 'src/app/sucursal/services/reserva.service';
import { PrecioPorHora } from 'src/app/sucursal/models/precio-por-hora.model';

@Component({
  selector: 'app-precio-por-hora',
  templateUrl: './precio-por-hora.component.html',
  styleUrls: ['./precio-por-hora.component.css'],
})
export class PrecioPorHoraComponent implements OnInit {
  @Input() precioPorHora: PrecioPorHora;
  @Output() eliminarPrecioPorHoraEventEmitter: EventEmitter<
    number
  > = new EventEmitter();
  @Output() guardarPrecioPorHoraEventEmitter: EventEmitter<
    void
  > = new EventEmitter();
  @Output() crearPrecioPorHoraEventEmitter: EventEmitter<
    number
  > = new EventEmitter();

  // Configuration
  primary: NgxMaterialTimepickerTheme = {
    container: {
      buttonColor: '#3f51b5',
    },
    dial: {
      dialBackgroundColor: '#3f51b5',
    },
    clockFace: {
      clockHandColor: '#3f51b5',
    },
  };
  appearance = environment.matFormFieldAppearance;
  diasSemana: string[] = cloneDeep(environment.ReservaCrear.diasSemana).sort((a, b) => {
    if (b.toLowerCase() === 'domingo') {
      return -1;
    }
    return 1;
  });

  // VARIABLES PUBLICAS

  get horaInicioString(): string {
    if (!this.precioPorHora.horaInicio && this.precioPorHora.horaInicio !== 0) {
      return <any>this.precioPorHora.horaInicio;
    }
    const horas = Math.trunc(this.precioPorHora.horaInicio || 0) + '';
    const minutos =
      Math.round(((this.precioPorHora.horaInicio || 0) - +horas) * 60) + '';
    return (
      '00'.substring(horas.length) +
      horas +
      ':' +
      '00'.substring(minutos.length) +
      minutos
    );
  }
  set horaInicioString(t: string) {
    if (!t && typeof t !== 'string') {
      this.precioPorHora.horaInicio = t;
      return;
    }
    this.precioPorHora.horaInicio = +t.split(':')[0] + +t.split(':')[1] / 60;
    this.actualizarHorasAdaptadas();
  }

  get horaFinalString(): string {
    if (!this.precioPorHora.horaFin && this.precioPorHora.horaFin !== 0) {
      return <any>this.precioPorHora.horaFin;
    }
    const horas = Math.trunc(this.precioPorHora.horaFin || 0) + '';
    const minutos =
      Math.round(((this.precioPorHora.horaFin || 0) - +horas) * 60) + '';
    return (
      '00'.substring(horas.length) +
      horas +
      ':' +
      '00'.substring(minutos.length) +
      minutos
    );
  }
  set horaFinalString(t: string) {
    if (!t && typeof t !== 'string') {
      this.precioPorHora.horaFin = t;
      return;
    }
    this.precioPorHora.horaFin = +t.split(':')[0] + +t.split(':')[1] / 60;
    this.actualizarHorasAdaptadas();
  }

  esHoraInicioConUnDiaMas = false;
  esHoraFinalConUnDiaMas = false;

  // Privadas

  private sucursalId: number = null;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private messageBoxService: MessageBoxService,
    private reservaServices: ReservaService
  ) {
    // override the route reuse strategy
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    this.sucursalId = +this.activatedRoute.snapshot.params['idSucursal'];
    this.esHoraInicioConUnDiaMas = this.precioPorHora.horaInicioAdaptada >= 24;
    this.esHoraFinalConUnDiaMas = this.precioPorHora.horaFinAdaptada >= 24;
  }

  onKeyDownPrecioUnitarioPorHora(event: KeyboardEvent) {
    const noAdmitidos = ['E', 'e', '.', ',', '+', '-', 'ArrowDown', 'ArrowUp'];
    if (noAdmitidos.indexOf(event.key) !== -1) {
      return false;
    }
  }

  onClickEliminar() {
    this.reservaServices
      .eliminarPrecioPorHora(this.precioPorHora, this.sucursalId)
      .subscribe(() => {
        this.messageBoxService.show(<MessageBox>{
          Tipo: MessageTipo.Success,
          Mensaje: 'Se eliminó el precio por hora',
          Strong: 'Perfecto. ',
          Float: MessageFloat.StickyFloat,
          Segundos: 10,
        });
        this.eliminarPrecioPorHoraEventEmitter.emit(this.precioPorHora.id);
      });
  }

  onClickGuardar() {
    if (!this.validarPrecioPorHora()) {
      return;
    }
    if (this.precioPorHora.id) {
      this.modificarPrecioPorHora();
    } else {
      this.crearPrecioPorHora();
    }
  }

  actualizarHorasAdaptadas() {
    if (this.esHoraInicioConUnDiaMas) {
      this.precioPorHora.horaInicioAdaptada =
        this.precioPorHora.horaInicio + 24;
    } else {
      this.precioPorHora.horaInicioAdaptada = this.precioPorHora.horaInicio;
    }
    if (this.esHoraFinalConUnDiaMas) {
      this.precioPorHora.horaFinAdaptada = this.precioPorHora.horaFin + 24;
    } else {
      this.precioPorHora.horaFinAdaptada = this.precioPorHora.horaFin;
    }
  }

  log(obj: any) {
    console.log(obj);
  }

  // Metodos Privados
  private recargarComponente() {
    this.router.navigated = false;
    this.router.navigate([this.router.url]);
  }

  private validarPrecioPorHora(): boolean {
    if (!this.precioPorHora.precioUnitarioPorHora) {
      this.precioPorHora.precioUnitarioPorHora = null;
    }

    return (
      this.precioPorHora.id >= 0 &&
      this.precioPorHora.horaFin > 0 &&
      this.precioPorHora.horaInicio > 0 &&
      this.precioPorHora.precioUnitarioPorHora > 0 &&
      this.precioPorHora.diaSemana &&
      this.precioPorHora.diaSemana.trim() &&
      true
    );
  }

  private modificarPrecioPorHora() {
    this.reservaServices
      .modificarPrecioPorHora(this.precioPorHora, this.sucursalId)
      .subscribe((precioPorHora: PrecioPorHora) => {
        this.precioPorHora = precioPorHora;
        this.messageBoxService.show(<MessageBox>{
          Tipo: MessageTipo.Success,
          Mensaje: 'Se modificó el precio por hora',
          Strong: 'Perfecto. ',
          Float: MessageFloat.StickyFloat,
          Segundos: 10,
        });
        this.guardarPrecioPorHoraEventEmitter.emit();
      });
  }

  private crearPrecioPorHora() {
    this.reservaServices
      .crearPrecioPorHora(this.precioPorHora, this.sucursalId)
      .subscribe((id: number) => {
        this.precioPorHora.id = id;
        this.messageBoxService.show(<MessageBox>{
          Tipo: MessageTipo.Success,
          Mensaje: 'Se creó con exito el precio por hora',
          Strong: 'Perfecto. ',
          Float: MessageFloat.StickyFloat,
          Segundos: 10,
        });
        this.crearPrecioPorHoraEventEmitter.emit(id);
      });
  }
}
