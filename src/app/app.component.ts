import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Title } from '@angular/platform-browser';
import { MonitorReservasService } from './sucursal/services/monitor-reservas.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = environment.title;
  public constructor(
    private titleService: Title,
    private monitorReservasService: MonitorReservasService
  ) {
    this.titleService.setTitle(environment.title);
  }
}
