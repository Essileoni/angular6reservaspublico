import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Cliente } from 'src/app/empresa/clientes/clientes.model';
import { environment } from 'src/environments/environment';
import { ClientesService } from 'src/app/empresa/clientes/clientes.service';
import { ClienteReserva } from 'src/app/sucursal/models/cliente-reserva.model';

@Component({
  selector: 'app-buscar-cliente',
  templateUrl: './buscar-cliente.component.html',
  styleUrls: ['./buscar-cliente.component.css'],
})
export class BuscarClienteComponent implements OnInit {
  // Two-Way-Binding
  private _clienteReserva: ClienteReserva;
  @Input() get clienteReserva() {
    return this._clienteReserva;
  }
  @Output() clienteReservaModelChange: EventEmitter<
    ClienteReserva
  > = new EventEmitter();
  set clienteReserva(cR: ClienteReserva) {
    this._clienteReserva = cR;
    this.clienteReservaModelChange.emit(this._clienteReserva);
  }
  // END-Two-Way-Binding

  @Output() editarClienteClick: EventEmitter<Cliente> = new EventEmitter();
  @Output() clienteModelChange: EventEmitter<Cliente> = new EventEmitter();
  @Output() crearClienteClick: EventEmitter<void> = new EventEmitter();

  @Input() semaforoOn = true;
  @Input() seniaOn = true;
  @Input() saldoOn = true;
  @Input() onlyFind = false;
  @Input() disabled = false;

  @Input() importeUnidadReservaParte: number;
  @Input() clientesAFiltrar: Cliente[];

  private esperar = false;

  // MODELO HTML

  _inputSeniaHtml: number;
  get inputSeniaHtml(): number {
    if (this._inputSeniaHtml === null) {
      return this._inputSeniaHtml;
    } else {
      return this.clienteReserva.senia;
    }
    // return this.clienteReserva ? (this.clienteReserva.senia ? this.clienteReserva.senia : 0) : 0;
  }
  set inputSeniaHtml(s: number) {
    if (s === null || s === undefined) {
      this._inputSeniaHtml = null;
      if (this.clienteReserva) {
        this.clienteReserva.senia = 0;
      }
    } else {
      this._inputSeniaHtml = s;
      if (this.clienteReserva) {
        this.clienteReserva.senia = s;
      }
    }
  }

  clienteHtml: Cliente = null;
  clientesBusquedaHtml: Cliente[] = [];

  // CONFIGURACION

  autocompleteClienteOn: boolean =
    environment.ReservaCrear.autocompleteClienteOn;
  appearance = environment.matFormFieldAppearance;

  constructor(private clientesService: ClientesService) {}

  ngOnInit() {
    // if (!this.clienteReserva) {
    //   this.clienteReserva = <ClienteReserva>{};
    //   this.resetModel();
    //   this.resetHtml();
    // }

    // if (!this.clienteReserva.cliente) {
    //   this.resetModel();
    //   this.resetHtml();
    // } else {
    //   this.cargarCliente(this.clienteReserva);
    // }

    if (this.clienteReserva && !this.clienteReserva.clienteId) {
      this.resetModel();
      this.resetHtml();
    } else {
      this.cargarCliente(this.clienteReserva);
    }
  }

  cargarCliente(clienteReserva: ClienteReserva) {
    if (this.clienteReserva) {
      this.clienteHtml = clienteReserva.cliente;
    }
  }

  private resetHtml() {
    this.inputSeniaHtml = null;
    this.clienteHtml = null;
  }

  private resetModel() {
    if (this.clienteReserva) {
      this.clienteReserva.cliente = null;
      this.clienteReserva.clienteId = null;
      this.clienteReserva.senia = 0;
      this.clienteReserva.deuda = 0;
      this.clienteReserva.esReservaFija = false;
      this.clienteReserva.estadoCumplimiento = null;
      this.clienteReserva.importeUnidadReservaParte = 0;
      this.clienteReserva.consumos = null;
      this.clienteReservaModelChange.emit(this.clienteReserva);
    }
  }

  private setClienteField(cliente: Cliente) {
    this.clienteHtml = cliente;
    this.clienteModelChange.emit(cliente);
    if (this.clienteReserva) {
      this.clienteReserva.cliente = this.clienteHtml;
      this.clienteReserva.clienteId = this.clienteHtml.id;
      this.clienteReserva.esReservaFija = false; // TODO: hacer un servicio para buscar si es reserva fija
      this.clienteReserva.estadoCumplimiento = null;
      this.clienteReserva.consumos = null;
      this.clienteReserva.usarSaldo = true;
      this.clienteReservaModelChange.emit(this.clienteReserva);
    }
  }

  buscarClientes(filtro: string) {
    this.resetHtml();
    this.resetModel();

    if (!this.esperar && filtro.length >= 3) {
      this.esperar = true;
      setTimeout(() => {
        this.clientesService
          .buscarCliente(filtro)
          .subscribe((clientes: Cliente[]) => {
            this.clientesBusquedaHtml = clientes.filter(
              (c: Cliente) =>
                !this.clientesAFiltrar.find((caf) => caf.id === c.id)
            );
          });
        this.esperar = false;
      }, 300);
    } else {
      this.clientesBusquedaHtml = [];
    }
  }

  log(obj: any) {
    console.log(obj);
  }

  displayInputSelect(cliente: Cliente): string {
    if (!cliente) {
      return '';
    }
    return cliente.nombreYApellido;
  }

  // LISTENERS
  onClickVolverABuscar() {
    this.clienteHtml = null;
    this.clienteModelChange.emit(null);
  }

  onSeniaKeyDown(event: any): boolean {
    return (
      event.key !== '-' &&
      event.key !== '+' &&
      event.key !== '.' &&
      event.key.toLowerCase() !== 'e'
    );
  }

  onClienteOptionSelected(event: any) {
    this.setClienteField(<Cliente>event.option.value);
  }

  onSeniaChange(senia: string) {
    if (this.clienteReserva) {
      this.clienteReserva.senia = +senia;
      this.clienteReservaModelChange.emit(this.clienteReserva);
    }
  }

  onClickEditCliente() {
    this.editarClienteClick.emit(this.clienteHtml);
  }

  onClickCrearCliente() {
    this.crearClienteClick.emit();
  }
}
