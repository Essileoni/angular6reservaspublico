export class Cliente {
  id: number;
  documento: string;
  nombreYApellido: string;
  email: string;
  telefonoArea: string;
  telefonoNumero: string;
  esTelefonoFijo: boolean;
  tipoDocumento: string;
  infraccionesGraves: number;
  infracciones: number;
  cumplimientos: number;
  saldo: number; // Saldo a favor o en contrar.
  eliminado = false; // [Propiedad solo existente en front end]
}
