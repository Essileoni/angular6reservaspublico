import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Cliente } from '../clientes.model';
import { environment } from 'src/environments/environment';
import { ClientesService } from '../clientes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageBoxService } from 'src/app/layout/mensajes/messagebox.service';
import {
  MessageBox,
  MessageTipo,
  MessageFloat,
} from 'src/app/layout/mensajes/message-box.model';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { RouterExtenderService } from 'src/app/layout/services/router-extender.service';

@Component({
  selector: 'app-cliente-page',
  templateUrl: './cliente-page.component.html',
  styleUrls: ['./cliente-page.component.css'],
})
export class ClientePageComponent implements OnInit, OnDestroy {
  @Input() cliente: Cliente;

  clientesAFiltrarR: Cliente[] = [];
  recordatorioTelefono = null;
  private caracteresNumericos = '0123456789';

  // Configuracion
  appearance = environment.matFormFieldAppearance;
  private telefonoArea = environment.clientes.telefonoArea;
  paramsSubs: Subscription;

  patternDe6a8digitos = environment.expresionesRegulares.regexDe6a8digitos
    .toString()
    .split('/')
    .join('');
  patternDe2a4digitos = environment.expresionesRegulares.regexDe2a4digitos
    .toString()
    .split('/')
    .join('');

  // Privates
  private clienteId: number;

  constructor(
    private clientesService: ClientesService,
    private activatedRoute: ActivatedRoute,
    private messageBoxService: MessageBoxService,
    private router: Router,

    private routerExtenderService: RouterExtenderService
  ) {
    this.obtenerCliente();
  }

  ngOnInit() {
    this.setCliente(this.cliente);
  }

  ngOnDestroy() {
    // tslint:disable-next-line: no-unused-expression
    // this.paramsSubs && this.paramsSubs.unsubscribe();
  }

  // LISTENERS
  onSubmit(form: NgForm) {
    if (form.invalid) {
      return false;
    }

    if (this.cliente.id) {
      this.modificarCliente(form);
    } else {
      this.crearCliente(form);
    }
  }

  onClienteModelChange(cliente: Cliente) {
    this.setCliente(cliente);
  }

  onClickBorrarCliente(form: NgForm) {
    this.eliminarCliente(form);
  }

  onClickVolver() {
    this.volver();
  }

  onKeyDownTelefonoArea(event: KeyboardEvent) {
    const noAdmitidos = '+-eE,.';
    const area = (this.cliente.telefonoArea || '') + '';
    if (noAdmitidos.indexOf(event.key) > -1) {
      event.preventDefault();
      return false;
    } else {
      this.recordatorioTelefono =
        this.cliente.telefonoNumero || this.recordatorioTelefono;
      this.cliente.telefonoNumero = null;
    }
    if (this.caracteresNumericos.indexOf(event.key) > -1 && area.length >= 4) {
      event.preventDefault();
      return false;
    }
  }

  onKeyDownTelefonoNumero(event: KeyboardEvent) {
    const noAdmitidos = '+-eE,.';
    const area = (this.cliente.telefonoArea || '') + '';
    const areaLength = area.length <= 2 ? 2 : area.length;
    const numero = (this.cliente.telefonoNumero || '') + '';
    if (noAdmitidos.indexOf(event.key) > -1) {
      event.preventDefault();
      return false;
    }
    if (
      this.caracteresNumericos.indexOf(event.key) > -1 &&
      numero.length >= 10 - areaLength
    ) {
      event.preventDefault();
      return false;
    }
  }

  onKeyDownSaldo(event: any) {
    const noAdmitidos = '+eE,.';
    const saldo = (this.cliente.saldo || '') + '';

    if (noAdmitidos.indexOf(event.key) > -1) {
      event.preventDefault();
      return false;
    }

    if (event.key === '-') {
      if (this.cliente.saldo > 0) {
        this.cliente.saldo = -1 * this.cliente.saldo;
      }
      if (saldo.length === 0) {
        return true;
      }
      event.preventDefault();
      return false;
    }
  }

  onKeyUpSaldo(event: any) {
    const saldo = (this.cliente.saldo || '') + '';
    if (
      this.caracteresNumericos.indexOf(event.key) > -1 &&
      saldo.length === 0
    ) {
      this.cliente.saldo = -1 * +event.key;
    }
  }

  onKeyDownNombreYApellido(event) {
    const regexCaracteres = /[A-Za-zÀÈÌÒÙàèìòùÁÉÍÓÚñáéíóúüÜÑçÇ \']/;
    if (!regexCaracteres.test(event.key)) {
      event.preventDefault();
      return false;
    }
  }

  onKeyDownDocumento(event) {
    const noAdmitidos = '+-eE,.';
    const documento = (this.cliente.documento || '') + '';
    if (noAdmitidos.indexOf(event.key) > -1) {
      event.preventDefault();
      return false;
    }
    if (
      this.caracteresNumericos.indexOf(event.key) > -1 &&
      documento.length >= 8
    ) {
      event.preventDefault();
      return false;
    }
  }

  // FUNCTIONS



  desactivarBuscarCliente() {
    return !!this.cliente.id;
  }

  mostrarBuscarCliente() {
    return this.clienteId === 0;
  }

  getMaxTelefono() {
    const longitudNumero = ((this.cliente.telefonoNumero || '') + '').length;

    let longitudArea = ((this.cliente.telefonoArea || '') + '').length;
    longitudArea = longitudArea <= 2 ? 2 : longitudArea;

    return longitudNumero + '/' + (10 - longitudArea);
  }

  // PRIVATES

  private setCliente(cliente: Cliente){
    this.cliente = cliente || this.clienteVacio();
  }

  private clienteVacio(): Cliente {
    return <Cliente>{
      id: 0,
      documento: '',
      nombreYApellido: '',
      email: '',
      telefonoNumero: '',
      telefonoArea: this.telefonoArea,
      esTelefonoFijo: false,
      tipoDocumento: '',
      infracciones: 0,
      infraccionesGraves: 0,
      cumplimientos: 0,
      saldo: 0,
      eliminado: false,
    };
  }

  private crearCliente(form: NgForm) {
    this.clientesService.crearCliente(this.cliente).subscribe((id: number) => {
      this.messageBoxService.show(<MessageBox>{
        Tipo: MessageTipo.Success,
        Mensaje: 'Se guardo el cliente número ' + id,
        Strong: 'Bien. ',
        Float: MessageFloat.StickyFloat,
        Segundos: 10,
      });
      this.cliente.id = id;
      this.routerExtenderService.clienteCreadoDeClienteAReserva = this.cliente;
      this.volver();
    });
  }

  private modificarCliente(form: NgForm) {
    this.clientesService
      .modificarCliente(this.cliente)
      .subscribe((cliente: Cliente) => {
        this.cliente = cliente;
        this.messageBoxService.show(<MessageBox>{
          Tipo: MessageTipo.Success,
          Mensaje: 'Se modificó el cliente número ' + cliente.id,
          Strong: 'Perfecto. ',
          Float: MessageFloat.StickyFloat,
          Segundos: 10,
        });
        this.volver();
      });
  }

  private eliminarCliente(form: NgForm) {
    this.clientesService.eliminarCliente(this.cliente.id).subscribe(() => {
      this.cliente.eliminado = true; // <- Se setea una variable en eliminado.
      this.messageBoxService.show(<MessageBox>{
        Tipo: MessageTipo.Success,
        Mensaje: 'Se eliminó el cliente número ' + this.cliente.id,
        Strong: 'Listo. ',
        Float: MessageFloat.StickyFloat,
        Segundos: 10,
      });
      this.volver();
    });
  }

  private obtenerCliente() {
    if (
      this.router.url.indexOf('/cliente/') !== -1 &&
      this.routerExtenderService.clienteDeReservaACliente
    ) {
      this.cliente = this.routerExtenderService.clienteDeReservaACliente;
      this.routerExtenderService.clienteDeReservaACliente = undefined;
    } else if (this.router.url.indexOf('/cliente/') !== -1) {
      this.clienteId = +this.activatedRoute.snapshot.params['id'];
      // this.paramsSubs = this.activatedRoute.params.subscribe(params => {
      // if (params.id) {
      if (this.clienteId) {
        // this.clientesService.getCliente(<number>params.id)
        this.clientesService
          .getCliente(this.clienteId)
          .subscribe((cliente) => (this.cliente = cliente));
      }
      // tslint:disable-next-line: no-unused-expression
      // this.paramsSubs && this.paramsSubs.unsubscribe();
      // });
    }
  }

  private volver() {
    this.routerExtenderService.volver();
  }
}
