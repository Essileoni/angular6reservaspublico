import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, of, forkJoin } from 'rxjs';
import { Cliente } from './clientes.model';
import { ClienteReserva } from 'src/app/sucursal/models/cliente-reserva.model';


const urlBuscarCliente =
  environment.empresaController.urlBase +
  environment.empresaController.pathBuscarClientes;

const urlGetCliente =
  environment.empresaController.urlBase +
  environment.empresaController.pathGetCliente;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const urlDeleteCliente =
  environment.empresaController.urlBase +
  environment.empresaController.pathDeleteCliente;

const urlEditCliente =
  environment.empresaController.urlBase +
  environment.empresaController.pathEditCliente;

const urlCreateCliente =
  environment.empresaController.urlBase +
  environment.empresaController.pathCreateCliente;

const urlClienteActualizarSemaforo =
  environment.empresaController.urlBase +
  environment.empresaController.pathClienteActualizarSemaforo;

const urlClienteActualizarSaldoCliente =
  environment.empresaController.urlBase +
  environment.empresaController.pathClienteActualizarSaldoCliente;

@Injectable({
  providedIn: 'root'
})
export class ClientesService {


  constructor(private httpClient: HttpClient) { }

  buscarCliente(datoCliente: string): Observable<Cliente[]> {
    if (datoCliente.trim().length === 0) {
      return new Observable<Cliente[]>();
    }
    return this.httpClient.get<Cliente[]>(urlBuscarCliente + '/' + datoCliente);
  }

  getCliente(id: number) {
    if (id) {
      return this.httpClient.get<Cliente>(urlGetCliente + '/' + id);
    }
  }

  eliminarCliente(id: number): Observable<string> {
    return this.httpClient.delete<string>(urlDeleteCliente + '/' + id, httpOptions);
  }

  modificarCliente(cliente: Cliente): Observable<Cliente> {
    return this.httpClient.patch<Cliente>(urlEditCliente + '/' + cliente.id, cliente, httpOptions);
  }

  crearCliente(cliente: Cliente): Observable<number> {
    return this.httpClient.post<number>(urlCreateCliente, cliente, httpOptions);
  }

  aplicarSemaforo(clientesReserva: ClienteReserva[]): Observable<string[]> {
    const observables: Observable<string>[] = [];
    clientesReserva.forEach((cRE: ClienteReserva) => {
      observables.push(
        this.httpClient.patch<string>(urlClienteActualizarSemaforo + '/' + cRE.cliente.id + '/' + cRE.estadoCumplimiento, null, httpOptions)
      );
    });
    return forkJoin(observables);
  }

  actualizarSaldoCliente(cliente: Cliente): Observable<string> {
    return this.httpClient.patch<string>(urlClienteActualizarSaldoCliente + '/' + cliente.id + '/' + cliente.saldo, null, httpOptions);
  }
}
