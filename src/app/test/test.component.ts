import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  color = null;

  objetos = [{ msj: '' }, { msj: 'hola2' }];

  verificarCampos = false;

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: [{ value: '', disabled: true }, Validators.required],
      subCtrl: [null, Validators.required],
      dateCtrl: [null, Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    for (let control in this.firstFormGroup.controls) {
      if (this.firstFormGroup.controls[control]) {
        this.firstFormGroup.controls[control].markAsDirty();
        this.firstFormGroup.controls[control].markAsTouched();
      }
    }

    this.firstFormGroup.valueChanges.subscribe(model => {
      console.log(model);
      // Solo activar y desactivar controlers.
      const dateCtrl = this.firstFormGroup.controls['dateCtrl'];
      this.desactivarControl('firstCtrl', dateCtrl.disabled);
    })
  }

  checkDate() {
    const dateCtrl = this.firstFormGroup.controls['dateCtrl'];
    if (dateCtrl.invalid && dateCtrl.enabled) {
      dateCtrl.markAsTouched();
      dateCtrl.markAsDirty();
      this.color = 'red';
    } else {
      this.color = null;
    }
  }

  log(obj: any) {
    console.log(obj);
  }

  private desactivarControl(nombreCtrl: string, disableCondition: boolean) {

    const ctrl = this.firstFormGroup.controls[nombreCtrl];
    if (!ctrl) {
      console.error('No existe el control ' + nombreCtrl);
      return;
    }

    const esDesactivable = disableCondition && ctrl.enabled; // !(disabled && !ctrl.disable) === !disable || ctrl.disable
    const esActivable = !disableCondition && ctrl.disabled;

    if (esDesactivable) {
      ctrl.disable();
    }

    if (esActivable) {
      ctrl.enable();
    }
  }

  onSubmit() {
    console.log('submit');
  }

}
