import { FormGroup, ValidatorFn, AbstractControl } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-subtest',
  templateUrl: './subtest.component.html',
  styleUrls: ['./subtest.component.css']
})
export class SubtestComponent implements OnInit {
  // @Input() subTestFG: FormGroup;

  _inicio = true;
  @Input() get verificarCampos() {
    return;
  }
  set verificarCampos(v) {
    if (this._inicio) {
      this._inicio = false;
      return;
    }
    document.getElementById('submit').click();
    console.log('click externo');
  }

  _value = null;
  @Input() get value() {
    return this._value;
  }
  @Output() valueChange = new EventEmitter();
  set value(v) {
    this._value = v;
    this.valueChange.emit(this._value);

  }

  constructor() { }

  ngOnInit() {
    // console.log(this.subTestFG);
  }
  log(obj){

    console.log(obj);
  }

}

