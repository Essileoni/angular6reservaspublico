import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MenuComponent } from './layout/menu/menu.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './routing/app-routing.module';
import { LoginComponent } from './layout/auth/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoadingComponent } from './layout/loading/loading.component';
import { LoadingInterceptor } from './layout/loading/loading.interceptor';
import { AuthInterceptor } from './layout/auth/auth.interceptor';
import { MessageBoxComponent } from './layout/mensajes/message-box.component';
import { ErrorInterceptor } from './layout/mensajes/error.interceptor';
import { RolesPageComponent } from './layout/user-admin/roles-page/roles-page.component';
import { UserPageComponent } from './layout/user-admin/user-page/user-page.component';
import { GroupPageComponent } from './layout/user-admin/group-page/group-page.component';

import { BuscarClienteComponent } from './empresa/clientes/buscar-cliente/buscar-cliente.component';
import { DatepickerSinInputComponent } from './layout/material/datepicker-sin-input/datepicker-sin-input.component';
import { MatSliderCustomComponent } from './sucursal/componentes/mat-slider-custom/mat-slider-custom.component';
import { MatSliderCustomDuracionComponent } from './sucursal/componentes/mat-slider-custom-duracion/mat-slider-custom-duracion.component';
import { InputDineroComponent } from './sucursal/componentes/input-dinero/input-dinero.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TestComponent } from './test/test.component';
import { SubtestComponent } from './test/subtest/subtest.component';
import { DateInterceptor } from './layout/date/date.interceptor';
import { ConfirmDialogComponent } from './layout/dialog/confirm-dialog/confirm-dialog.component';
import { ClientePageComponent } from './empresa/clientes/cliente-page/cliente-page.component';

import { registerLocaleData } from '@angular/common';
import localeEsAr from '@angular/common/locales/es-AR';
import localeEsArExtra from '@angular/common/locales/extra/es-AR';
import { AppAuthRoutingModule } from './routing/app-auth-routing.module';
import { ReservaPageComponent } from './sucursal/componentes/reservas/reserva-page/reserva-page.component';
import { GrillaComponent } from './sucursal/componentes/reservas/grilla/grilla.component';
import { GrillaReservaComponent } from './sucursal/componentes/reservas/grilla/grilla-reserva/grilla-reserva.component';
import { UnidadReservaComponent } from './sucursal/componentes/unidades-reservas/unidad-reserva/unidad-reserva.component';
import { BorrarReservaDialogComponent } from './sucursal/componentes/reservas/dialog/borrar-reserva-dialog/borrar-reserva-dialog.component';
import { PrecioPorHoraComponent } from './sucursal/componentes/unidades-reservas/precio-por-hora/precio-por-hora.component';
import { ReservaComenzadaPageComponent } from './sucursal/componentes/reservas/reserva-comenzada-page/reserva-comenzada-page.component';
import { UnidadesReservasComponent } from './sucursal/componentes/unidades-reservas/unidades-reservas/unidades-reservas.component';
import { ClientePagaReservaDialogComponent } from './sucursal/componentes/reservas/dialog/cliente-paga-reserva-dialog/cliente-paga-reserva-dialog.component';
import { ListadoSucursalesReservasDialogComponent } from './sucursal/componentes/reservas/dialog/listado-sucursales-reservas-dialog/listado-sucursales-reservas-dialog.component';

registerLocaleData(localeEsAr, 'es-AR', localeEsArExtra);

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    LoginComponent,
    LoadingComponent,
    MessageBoxComponent,
    RolesPageComponent,
    UserPageComponent,
    GroupPageComponent,
    ReservaPageComponent,
    BuscarClienteComponent,
    DatepickerSinInputComponent,
    MatSliderCustomComponent,
    MatSliderCustomDuracionComponent,
    InputDineroComponent,
    GrillaComponent,
    GrillaReservaComponent,
    TestComponent,
    SubtestComponent,
    ConfirmDialogComponent,
    ClientePageComponent,
    UnidadReservaComponent,
    BorrarReservaDialogComponent,
    PrecioPorHoraComponent,
    ReservaComenzadaPageComponent,
    UnidadesReservasComponent,
    ClientePagaReservaDialogComponent,
    BuscarClienteComponent,
    ListadoSucursalesReservasDialogComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    // AppAuthRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    PerfectScrollbarModule,
    HttpClientModule,
    FontAwesomeModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-Ar' }, // Configurar todos los pipes y datepickers en español argentina.
    Title,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: DateInterceptor, multi: true },
  ],
  entryComponents: [
    BorrarReservaDialogComponent,
    ClientePagaReservaDialogComponent,
    ListadoSucursalesReservasDialogComponent
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
