export const environment = {
  development: false,
  testing: true,
  production: false,

  rutasExcluidas: ['/api/reservas/Empresa/BuscarCliente', '/api/reservas/Sucursal/{*}/GetReservasParaUnaFechaNotLoading', '/api/auth/Account/GetSucursalesParaUnRolNotLoading'],
  messageBoxTimeOn: true,
  messageBoxHttpErrorActivo: true,
  title: 'Reservas',
  loadingOn: false,
  matFormFieldAppearance: 'outline',
  // 'outline' | 'fill' | 'standard' | 'legacy'  https://material.angular.io/components/form-field/overview
  toolbar: {
    title: 'Reservas',
    materialicon: 'today',
    anothericonurl: '',
  },
  account: {
    notAuthUrls: ['Account/Login', 'test'],
    authOff: false,
    urlBase: 'http://luchopeco-001-site7.itempurl.com',
    urlLoginEndPoint: '/api/auth/Account/Login',
    urlRolesEndPoint: '/api/auth/Account/GetAllRoles',
    urlCreateRoleEndPoint: '/api/auth/Account/CreateRole',
    urlUsersEndPoint: '/api/auth/Account/GetUsers',
    urlCreateUserEndPoint: '/api/auth/Account/Register',
    urlEditUserEndPoint: '/api/auth/Account/EditUser',
    urlDeleteUserEndPoint: '/api/auth/Account/DeleteUser',
    urlGroupsEndPoint: '/api/auth/Account/GetAllGroups',
    urlCreateGroupEndPoint: '/api/auth/Account/CreateGroup',
    urlEditGroupEndPoint: '/api/auth/Account/EditGroup',
    urlDeleteGroupEndPoint: '/api/auth/Account/DeleteGroup',
    urlGetSucursalesAdministrables:
      '/api/auth/Account/GetSucursalesParaAdministradorSucursal',
    urlGetSucursalesParaUnRol: '/api/auth/Account/GetSucursalesParaUnRol',
  },
  Grilla: {
    bloquearFilas: 'diario', // 'nunca' | 'diario' | 'hora'
  },
  Menu: {
    unidadesreservastext: 'Canchas',
    unidadreservanuevatext: 'Cancha Nueva',
  },
  ReservaCrear: {
    urlBase: 'http://luchopeco-001-site7.itempurl.com',
    sucursalName: 'Tifosi1',
    pathReservaGet: '/api/reservas/Sucursal/{sucursal}/GetReserva',
    pathReservaCreate: '/api/reservas/Sucursal/{sucursal}/CrearReserva',
    pathReservaModify: '/api/reservas/Sucursal/{sucursal}/ModificarReserva',
    pathReservaChangeState:
      '/api/reservas/Sucursal/{sucursal}/CambiarEstadoReserva',
    pathReservaDelete: '/api/reservas/Sucursal/{sucursal}/EliminarReserva',
    pathMotivosReservaList:
      '/api/reservas/Sucursal/{sucursal}/ObtenerMotivosReserva',
    pathUnidadesReservaList:
      '/api/reservas/Sucursal/{sucursal}/ObtenerUnidadesReservaList',
    pathUnidadReservaGet:
      '/api/reservas/Sucursal/{sucursal}/ObtenerUnidadReserva',
    pathUnidadReservaEdit:
      '/api/reservas/Sucursal/{sucursal}/ModificarUnidadReserva',
    pathPrecioPorHoraRemove:
      '/api/reservas/Sucursal/{sucursal}/EliminarPrecioPorHora',
    pathUnidadReservaCreate:
      '/api/reservas/Sucursal/{sucursal}/CrearUnidadReserva',
    pathPrecioPorHoraList:
      '/api/reservas/Sucursal/{sucursal}/ObtenerPreciosPorHora',
    pathPrecioPorHoraEdit:
      '/api/reservas/Sucursal/{sucursal}/ModificarPrecioPorHora',
    pathReservasParaUnaFecha:
      '/api/reservas/Sucursal/{sucursal}/GetReservasParaUnaFecha',
    pathPrecioPorHoraCreate:
      '/api/reservas/Sucursal/{sucursal}/CrearPrecioPorHora',
    pathReservaParaUnaFecha:
      '/api/reservas/Sucursal/{sucursal}/GetReservasParaUnaFecha',
    pathReservasEliminadasParaUnaFecha:
      '/api/reservas/Sucursal/{sucursal}/GetReservasEliminadasParaUnaFecha',
    pathGetReservaEstaLiberadaParaUnaFechaHoraCancha:
      '/api/reservas/Sucursal/{sucursal}/GetReservaEstaLiberadaParaUnaFechaHoraCancha',
    stringToReplace: '{sucursal}',
    diasSemana: [
      'domingo',
      'lunes',
      'martes',
      'miercoles',
      'jueves',
      'viernes',
      'sabado',
    ],
    nombresMostrarDiasSemana: [
      'Domingo',
      'Lunes',
      'Martes',
      'Miércoles',
      'Jueves',
      'Viernes',
      'Sábado',
    ],
    TipoUnidadesReserva: ['5', '6', '7', '9', '11 '],
    UnidadReservaTitle: 'Cancha',
    ImporteReservaText: 'Valor Cancha',
    FinalizarReservaText: 'Finalizar Cancha',
    // mostrarSaldoCliente: true,
    autocompleteClienteOn: true,
    // duracionEnMinutos: 1 * 60,
    duracionMaximaMinutos: 12 * 60,
    comienzoDia: 6 * 60,
    step: 30,
    tiempoRefrescoGrilla: 120, // segundos
    minutosAvisoAntesDeFinalizarUnaReserva: 1,
    minutosProximosAvisos: [30, 60],
    rutasNoAvisarFinalizanReservas: ['login']
  },
  clientes: {
    telefonoArea: '341',
  },
  empresaController: {
    urlBase: 'http://luchopeco-001-site7.itempurl.com',
    pathGetCliente: '/api/reservas/Empresa/GetCliente',
    pathBuscarClientes: '/api/reservas/Empresa/BuscarCliente',
    pathCreateCliente: '/api/reservas/Empresa/CrearCliente',
    pathEditCliente: '/api/reservas/Empresa/EditarCliente',
    pathDeleteCliente: '/api/reservas/Empresa/EliminarCliente',
    pathClienteActualizarSemaforo:
      '/api/reservas/Empresa/ActualizarSemaforoCliente',
    pathClienteActualizarSaldoCliente:
      '/api/reservas/Empresa/ActualizarSaldoCliente',
    pathGetSucursales: '/api/reservas/Empresa/GetSucursales',
  },
  expresionesRegulares: {
    iso8601JsonDateRegEx: /^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d+)?(([+-]\d\d:\d\d)|Z)?$/,
    regexDe6a8digitos: /^\d{6,8}$/,
    regexDe2a4digitos: /^\d{2,4}$/,
  },
};
