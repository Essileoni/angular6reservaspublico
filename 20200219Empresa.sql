CREATE DATABASE Empresa
GO
USE [Empresa]
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 19/02/2020 8:38:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](128) NULL,
	[TelefonoNumero] [nvarchar](32) NULL,
	[TelefonoArea] [nvarchar](16) NULL,
	[EsTelefonoFijo] [bit] NOT NULL,
	[Documento] [nvarchar](32) NULL,
	[TipoDocumento] [nvarchar](32) NULL,
	[InfraccionesGraves] [int] NOT NULL,
	[Infracciones] [int] NOT NULL,
	[Cumplimientos] [int] NOT NULL,
	[Saldo] [decimal](18, 2) NOT NULL,
	[EmpresaId] [int] NULL,
	[Estado] [nvarchar](64) NULL,
	[NombreYApellido] [nvarchar](256) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Empresa]    Script Date: 19/02/2020 8:38:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresa](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NombreCompleto] [nvarchar](256) NULL,
	[CUIT] [nvarchar](64) NULL,
	[Documento] [nvarchar](32) NULL,
	[TipoDocumento] [nvarchar](32) NULL,
	[Actividad] [nvarchar](max) NULL,
 CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sucursal]    Script Date: 19/02/2020 8:38:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sucursal](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](128) NULL,
	[Calle] [nvarchar](256) NULL,
	[Numero] [nvarchar](64) NULL,
	[ComoLlegar] [nvarchar](2048) NULL,
	[Telefonos] [nvarchar](max) NULL,
	[Coordenadas] [nvarchar](256) NULL,
	[DataBaseCatalogName] [nvarchar](128) NULL,
	[EmpresaId] [int] NULL,
 CONSTRAINT [PK_Sucursal] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([Id], [Email], [TelefonoNumero], [TelefonoArea], [EsTelefonoFijo], [Documento], [TipoDocumento], [InfraccionesGraves], [Infracciones], [Cumplimientos], [Saldo], [EmpresaId], [Estado], [NombreYApellido]) VALUES (2, N'esteban2@gmail.com', N'1231231', N'342', 1, N'34343332', N'dni', 81, 91, 148, CAST(0.00 AS Decimal(18, 2)), 1, NULL, N'Esteban Kito')
INSERT [dbo].[Clientes] ([Id], [Email], [TelefonoNumero], [TelefonoArea], [EsTelefonoFijo], [Documento], [TipoDocumento], [InfraccionesGraves], [Infracciones], [Cumplimientos], [Saldo], [EmpresaId], [Estado], [NombreYApellido]) VALUES (3, N'carlos@gmail.com', N'3503467', N'341', 0, N'33333333', N'dni', 0, 28, 31, CAST(-200.00 AS Decimal(18, 2)), 1, NULL, N'Ezequiel Araujo')
INSERT [dbo].[Clientes] ([Id], [Email], [TelefonoNumero], [TelefonoArea], [EsTelefonoFijo], [Documento], [TipoDocumento], [InfraccionesGraves], [Infracciones], [Cumplimientos], [Saldo], [EmpresaId], [Estado], [NombreYApellido]) VALUES (4, N'ezequiel@gmail.com', N'3434343', N'341', 0, N'12345678', N'', 0, 0, 8, CAST(-150.00 AS Decimal(18, 2)), NULL, NULL, N'Homero Simpson')
SET IDENTITY_INSERT [dbo].[Clientes] OFF
SET IDENTITY_INSERT [dbo].[Empresa] ON 

INSERT [dbo].[Empresa] ([Id], [NombreCompleto], [CUIT], [Documento], [TipoDocumento], [Actividad]) VALUES (1, N'Tifosi', N'20343434347', N'34343434', N'Cuit', N'Deportes')
SET IDENTITY_INSERT [dbo].[Empresa] OFF
SET IDENTITY_INSERT [dbo].[Sucursal] ON 

INSERT [dbo].[Sucursal] ([Id], [Nombre], [Calle], [Numero], [ComoLlegar], [Telefonos], [Coordenadas], [DataBaseCatalogName], [EmpresaId]) VALUES (1, N'Tifosi1', N'Sarmiento', N'1155', N'Por mendoza derecho', N'4574574', N'-65.6598956, -84,576546', N'Tifosi1', NULL)
INSERT [dbo].[Sucursal] ([Id], [Nombre], [Calle], [Numero], [ComoLlegar], [Telefonos], [Coordenadas], [DataBaseCatalogName], [EmpresaId]) VALUES (2, N'Tifosi2', N'Mendoza', N'2041', N'Derecho por mendoza', N'4564564', N'-70.45678', N'Tifosi2', NULL)
SET IDENTITY_INSERT [dbo].[Sucursal] OFF
ALTER TABLE [dbo].[Clientes] ADD  CONSTRAINT [DF_Clientes_EsTelefonoFijo]  DEFAULT ((0)) FOR [EsTelefonoFijo]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Empresa_EmpresaId] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresa] ([Id])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_Empresa_EmpresaId]
GO
ALTER TABLE [dbo].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_Empresa_EmpresaId] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresa] ([Id])
GO
ALTER TABLE [dbo].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Empresa_EmpresaId]
GO
